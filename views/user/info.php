<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $user app\models\ars\User */
/* @var $startDate string */
/* @var $meterList array */
/* @var $railwayList array */

$this->title = 'Пользователь: ' . $user->login;
$this->params['breadcrumbs'][] = 'Инфо';
?>
<div class="order-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model'      => $user,
        'attributes' => [
            'login',
            'email',
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ]) ?>

</div>
