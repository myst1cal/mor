<?php


namespace app\services;


use app\models\ars\ClientCertificate;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use yii\helpers\ArrayHelper;

class ReportService
{
    private static $font = [
        'font' => [
            'bold' => true,
            'size' => 12,
        ],
    ];

    /**
     * @param ClientCertificate[] $data
     * @return Spreadsheet
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public static function byDatesToXls(array $data)
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $sheet->getColumnDimension('A')->setWidth(25);
        $sheet->getColumnDimension('B')->setWidth(25);
        $sheet->getColumnDimension('C')->setWidth(15);
        $sheet->getColumnDimension('D')->setWidth(15);
        $sheet->getColumnDimension('E')->setWidth(15);
        $sheet->getColumnDimension('F')->setWidth(15);
        $sheet->getColumnDimension('G')->setWidth(15);
        $sheet->getColumnDimension('H')->setWidth(25);
        $sheet->getColumnDimension('I')->setWidth(25);

        $sheet->getStyle('A')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_RIGHT);

        $sheet->getStyle('A1:J1')->applyFromArray(self::$font)->getAlignment()->setWrapText(true);

        $sheet->setCellValue('A' . 1, 'ФИО');
        $sheet->setCellValue('B' . 1, 'Дата рождения');
        $sheet->setCellValue('C' . 1, 'Курс');
        $sheet->setCellValue('D' . 1, 'Прайс');
        $sheet->setCellValue('E' . 1, 'Доп. оплата');
        $sheet->setCellValue('F' . 1, 'Сумма');
        $sheet->setCellValue('G' . 1, 'Оплечен');
        $sheet->setCellValue('H' . 1, 'Дата получения');
        $sheet->setCellValue('I' . 1, 'Посредник');

        $rowNumber = 2;
        foreach ($data as $certificate) {

            $sheet->setCellValue('A' . $rowNumber, $certificate->client->surname_ukr . ' ' . $certificate->client->name_ukr . ' ' . $certificate->client->patronymic_ukr);
            $sheet->setCellValue('B' . $rowNumber, $certificate->client->birthday);
            $sheet->setCellValue('C' . $rowNumber, $certificate->course->title_ukr);
            $sheet->setCellValue('D' . $rowNumber, $certificate->cost_price);
            $sheet->setCellValue('E' . $rowNumber, $certificate->additional_sum);
            $sheet->setCellValue('F' . $rowNumber, $certificate->total_sum);
            $sheet->setCellValue('G' . $rowNumber, $certificate->paid ? 'Да' : 'Нет');
            $sheet->setCellValue('H' . $rowNumber, $certificate->group->end_date);
            $sheet->setCellValue('I' . $rowNumber, $certificate->middlemen_id ? $certificate->middlemen->name : 'Нет');

            $rowNumber++;
        }

        return $spreadsheet;
    }

    /**
     * @param array[] $data
     * @return Spreadsheet
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public static function byDayToXls(array $data)
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $sheet->getColumnDimension('A')->setWidth(25);
        $sheet->getColumnDimension('B')->setWidth(25);

        $sheet->getStyle('A')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_RIGHT);

        $sheet->getStyle('A1:B1')->applyFromArray(self::$font)->getAlignment()->setWrapText(true);

        $sheet->setCellValue('A' . 1, 'ФИО');
        $sheet->setCellValue('B' . 1, 'Сумма, грн.');

        $rowNumber = 2;
        foreach ($data as $certificate) {

            $sheet->setCellValue('A' . $rowNumber, $certificate['surname_ukr'] . ' ' . $certificate['name_ukr'] . ' ' . $certificate['patronymic_ukr']);
            $sheet->setCellValue('B' . $rowNumber, $certificate['total_sum']);

            $rowNumber++;
        }
        $sum = array_sum(ArrayHelper::getColumn($data, 'total_sum'));
        $rowNumber++;

        $sheet->setCellValue('A' . $rowNumber, 'Всего:');
        $sheet->setCellValue('B' . $rowNumber, $sum);

        return $spreadsheet;
    }
}