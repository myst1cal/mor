<?php

use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ars\Group */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="group-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-lg-4">
            <?= $form->field($model, 'course_id')->dropDownList($courses, [
                    'prompt' => 'Выберите курс',
                    'onChange' => '
                        $.post("/course/get-teachers?id=" + $(this).val(), function(data){
                            $("#group-teacher_id").html(data);
                        }) 
                    '
            ]) ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'teacher_id')->dropDownList($currentTeachers) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'max_students')->input('number') ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4">
            <?= $form->field($model, 'start_date')->widget(DatePicker::classname(), [
                'options'       => ['placeholder' => 'ГГГГ-ММ-ДД', 'autocomplete' => 'off'],
                'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd'
                ]]) ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'end_date')->widget(DatePicker::classname(), [
                'options'       => ['placeholder' => 'ГГГГ-ММ-ДД', 'autocomplete' => 'off'],
                'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd'
                ]]) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
