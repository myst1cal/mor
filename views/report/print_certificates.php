<?php

use app\helpers\AuthHelper;
use app\models\ars\ClientCertificate;
use kartik\date\DatePicker;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\searches\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $certificate ClientCertificate */
/* @var $sum integer */

$this->title = 'Отчет: печать сертификатов';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="report-print_certificates">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= Html::beginForm('', 'get', ['id' => 'report-print-certificates']) ?>
    <div class="form-group">
        <div class="row">
            <div class="col-lg-3">
                <?= DatePicker::widget(
                    [
                        'name' => 'day',
                        'options' => ['placeholder' => 'ГГГГ-ММ-ДД', 'autocomplete' => 'off'],
                        'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                        'value' => $day,
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'yyyy-mm-dd',
                        ],
                    ]) ?>
            </div>
            <div class="col-lg-1">
                <?= Html::submitButton('Поиск', ['class' => 'report-print_certificates btn btn-success']) ?>
            </div>
        </div>
    </div>
    <?= Html::endForm() ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'label' => 'Курс',
                'value' => function ($certificate) {
                    /** @var $certificate ClientCertificate */
                    return $certificate->course->title_ukr;
                }
            ],
            [
                'label' => 'ФИО',
                'format' => 'raw',
                'value' => function ($certificate) {
                    /** @var $certificate ClientCertificate */
                    $text = $certificate->client->getFullNameUkr() . "({$certificate->client->getFullNameTransliteration()})";

                    return Html::a($text, Url::to(['client/view', 'id' => $certificate->client_id]), ['target' => '_blank']);
                }
            ],
            'date_of_issue',
            [
                'attribute' => 'paid',
                'format' => 'boolean'
            ],
            [
                'label' => 'Распечатан',
                'format' => 'raw',
                'value' => function (ClientCertificate $certificate) {
                    return Html::checkbox('printed', $certificate->printed, ['class' => 'form-control ajax-status', 'data-id' => $certificate->id, 'data-attribute' => 'printed',] + (AuthHelper::canEditCertificatePrinted() ? [] : ['disabled' => 'disabled']));
                }
            ],
            [
                'label' => '',
                'format' => 'raw',
                'value' => function ($certificate) {
                    /** @var $model ClientCertificate */
                    $html = Html::a('Печать',
                        ['print/certificate'], [
                            'class' => 'print-button btn btn-success ' . ($certificate->paid ? '' : 'hide'),
                            'target' => '_blank',
                            'data-id' => $certificate->id,
                            'data-method' => 'POST',
                            'data-params' => [
                                'certificate_id' => $certificate->id,
                                'csrf_param' => Yii::$app->request->csrfParam,
                                'csrf_token' => Yii::$app->request->csrfToken,
                            ],
                        ]);

                    $html .= Html::a('Печать на карте',
                        ['print-card/certificate'], [
                            'class' => 'print-button btn btn-success ' . ($certificate->paid ? '' : 'hide'),
                            'target' => '_blank',
                            'data-id' => $certificate->id,
                            'data-method' => 'POST',
                            'data-params' => [
                                'certificate_id' => $certificate->id,
                                'csrf_param' => Yii::$app->request->csrfParam,
                                'csrf_token' => Yii::$app->request->csrfToken,
                            ],
                        ]);
                    if ($certificate->course->has_ukr_only) {
                        $html .= Html::a('Печать укр. шаблона',
                            ['print/certificate'], [
                                'class' => 'print-button btn btn-info ' . ($certificate->paid ? '' : 'hide'),
                                'target' => '_blank',
                                'data-id' => $certificate->id,
                                'data-method' => 'POST',
                                'data-params' => [
                                    'certificate_id' => $certificate->id,
                                    'ukr_only' => 1,
                                    'csrf_param' => Yii::$app->request->csrfParam,
                                    'csrf_token' => Yii::$app->request->csrfToken,
                                ],
                            ]);
                    }
                    return $html;
                }
            ],
        ],
    ]); ?>
</div>

<?php $this->registerJsFile('js/certificate.js', ['depends' => ['yii\web\JqueryAsset']]); ?>
