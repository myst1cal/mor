<?php

namespace app\models\ars;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property int $id [int(11)]
 * @property string $login [varchar(255)]
 * @property string $auth_key [varchar(32)]
 * @property string $password_hash [varchar(255)]
 * @property string $password_reset_token [varchar(255)]
 * @property string $email [varchar(255)]
 * @property int $created_at [int(11)]
 * @property int $updated_at [int(11)]
 * @property string $password
 * @property string $authKey
 * @property int $deleted [smallint(6)]
 */
class User extends ActiveRecord implements IdentityInterface
{
    const NOT_DELETED = 0;
    const DELETED = 1;

    const ROLE_ADMINISTRATOR = 'administrator';
    const ROLE_ACCOUNTANT = 'accountant';
    const ROLE_MANAGER = 'manager';
    const ROLE_METHODIST = 'methodist';
    const ROLE_AUDITOR = 'auditor';

    public static function getRoles() {
        return [
            self::ROLE_MANAGER => 'Менеджер',
            self::ROLE_METHODIST => 'Методист',
            self::ROLE_AUDITOR => 'Аудитор',
            self::ROLE_ACCOUNTANT => 'Бухгалтер',
            self::ROLE_ADMINISTRATOR => 'Администратор',
        ];
    }
    public static function findByLogin($login)
    {
        return self::findOne(['login' => trim($login)]);
    }

    public function attributeLabels()
    {
        return [
            'login' => 'Логин',
            'email' => 'Почта',
            'created_at' => 'Создан',
            'updated_at' => 'Отредактирован',
        ];
    }

    public static function tableName()
    {
        return '{{%user}}';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function rules()
    {
        return [];
    }

    public function getRole()
    {
        $userRoles = Yii::$app->authManager->getRolesByUser($this->id);

        foreach ($userRoles as $role) {
            return $role->name;
        }

        return '';
    }

    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'deleted' => self::NOT_DELETED]);
    }

    /**
     * @param mixed $token
     * @param null $type
     * @return void|IdentityInterface
     * @throws NotSupportedException
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'deleted' => self::NOT_DELETED,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int)substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     * @throws \yii\base\Exception
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }
}

