<?php

use app\models\ars\ClientCertificate;
use yii\db\Migration;

/**
 * Class m190703_152129_addCourseIdColumnToClientCertificateTable
 */
class m190703_152129_addCourseIdColumnToClientCertificateTable extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%client_certificate}}', 'course_id', $this->integer()->notNull()->after('group_id'));
        $this->alterColumn('{{%client_certificate}}', 'group_id', $this->integer()->null());
        $this->createIndex('course_id_i', '{{%client_certificate}}', 'course_id');

        $certificateList = ClientCertificate::find()->all();

        foreach ($certificateList as $certificate) {
            $certificate->course_id = $certificate->group->course_id;
            $certificate->save(false);
        }
    }

    public function safeDown()
    {
        $this->dropIndex('course_id_i', '{{%client_certificate}}');
        $this->dropColumn('{{%client_certificate}}', 'course_id');
    }
}
