<?php

use app\helpers\AuthHelper;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\searches\MiddlemenSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Посредники';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="middlemen-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= AuthHelper::canEditMiddlemen() ? Html::a('Добавить посредника', ['create'], ['class' => 'btn btn-success']) : '' ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',

            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => AuthHelper::canEditMiddlemen() ? '{view} {update}' : '{view}',
            ],
        ],
    ]); ?>


</div>
