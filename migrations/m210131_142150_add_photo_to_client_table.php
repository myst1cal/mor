<?php

use app\models\ars\Client;
use yii\db\Migration;

class m210131_142150_add_photo_to_client_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn(Client::tableName(), 'photo_dir', $this->string(28)->null());
        $this->addColumn(Client::tableName(), 'photo_name', $this->string(15)->null());
    }

    public function safeDown()
    {
        $this->dropColumn(Client::tableName(), 'photo_dir');
        $this->dropColumn(Client::tableName(), 'photo_name');
    }
}
