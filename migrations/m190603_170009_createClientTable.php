<?php

use yii\db\Migration;

/**
 * Class m190603_170009_createClientTable
 */
class m190603_170009_createClientTable extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%client}}', [
            'id'                         => $this->primaryKey(),
            'name_ukr'                   => $this->string(200)->notNull(),
            'patronymic_ukr'             => $this->string(200),
            'surname_ukr'                => $this->string(200)->notNull(),
            'name_transliteration'       => $this->string(200)->notNull(),
            'patronymic_transliteration' => $this->string(200),
            'surname_transliteration'    => $this->string(200)->notNull(),
            'birthday'                   => $this->date(),
            'created_at'                 => $this->integer(),
            'updated_at'                 => $this->integer(),
            'phone'                      => $this->string()->null(),
            'email'                      => $this->string()->null(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%client}}');
    }
}
