<?php

namespace app\models\ars;

use Yii;

/**
 * This is the model class for table "course_additional_field".
 *
 * @property int $id
 * @property string $name
 * @property string $additional_data
 *
 * @property CourseAdditionalFieldClientCertificate[] $courseAdditionalFieldClientCertificates
 * @property ClientCertificate[] $clientCertificates
 * @property CourseAdditionalFieldCourse[] $courseAdditionalFieldCourses
 * @property Course[] $courses
 */
class CourseAdditionalField extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'course_additional_field';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['additional_data'], 'string'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'additional_data' => 'Доп. информация',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourseAdditionalFieldClientCertificates()
    {
        return $this->hasMany(CourseAdditionalFieldClientCertificate::className(), ['course_additional_field_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientCertificates()
    {
        return $this->hasMany(ClientCertificate::className(), ['id' => 'client_certificate_id'])->viaTable('course_additional_field_client_certificate', ['course_additional_field_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourseAdditionalFieldCourses()
    {
        return $this->hasMany(CourseAdditionalFieldCourse::className(), ['course_additional_field_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourses()
    {
        return $this->hasMany(Course::className(), ['id' => 'course_id'])->viaTable('course_additional_field_course', ['course_additional_field_id' => 'id']);
    }
}
