<?php

namespace app\services;


use app\models\ars\Log;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

class LogService
{
    static $document = null;
    static $user = null;

    static $oldData = [];
    static $newData = [];

    public static function add(ActiveRecord $document, IdentityInterface $user = null)
    {
        self::$document = $document;
        self::$user = $user;

        $document->on(ActiveRecord::EVENT_BEFORE_UPDATE, [LogService::class, 'beforeSave']);
        $document->on(ActiveRecord::EVENT_BEFORE_INSERT, [LogService::class, 'beforeSave']);
        $document->on(ActiveRecord::EVENT_AFTER_UPDATE, [LogService::class, 'save']);
        $document->on(ActiveRecord::EVENT_AFTER_INSERT, [LogService::class, 'save']);
    }

    public static function beforeSave()
    {
        $document = self::$document;

        if ($document->isNewRecord) {
            self::$oldData = [];
            self::$newData = $document->attributes;
        } else {
            foreach ($document->oldAttributes as $attribute => $value) {
                if ($value != $document->$attribute) {
                    self::$oldData[$attribute] = $value;
                    self::$newData[$attribute] = $document->$attribute;
                }
            }
        }
    }

    public static function save()
    {
        $document = self::$document;
        $user = self::$user;

        $log = new Log();

        $log->table = $document::tableName();
        $log->foreign_id = (string)$document->getPrimaryKey();
        $log->old_data = json_encode(self::$oldData);
        $log->new_data = json_encode(self::$newData);
        $log->user_id = empty($user) ? null : $user->getId();
        $log->created_at = time();

        $log->validate();

        return $log->save();
    }
}