<?php

use app\helpers\AuthHelper;
use app\models\ars\Course;
use app\models\ars\Group;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\searches\GroupSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Группы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="group-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= AuthHelper::canEditGroup() ? Html::a('Добавить группу', ['create'], ['class' => 'btn btn-success']) : '' ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'course_id',
                'value' => function ($model) {
                    /** @var $model Group */
                    return $model->course->title_ukr;
                },
                'filter' => Course::getList(),
            ],
            [
                'attribute' => 'teacher_id',
                'value' => function ($model) {
                    /** @var $model Group */
                    return $model->teacher->full_name;
                },
            ],
            'name',
            [
                'label' => 'Осталось/всего мест',
                'attribute' => 'max_students',
                'value' => function ($model) {
                    /** @var $model Group */
                    return ($model->max_students - $model->countStudents()) . '/' . $model->max_students;
                },
            ],
            'start_date',
            'end_date',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => AuthHelper::canEditGroup() ? '{view} {update} {print}' : '{view} {print}',
                'buttons' => [
                    'print' => function ($url, $model) {
                        return Html::a(
                            '<span class="glyphicon glyphicon-print"></span>',
                            '/print/group?group_id=' . $model->id,
                            ['target' => '_blank']
                        );
                    },
                ],
            ],
        ],
    ]); ?>


</div>
