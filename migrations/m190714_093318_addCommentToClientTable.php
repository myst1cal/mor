<?php

use yii\db\Migration;

class m190714_093318_addCommentToClientTable extends Migration
{
    public function safeUp()
    {
        $this->addColumn('client', 'comment', $this->text()->null());
    }

    public function safeDown()
    {
        $this->dropColumn('client', 'comment');
    }
}
