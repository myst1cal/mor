<?php
/** @var $certificate \app\models\ars\ClientCertificate */
?>
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1251">
<meta name=Generator content="Microsoft Word 14 (filtered)">
<style>
        <!--
        /* Font Definitions */
        @font-face {
            font-family: Calibri;
            panose-1: 2 15 5 2 2 2 4 3 2 4;
            mso-font-charset: 204;
            mso-generic-font-family: swiss;
            mso-font-pitch: variable;
            mso-font-signature: -536870145 1073786111 1 0 415 0;
        }

        @font-face {
            font-family: Tahoma;
            panose-1: 2 11 6 4 3 5 4 4 2 4;
            mso-font-charset: 204;
            mso-generic-font-family: swiss;
            mso-font-pitch: variable;
            mso-font-signature: -520081665 -1073717157 41 0 66047 0;
        }

        /* Style Definitions */
        p.MsoNormal, li.MsoNormal, div.MsoNormal {
            mso-style-unhide: no;
            mso-style-qformat: yes;
            mso-style-parent: "";
            margin-top: 0cm;
            margin-right: 0cm;
            margin-bottom: 10.0pt;
            margin-left: 0cm;
            line-height: 115%;
            mso-pagination: widow-orphan;
            font-size: 11.0pt;
            font-family: "Calibri", "sans-serif";
            mso-fareast-font-family: "Times New Roman";
            mso-fareast-theme-font: minor-fareast;
            mso-bidi-font-family: "Times New Roman";
        }

        a:-webkit-any-link {
            color: #000;
            cursor: pointer;
            text-decoration: none !important;
        }

        p.MsoAcetate, li.MsoAcetate, div.MsoAcetate {
            mso-style-noshow: yes;
            mso-style-priority: 99;
            mso-style-link: "Текст выноски Знак";
            margin: 0cm;
            margin-bottom: .0001pt;
            mso-pagination: widow-orphan;
            font-size: 8.0pt;
            font-family: "Tahoma", "sans-serif";
            mso-fareast-font-family: "Times New Roman";
            mso-fareast-theme-font: minor-fareast;
        }

        span.a {
            mso-style-name: "Текст выноски Знак";
            mso-style-noshow: yes;
            mso-style-priority: 99;
            mso-style-unhide: no;
            mso-style-locked: yes;
            mso-style-link: "Текст выноски";
            font-family: "Tahoma", "sans-serif";
            mso-ascii-font-family: Tahoma;
            mso-hansi-font-family: Tahoma;
            mso-bidi-font-family: Tahoma;
        }

        p.msopapdefault, li.msopapdefault, div.msopapdefault {
            mso-style-name: msopapdefault;
            mso-style-unhide: no;
            mso-margin-top-alt: auto;
            margin-right: 0cm;
            margin-bottom: 10.0pt;
            margin-left: 0cm;
            line-height: 115%;
            mso-pagination: widow-orphan;
            font-size: 12.0pt;
            font-family: "Times New Roman", "serif";
            mso-fareast-font-family: "Times New Roman";
            mso-fareast-theme-font: minor-fareast;
        }

        span.SpellE {
            mso-style-name: "";
            mso-spl-e: yes;
        }

        span.GramE {
            mso-style-name: "";
            mso-gram-e: yes;
        }

        .MsoChpDefault {
            mso-style-type: export-only;
            mso-default-props: yes;
            font-size: 10.0pt;
            mso-ansi-font-size: 10.0pt;
            mso-bidi-font-size: 10.0pt;
        }

        @page WordSection1 {
            size: 595.3pt 841.9pt;
            margin: 0cm 0cm 0cm 0cm;
            mso-header-margin: 35.4pt;
            mso-footer-margin: 35.4pt;
            mso-paper-source: 0;
        }

        div.WordSection1 {
            page: WordSection1;
        }

        -->
</style>
</head>

<body>
 <div class=WordSection1>
  <div align=center>
   <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=809 style='width:606.9pt;border-collapse:collapse'>
    <tr style='height:68.25pt'>
     <td width=123 nowrap colspan=5 align=center valign=center style='width:92.25pt;padding:0cm 5.4pt 0cm 5.4pt;height:68.25pt'><p class=MsoNormal align=center ><span style='font-size:8.0pt; font-family:"Times New Roman","serif"'>м.п. <br />закладу</span></p></td>
     <td width=51 rowspan=7 style='width:38.1pt;padding:0cm 5.4pt 0cm 5.4pt;height:419.25pt'>
      <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt; writing-mode: vertical-rl; text-align:center;line-height:normal'><b><span style='font-size:10.0pt;font-family:"Times New Roman","serif"'>ОФІЦЕР З БЕЗПЕКИ СУДНА</span></b></p>
     </td>
     <td width=28 rowspan=7 style='width:20.85pt;padding:0cm 5.4pt 0cm 5.4pt;height:419.25pt'>
      <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt; writing-mode: vertical-rl; text-align:center;line-height:normal'><span style='font-size:8.0pt; font-family:"Times New Roman","serif"'>успішно пройшов підготовку за схваленим дистанційним теоретичним курсом</span></p>
     </td>
     <td width=28 nowrap style='width:20.85pt;padding:0cm 5.4pt 0cm 5.4pt;height:68.25pt'>
      <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt; writing-mode: vertical-rl; text-align:center;line-height:normal'><span style='font-size:8.0pt; font-family:"Times New Roman","serif"'>Дата народження:</span></p>
     </td>
     <td width=66 nowrap colspan=2 valign=center style='width:49.2pt;padding:0cm 5.4pt 0cm 20.0pt;height:88.25pt'>
      <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;line-height: normal; writing-mode: vertical-rl;'><span style='font-size:8.0pt;font-family:"Times New Roman","serif"; color:black'>Цим засвідчується,<br /> що:</span></p>
     </td>
     <td width=34 nowrap rowspan=7 valign=center style='width:25.35pt;padding:0cm 5.4pt 0cm 5.4pt;height:419.25pt'>
      <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt; writing-mode: vertical-rl; text-align:center;line-height:normal'><b><span style='font-size:12.0pt; font-family:"Times New Roman","serif"'>СВІДОЦТВО </span><span style='display: inline-block;transform: rotate(90deg);font-size:12.0pt; font-family:"Times New Roman","serif"'>№ </span><span style='font-size:12.0pt; font-family:"Times New Roman","serif"'> <?= $certificate->number ?></span></b></p>
     </td>
     <td width=53 nowrap rowspan=7 valign=bottom style='width:38.45pt;padding:0cm 5.4pt 0cm 5.4pt;'></td>
     <td width=54 nowrap rowspan=7 valign=bottom style='width:35.25pt;padding:0cm 5.4pt 0cm 5.4pt;'></td>
     <td width=34 nowrap rowspan=7 valign=center style='width:25.35pt;padding:0cm 5.4pt 0cm 5.4pt;height:419.25pt'>
      <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt; writing-mode: vertical-lr; transform: rotate(-180deg);text-align:center;line-height:normal'><b><span style='font-size:12.0pt; font-family:"Times New Roman","serif"'>CERTIFICATE </span><span style='display: inline-block;transform: rotate(90deg);font-size:12.0pt; font-family:"Times New Roman","serif"'>№ </span><span style='font-size:12.0pt; font-family:"Times New Roman","serif"'> <?= $certificate->number ?></span></b></p>
     </td>
     <td width=34 nowrap rowspan=6 valign=center style='width:25.35pt;padding:0cm 2.4pt 0cm 2.4pt; height:68.25pt'>
      <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;writing-mode: vertical-lr; transform: rotate(-180deg);text-align:center;line-height:normal'><b><span style='font-size:14.0pt;font-family:"Times New Roman","serif";'><?= $certificate->client->name_transliteration ?></span></b></p>
     </td>
     <td width=34 nowrap rowspan=6 valign=center style='width:25.35pt;padding:0cm 2.4pt 0cm 2.4pt; height:68.25pt'>
      <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;writing-mode: vertical-lr; transform: rotate(-180deg);text-align:center; line-height:normal'><b><span style='font-size:14.0pt;font-family:"Times New Roman","serif";'><?= $certificate->client->surname_transliteration ?></span></b></p>
     </td>
     <td width=28 nowrap rowspan=6 valign=center style='width:20.85pt;padding:0cm 2.4pt 0cm 2.4pt; height:68.25pt'>
      <p class=MsoNormal align=center style='margin-bottom: 0cm;margin-bottom:.0001pt;writing-mode: vertical-lr; transform: rotate(-180deg);line-height:normal'><b><span style='font-size:14.0pt;font-family:"Times New Roman","serif";'><?= Yii::$app->formatter->asDate($certificate->client->birthday) ?></span></b></p>
     </td>
     <td width=28 rowspan=7 style='width:20.85pt;padding:0cm 5.4pt 0cm 5.4pt;height:419.25pt'>
      <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;writing-mode: vertical-lr; transform: rotate(-180deg); text-align:center;line-height:normal'><span lang=EN-US style='font-size:8.0pt; font-family:"Times New Roman","serif"'>has successfully completed the approved distance theoretical course of training </span></p>
     </td>
     <td width=30 rowspan=7 style='width:22.35pt;padding:0cm 5.4pt 0cm 5.4pt;height:419.25pt'>
      <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;writing-mode: vertical-rl; transform: rotate(-180deg); text-align:center;line-height:normal'><b><span lang=EN-US style='font-size: 10.0pt;font-family:"Times New Roman","serif"'>SHIP SAFETY OFFICER</span></b></p>
     </td>
     <td width=28 rowspan=6 valign=bottom style='width:20.85pt;padding:0cm 5.4pt 0cm 5.4pt;height:351.25pt'>
      <p class=MsoNormal align=center style='margin-bottom:0.7cm;writing-mode: vertical-rl; transform: rotate(-180deg); text-align:left;line-height:normal'><span lang=EN-US style='font-size:8.0pt; font-family:"Times New Roman","serif"'>For mаnаgement level officers and based on the requirements of the ISM Code (IMO Resolution A.741.(18)), Chapter IX SOLAS-74, Sections А-ІІ/2, А-ІІІ/2 of the STCW Code and IMO Model Course 3.11</span></p>
     </td>
     <td width=28 rowspan=6 valign=center style='width:20.85pt;padding:0cm 5.4pt 0cm 5.4pt;height:351.25pt'>
      <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;writing-mode: vertical-rl; transform: rotate(-180deg); text-align:center;line-height:normal'><span style='font-size:8.0pt; font-family:"Times New Roman","serif"'>Manager..............………             Registrar…….…………</span></p>
     </td>
     <td width=56 nowrap colspan=2 rowspan=4 valign=bottom style='width:41.7pt;padding:0cm 5.4pt 0cm 5.4pt; height:68.25pt'></td>
     <td width=15 rowspan=6 valign=bottom style='width:11.1pt;padding:0cm 5.4pt 0cm 5.4pt;height:68.25pt'></td>
     <td style='height:68.25pt;border:none' width=0 height=91></td>
	</tr>
	<tr style='height:48.0pt'>
     <td width=15 rowspan=6 valign=top style='width:11.1pt;padding:0cm 5.4pt 0cm 5.4pt;height:48.0pt'></td>
     <td width=28 nowrap style='width:20.85pt; padding:0cm 5.4pt 0cm 5.4pt;height:48.0pt'>
      <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;writing-mode: vertical-rl;line-height: normal'><span style='font-size:8.0pt;font-family:"Times New Roman","serif"'>Дійсно до:</span></p>
     </td>
     <td width=28 nowrap style='width:20.85pt; padding:0cm 5.4pt 0cm 5.4pt;height:48.0pt'>
      <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;writing-mode: vertical-rl;line-height: normal'><span style='font-size:8.0pt;font-family:"Times New Roman","serif"'>Дата видачі:</span></p>
     </td>
	 <td width=25 rowspan=6 valign=center style='width:18.6pt;padding:0cm 5.4pt 0cm 5.4pt;height:351.0pt'>
      <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;writing-mode: vertical-rl;text-align:center;line-height:normal'><b><i><span style='font-size:6.0pt; font-family:"Times New Roman","serif"'>Керівник
  ..…………………………………                  Реєстратор.……………........................</span></i></b></p>
     </td>
     <td width=28 rowspan=6 valign=top style='width:20.85pt; padding:0cm 5.4pt 0cm 5.4pt;height:351.0pt'>
      <p class=MsoNormal align=center style='margin-bottom:0.5cm;writing-mode: vertical-rl; text-align:left;line-height:normal'><span style='font-size:8.0pt; font-family:"Times New Roman","serif"'>Для старшого командного плавскладу суден, за програмою, яка базується на вимогах Міжнародного Кодексу з управління безпечною експлуатацією суден і запобіганню забруднення (IMO Резолюція А.741.(18)), Глави ІХ SOLAS-74, розділів А-ІІ/2, А-ІІІ/2 Кодексу ПДНВ та Модельного курсу ІМО 3.11</span></p>
     </td>
     <td width=28 nowrap rowspan=6 style='width:20.85pt; padding:0cm 2.4pt 0cm 2.4pt;'>
      <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;writing-mode: vertical-rl;line-height: normal'><b><span style='font-size:14.0pt;font-family:"Times New Roman","serif"'><?= Yii::$app->formatter->asDate($certificate->client->birthday) ?></span></b></p>
     </td>
     <td width=33 nowrap rowspan=6 style='width:24.6pt; padding:0cm 2.4pt 0cm 2.4pt;'>
      <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt; writing-mode: vertical-rl; text-align:center;line-height:normal'><b><span style='font-size:14.0pt;font-family:"Times New Roman","serif"'><?= $certificate->client->name_ukr . '&nbsp;' . $certificate->client->patronymic_ukr ?></span></b></p>
     </td>
     <td width=33 nowrap rowspan=6 style='width:24.6pt; padding:0cm 2.4pt 0cm 2.4pt;'>
      <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;writing-mode: vertical-rl; text-align:center;line-height:normal'><b><span style='font-size:14.0pt;font-family:"Times New Roman","serif";'><?= $certificate->client->surname_ukr ?></span></b></p>
     </td>
     <td style='border:none' width=0></td>
	</tr>
	<tr style='height:47.25pt'>
	 <td width=28 nowrap style='width:20.85pt; padding:0cm 5.4pt 0cm 5.4pt;height:47.25pt'>
      <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt; writing-mode: vertical-rl;line-height: normal'><b><span style='font-size:8.0pt;font-family:"Times New Roman","serif"'><?= $certificate->getDateEnd() ?></span></b></p>
     </td>
     <td width=28 nowrap style='width:20.85pt; padding:0cm 5.4pt 0cm 5.4pt;height:47.25pt'>
      <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt; writing-mode: vertical-rl; line-height: normal'><b><span style='font-size:8.0pt;font-family:"Times New Roman","serif"'><?= $certificate->getDateStart() ?></span></b></p>
     </td>
     <td style='height:47.25pt;border:none' width=0 height=63></td>
	</tr>
	<tr style='height:41.25pt'>
	 <td width=56 nowrap colspan=2 rowspan=4 valign=bottom style='width:41.7pt; padding:0cm 5.4pt 0cm 5.4pt;height:41.25pt'></td>
     <td style='height:41.25pt;border:none' width=0 height=55></td>
	</tr>
	<tr style='height:48.0pt'>
	 <td width=28 nowrap style='width:20.85pt; padding:0cm 5.4pt 0cm 5.4pt;height:48.0pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;writing-mode: vertical-rl; transform: rotate(-180deg);line-height: normal'><b><span style='font-size:8.0pt;font-family:"Times New Roman","serif"'><?= $certificate->getDateStart() ?></span></b></p>
	 </td>
     <td width=28 nowrap style='width:20.85pt; padding:0cm 5.4pt 0cm 5.4pt;height:48.0pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;writing-mode: vertical-rl; transform: rotate(-180deg);line-height:normal'><b><span style='font-size:8.0pt;font-family:"Times New Roman","serif"; color:black'><?= $certificate->getDateEnd() ?></span></b></p>
     </td>
      <td style='height:48.0pt;border:none' width=0 height=64></td>
     </tr>
     <tr style='height:46.25pt'>
      <td width=28 style='width:20.85pt; padding:0cm 5.4pt 0cm 5.4pt;height:46.25pt'>
       <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;writing-mode: vertical-rl; transform: rotate(-180deg);line-height:normal'><span style='font-size:8.0pt;font-family:"Times New Roman","serif"'>Date&nbsp;of&nbsp;issue:</span></p>
      </td>
      <td width=28 nowrap style='width:20.85pt; padding:0cm 5.4pt 0cm 5.4pt;height:46.25pt'>
       <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;writing-mode: vertical-rl; transform: rotate(-180deg);line-height:normal'><span style='font-size:8.0pt;font-family:"Times New Roman","serif";color:black'>Date&nbsp;of&nbsp;expire:</span></p>
      </td>
      <td style='height:86.25pt;border:none' width=0 height=115></td>
     </tr>
     <tr style='page-break-inside:avoid;height:74.25pt'>
      <td width=68 colspan=2 style='width:50.7pt; padding:0cm 5.4pt 0cm 20.4pt;height:74.25pt'>
       <p class=MsoNormal style='margin-bottom:0.5cm;writing-mode: vertical-rl; transform: rotate(-180deg);line-height:normal'><span lang=EN-US style='font-size:8.0pt;font-family:"Times New Roman","serif"; color:black'>&nbsp;&nbsp;This is to &nbsp;&nbsp;certify that:</span></p>
      </td>
      <td width=28 nowrap style='width:20.85pt; padding:0cm 5.4pt 0cm 5.4pt;height:74.25pt'>
       <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;writing-mode: vertical-rl; transform: rotate(-180deg);line-height:normal'><span style='font-size:8.0pt;font-family:"Times New Roman","serif";color:black'>&nbsp;&nbsp;Date&nbsp;of&nbsp;birth:</span></p>
      </td>
      <td width=126 colspan=5 style='width:94.5pt; padding:0cm 5.4pt 0cm 5.4pt;height:74.25pt'></td>
      <td style='height:74.25pt;border:none' width=0 height=99></td>
     </tr>
    </table>
   </div>
  </div>
 </body>
</html>