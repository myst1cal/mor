<?php

namespace app\commands;


use app\models\ars\User;
use yii\console\Controller;
use yii\rbac\DbManager;

class RbacController extends Controller
{
    public function actionInit()
    {
        /** @var DbManager $authManager */
        $authManager = \Yii::$app->authManager;

        // Create roles
        $manager = $authManager->createRole(User::ROLE_MANAGER);
        $accountant = $authManager->createRole(User::ROLE_ACCOUNTANT);
        $admin = $authManager->createRole(User::ROLE_ADMINISTRATOR);

        //$guest->ruleName  = 'guest';
        //$user->ruleName  = 'user';
        //$moderator->ruleName = 'moderator';
        //$admin->ruleName = 'administrator';

        // Add roles in Yii::$app->authManager
        $authManager->add($manager);
        $authManager->add($accountant);
        $authManager->add($admin);

        $user = User::findOne(['login' => 'admin']);
        $role = $authManager->getRole(User::ROLE_ADMINISTRATOR);
        $authManager->assign($role, $user->id);
    }

    public function actionAddMethodist() {
        /** @var DbManager $authManager */
        $authManager = \Yii::$app->authManager;

        // Create roles
        $methodist = $authManager->createRole(User::ROLE_METHODIST);

        $authManager->add($methodist);

    }

    public function actionAddAuditor() {
        /** @var DbManager $authManager */
        $authManager = \Yii::$app->authManager;

        // Create roles
        $auditor = $authManager->createRole(User::ROLE_AUDITOR);

        $authManager->add($auditor);

    }
}