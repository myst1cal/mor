<?php

use app\models\ars\ClientCertificate;
use yii\db\Migration;

/**
 * Class m191110_075945_dropBankSumColumnInClientCertificateTable
 */
class m191110_075945_dropBankSumColumnInClientCertificateTable extends Migration
{
    public function safeUp()
    {
        $this->dropColumn(ClientCertificate::tableName(), 'bank_sum');
    }

    public function safeDown()
    {
        $this->addColumn(ClientCertificate::tableName(), 'bank_sum', $this->decimal(12, 2)->null());
    }
}
