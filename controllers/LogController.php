<?php

namespace app\controllers;

use app\helpers\AuthHelper;
use app\models\ars\Client;
use app\models\ars\ClientCertificate;
use app\models\ars\Log;
use app\models\ars\Middlemen;
use app\models\searches\ClientSearch;
use app\services\LogService;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;


class LogController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['view'],
                        'allow'   => true,
                        'roles'   => AuthHelper::CAN_VIEW_LOGS,
                    ],
                ],
            ],
        ];
    }

    public function actionView()
    {
        $table = Yii::$app->request->get('table');
        $id = Yii::$app->request->get('id');

        $query = Log::find()->where(['table' => $table, 'foreign_id' => $id])->orderBy(['created_at' => SORT_DESC]);

        $dataProvider = new ActiveDataProvider(['query' => $query, 'pagination' => false]);

        return $this->render('view', [
            'dataProvider' => $dataProvider,
        ]);
    }
}
