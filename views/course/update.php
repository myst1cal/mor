<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ars\Course */

$this->title = 'Редактирование курса: ' . $model->title_ukr;
$this->params['breadcrumbs'][] = ['label' => 'Курсы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="course-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'teachers' => $teachers,
        'additionalFields' => $additionalFields,
        'currentTeachers' => $currentTeachers,
        'currentAdditionalFields' => $currentAdditionalFields,
        'currentAdditionalFieldSort' => $currentAdditionalFieldSort,
    ]) ?>

</div>
