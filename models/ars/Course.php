<?php

namespace app\models\ars;

use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "course".
 *
 * @property int $id
 * @property string $title_ukr
 * @property string $title_eng
 * @property string $price
 * @property string $price_2
 * @property string $template
 * @property bool $has_ukr_only [tinyint(1)]
 * @property int $conventional [tinyint(1)]
 *
 * @property CourseAdditionalField[] $courseAdditionalFields
 * @property CourseAdditionalFieldCourse[] $courseAdditionalFieldCourses
 * @property TeacherCourse[] $teacherCourses
 * @property Teacher[] $teachers
 */
class Course extends \yii\db\ActiveRecord
{
    const CONVENTIONAL_YES = 1;
    const CONVENTIONAL_NO = 0;

    public static function getConventionalList()
    {
        return [
            self::CONVENTIONAL_YES => 'Конвенция',
            self::CONVENTIONAL_NO => 'Не конвенция',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'course';
    }

    public static function getList()
    {
        $list = self::find()->all();

        $courses = ArrayHelper::map($list, 'id', 'title_ukr');

        return $courses;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title_ukr', 'title_eng', 'template'], 'required'],
            [['title_ukr', 'title_eng'], 'string'],
            [['price', 'price_2'], 'number'],
            [['has_ukr_only', 'conventional'], 'integer'],
            [['template'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title_ukr' => 'Название(Укр)',
            'title_eng' => 'Название(Eng)',
            'price' => 'Цена (полный курс) P1, грн.',
            'price_2' => 'Цена (укороченный курс) P2, грн.',
            'template' => 'Шаблон',
            'has_ukr_only' => 'Есть только украинский шаблон',
            'conventional' => 'Конвенционный'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeacherCourses()
    {
        return $this->hasMany(TeacherCourse::className(), ['course_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeachers()
    {
        return $this->hasMany(Teacher::className(), ['id' => 'teacher_id'])->viaTable('teacher_course', ['course_id' => 'id']);
    }

    public function getAvailableGroups()
    {
        $groups = Group::find()
            ->leftJoin(ClientCertificate::tableName(), 'group.id=client_certificate.group_id')
            ->where(['group.course_id' => $this->id])
            ->groupBy('group.id')
            ->having('count(client_certificate.id) < group.max_students')
            ->orderBy(['start_date' => SORT_DESC])
            ->all();

        $response = [];
        foreach ($groups as $group) {
            $response[$group->id] = $group->name . ' (' . date('d.m.Y', strtotime($group->start_date)) . ' => ' . date('d.m.Y', strtotime($group->end_date)) . ')';
        }
        return $response;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourseAdditionalFieldCourses()
    {
        return $this->hasMany(CourseAdditionalFieldCourse::className(), ['course_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourseAdditionalFields()
    {
        return $this->hasMany(CourseAdditionalField::className(), ['id' => 'course_additional_field_id'])->viaTable('course_additional_field_course', ['course_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroups()
    {
        return $this->hasMany(Group::className(), ['course_id' => 'id']);
    }
}
