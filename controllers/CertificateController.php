<?php

namespace app\controllers;

use app\helpers\AuthHelper;
use app\models\ars\ClientCertificate;
use app\models\ars\Course;
use app\models\ars\CourseAdditionalFieldClientCertificate;
use app\models\ars\Middlemen;
use app\models\searches\CertificateSearch;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * CertificateController implements the CRUD actions for ClientCertificate model.
 */
class CertificateController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['change-number'],
                        'allow'   => true,
                        'roles'   => AuthHelper::CAN_EDIT_CERTIFICATE_NUMBER,
                    ],
                    [
                        'actions' => ['set-status'],
                        'allow'   => true,
                        'roles'   => array_merge(AuthHelper::CAN_EDIT_CERTIFICATE_PAID, AuthHelper::CAN_EDIT_CERTIFICATE_PRINTED, AuthHelper::CAN_EDIT_CERTIFICATE_ISSUED),
                    ],
                    [
                        'actions' => ['set-middlemen', 'set-additional-data'],
                        'allow'   => true,
                        'roles'   => AuthHelper::CAN_EDIT_CERTIFICATE_ADDITIONAL_DATA,
                    ],
                    [
                        'actions' => ['set-date-of-issue'],
                        'allow'   => true,
                        'roles'   => AuthHelper::CAN_EDIT_CERTIFICATE_ISSUED,
                    ],
                    [
                        'actions' => ['index', 'view'],
                        'allow'   => true,
                        'roles'   => AuthHelper::CAN_SEE_MENU_CERTIFICATES,
                    ],
                    [
                        'actions' => ['list-by-middlemen'],
                        'allow'   => true,
                        'roles'   => AuthHelper::CAN_SEE_MENU_REPORT,
                    ],
                    [
                        'actions' => ['create', 'update', 'add', 'set-group', 'delete', 'set-additional-field'],
                        'allow'   => true,
                        'roles'   => AuthHelper::CAN_EDIT_CERTIFICATE,
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionChangeNumber()
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $id = Yii::$app->request->get('id');
            $number = Yii::$app->request->get('number');

            $certificate = ClientCertificate::findOne($id);

            $certificate->number = $number;

            if ($certificate->save()) {
                return ['status' => 'OK'];
            }

            return ['status' => 'Error', 'messages' => $certificate->getErrors()];
        }
    }

    public function actionSetAdditionalData()
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $id = Yii::$app->request->get('id');
            $attribute = Yii::$app->request->get('attribute');
            $value = Yii::$app->request->get('value');

            $certificate = ClientCertificate::findOne($id);

            $certificate->$attribute = $value;

            if ($certificate->save()) {
                return ['status' => 'OK', 'additionalSum' => $certificate->additional_sum, 'totalSum' => $certificate->total_sum, 'costPrice' => $certificate->priceCost()];
            }

            return ['status' => 'Error', 'messages' => $certificate->getErrors()];
        }
    }

    public function actionSetStatus()
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $id = Yii::$app->request->get('id');
            $attribute = Yii::$app->request->get('attribute');
            $value = Yii::$app->request->get('value');

            $certificate = ClientCertificate::findOne($id);

            $certificate->$attribute = $value;

            if ($certificate->save()) {
                return ['status' => 'OK'];
            }

            return ['status' => 'Error', 'messages' => $certificate->getErrors()];
        }
    }

    public function actionSetGroup()
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $certificateId = Yii::$app->request->get('certificate_id');
            $groupId = Yii::$app->request->get('group_id');

            $certificate = ClientCertificate::findOne($certificateId);

            $certificate->group_id = $groupId;

            if ($certificate->save()) {
                return ['status' => 'OK'];
            }

            return ['status' => 'Error', 'messages' => $certificate->getErrors()];
        }
    }

    public function actionSetDateOfIssue()
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $id = Yii::$app->request->get('id');
            $value = Yii::$app->request->get('value');

            $certificate = ClientCertificate::findOne($id);

            $certificate->date_of_issue = $value;

            if ($certificate->save()) {
                return ['status' => 'OK'];
            }

            return ['status' => 'Error', 'messages' => $certificate->getErrors()];
        }
    }

    public function actionSetMiddlemen()
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $id = Yii::$app->request->get('id');
            $value = Yii::$app->request->get('value');

            $certificate = ClientCertificate::findOne($id);

            $certificate->middlemen_id = $value;

            if ($certificate->save()) {
                return ['status' => 'OK'];
            }

            return ['status' => 'Error', 'messages' => $certificate->getErrors()];
        }
    }

    public function actionSetAdditionalField()
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $certificateId = Yii::$app->request->post('certificate_id');
            $additionalFieldId = Yii::$app->request->post('additional_field_id');
            $value = Yii::$app->request->post('value') == 'true';

            if ($value) {
                $certificate = new CourseAdditionalFieldClientCertificate();

                $certificate->client_certificate_id = $certificateId;
                $certificate->course_additional_field_id = $additionalFieldId;
                $certificate->value = '1';

                if ($certificate->save()) {
                    return ['status' => 'OK'];
                }

                return ['status' => 'Error', 'messages' => $certificate->getErrors()];
            } else {
                $certificate = CourseAdditionalFieldClientCertificate::findOne(['course_additional_field_id' => $additionalFieldId, 'client_certificate_id' => $certificateId]);

                if ($certificate->delete()) {
                    return ['status' => 'OK'];
                }

                return ['status' => 'Error', 'messages' => $certificate->getErrors()];
            }
        }
    }

    /**
     * Lists all ClientCertificate models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CertificateSearch();
        $a = Yii::$app->request->queryParams;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionListByMiddlemen()
    {
        $middlemenId = Yii::$app->request->get('middlemen_id');

        $query = ClientCertificate::find();
        if ($middlemenId) {
            $query = $query->where(['middlemen_id' => $middlemenId]);
        }

        $dataProvider = $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $middlemen = Middlemen::findOne($middlemenId);

        return $this->render('list-by', [
            'middlemen' => $middlemen,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ClientCertificate model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the ClientCertificate model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ClientCertificate the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ClientCertificate::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Creates a new ClientCertificate model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $models = Course::find()->all();

        if (Yii::$app->request->post('Certificate') && Yii::$app->request->get('client_id')) {
            $certificates = Yii::$app->request->post('Certificate');
            $clientId = Yii::$app->request->get('client_id');
            $addDates = Yii::$app->request->post('date_add');

            foreach ($certificates as $courseId => $status) {
                if ($status) {
                    $course = Course::findOne($courseId);
                    $certificate = new ClientCertificate();

                    if (!empty($addDates[$courseId])) {
                        $certificate->date_add = $addDates[$courseId];
                    }
                    $certificate->client_id = $clientId;
                    $certificate->course_id = $courseId;
                    $certificate->cost_price = $course->price;
                    $certificate->cost_price_2 = $course->price_2;

                    $certificate->save();
                }
            }

            return $this->redirect(['client/view', 'id' => $clientId]);
        }

        return $this->render('create', [
            'models' => $models,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        $middlemens = ArrayHelper::map(Middlemen::find()->all(), 'id', 'name');

        return $this->render('update', [
            'model' => $model,
            'middlemens' => $middlemens,
        ]);
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
}
