<?php

namespace app\controllers;

use app\helpers\AuthHelper;
use app\models\ars\ClientCertificate;
use app\models\ars\Course;
use Yii;
use app\models\ars\Group;
use app\models\searches\GroupSearch;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * GroupController implements the CRUD actions for Group model.
 */
class GroupController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view'],
                        'allow'   => true,
                        'roles'   => AuthHelper::CAN_SEE_MENU_GROUPS,
                    ],
                    [
                        'actions' => ['create', 'update'],
                        'allow'   => true,
                        'roles'   => AuthHelper::CAN_EDIT_GROUP,
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Group models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new GroupSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Group model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $certificates = ClientCertificate::findAll(['group_id' => $id]);

        return $this->render('view', [
            'model' => $this->findModel($id),
            'certificates' => $certificates,
        ]);
    }

    /**
     * Creates a new Group model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Group();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        $courses = Course::find()->all();
        return $this->render('create', [
            'model' => $model,
            'courses' => ArrayHelper::map($courses, 'id', 'title_ukr'),
            'currentTeachers' => [],
        ]);
    }

    /**
     * Updates an existing Group model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        /** @var Group $model */
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        $course = $model->course;
        $teachers = ArrayHelper::map($course->teachers, 'id', 'full_name');
        $courses = Course::find()->all();
        return $this->render('update', [
            'model' => $model,
            'courses' => ArrayHelper::map($courses, 'id', 'title_ukr'),
            'currentTeachers' => $teachers,
        ]);
    }

    /**
     * Finds the Group model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Group the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Group::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
