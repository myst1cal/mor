<?php

use yii\db\Migration;

/**
 * Class m190624_192058_addCountertable
 */
class m190624_192058_addCounterTable extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('counter', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'value' => $this->integer()->notNull()->defaultValue(0)->unsigned(),
        ]);
        $this->insert('counter', ['name' => 'certificate']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('counter');
    }
}
