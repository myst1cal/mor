<?php

use app\models\ars\Course;
use yii\db\Migration;

class m211024_142150_add_price_to_course extends Migration
{
    public function safeUp()
    {
        $table = \Yii::$app->db->schema->getTableSchema(Course::tableName());
        if (!isset($table->columns['price_2'])) {
            $this->addColumn(Course::tableName(), 'price_2', $this->decimal(10, 2));
        }

        $this->update(Course::tableName(), ['price_2' => 0.0]);
    }

    public function safeDown()
    {
        $this->dropColumn(Course::tableName(), 'price_2');
    }
}
