<?php

use app\helpers\AuthHelper;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ars\Teacher */

$this->title = $model->full_name;
$this->params['breadcrumbs'][] = ['label' => 'Преподаватели', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="teacher-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Список', ['index', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
        <?= AuthHelper::canEditTeacher() ? Html::a('Редактирование', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) : '' ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'full_name',
            'requirements:ntext',
            'diploma',
            'work_diploma',
            'certificate',
            'experience',
            'rank',
            'ship_type',
            'phone',
        ],
    ]) ?>

</div>
