<?php

use yii\db\Migration;

/**
 * Class m190928_130342_createClientCertificateFieldTable
 */
class m190928_130342_createCourseAdditionalFieldTable extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%course_additional_field}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'additional_data' => $this->text()->null(),
        ], $tableOptions);


        $this->createTable('{{%course_additional_field_course}}', [
            'course_additional_field_id' => $this->integer()->notNull(),
            'course_id' => $this->integer()->notNull(),
            'sort' => $this->integer()->notNull()->defaultValue(0),
        ], $tableOptions);

        $this->createIndex('course_additional_field_i', '{{%course_additional_field_course}}', ['course_additional_field_id', 'course_id'], true);
        $this->createIndex('course_i', '{{%course_additional_field_course}}', ['course_id']);

        $this->addForeignKey('course_additional_field_id_fk', '{{%course_additional_field_course}}', 'course_additional_field_id', '{{%course_additional_field}}', 'id', 'cascade', 'cascade');
        $this->addForeignKey('course_id_fk', '{{%course_additional_field_course}}', 'course_id', '{{%course}}', 'id', 'cascade', 'cascade');


        $this->createTable('{{%course_additional_field_client_certificate}}', [
            'course_additional_field_id' => $this->integer()->notNull(),
            'client_certificate_id' => $this->integer()->notNull(),
            'value' => $this->string()->notNull(),
        ], $tableOptions);

        $this->createIndex('course_additional_field_i', '{{%course_additional_field_client_certificate}}', ['course_additional_field_id', 'client_certificate_id'], true);
        $this->createIndex('client_certificate_i', '{{%course_additional_field_client_certificate}}', ['client_certificate_id']);

        $this->addForeignKey('course_additional_field_id_fk_1', '{{%course_additional_field_client_certificate}}', 'course_additional_field_id', '{{%course_additional_field}}', 'id', 'cascade', 'cascade');
        $this->addForeignKey('client_certificate_id_fk', '{{%course_additional_field_client_certificate}}', 'client_certificate_id', '{{%client_certificate}}', 'id', 'cascade', 'cascade');
    }

    public function safeDown()
    {
        $this->dropForeignKey('course_additional_field_id_fk_1', '{{%course_additional_field_client_certificate}}');
        $this->dropForeignKey('client_certificate_id_fk', '{{%course_additional_field_client_certificate}}');

        $this->dropIndex('course_additional_field_i', '{{%course_additional_field_client_certificate}}');
        $this->dropIndex('client_certificate_i', '{{%course_additional_field_client_certificate}}');

        $this->dropForeignKey('course_additional_field_id_fk', '{{%course_additional_field_course}}');
        $this->dropForeignKey('course_id_fk', '{{%course_additional_field_course}}');

        $this->dropIndex('course_additional_field_i', '{{%course_additional_field_course}}');
        $this->dropIndex('course_i', '{{%course_additional_field_course}}');

        $this->dropTable('{{%course_additional_field_client_certificate}}');
        $this->dropTable('{{%course_additional_field_course}}');
        $this->dropTable('{{%course_additional_field}}');
    }
}
