<?php

namespace app\models\searches;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ars\Client;

/**
 * ClientSearch represents the model behind the search form of `app\models\ars\Client`.
 */
class ClientSearch extends Client
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'created_at', 'updated_at'], 'integer'],
            [['name_ukr', 'patronymic_ukr', 'surname_ukr', 'name_transliteration', 'patronymic_transliteration', 'surname_transliteration', 'birthday', 'phone', 'email', 'comment'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Client::find()->orderBy(['id' => SORT_DESC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'birthday' => $this->birthday,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'name_ukr', $this->name_ukr])
            ->andFilterWhere(['like', 'patronymic_ukr', $this->patronymic_ukr])
            ->andFilterWhere(['like', 'surname_ukr', $this->surname_ukr])
            ->andFilterWhere(['like', 'name_transliteration', $this->name_transliteration])
            ->andFilterWhere(['like', 'patronymic_transliteration', $this->patronymic_transliteration])
            ->andFilterWhere(['like', 'surname_transliteration', $this->surname_transliteration])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'comment', $this->comment]);

        return $dataProvider;
    }
}
