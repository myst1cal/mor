<?php

use yii\db\Migration;

/**
 * Class m190607_164716_addGroupTable
 */
class m190607_164716_addTeacherTable extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%teacher}}', [
            'id'           => $this->primaryKey(),
            'full_name'    => $this->string()->notNull(),
            'requirements' => $this->text()->null(),
            'diploma'      => $this->string()->null(),
            'work_diploma' => $this->string()->null(),
            'certificate'  => $this->string()->null(),
            'experience'   => $this->string()->null(),
            'rank'         => $this->string()->null(),
            'ship_type'    => $this->string()->null(),
            'phone'        => $this->string()->null(),
        ], $tableOptions);

        $this->createTable('{{%teacher_course}}', [
            'teacher_id' => $this->integer()->notNull(),
            'course_id'  => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('teacher_course_i', '{{%teacher_course}}', ['teacher_id', 'course_id'], true);
        $this->createIndex('course_teacher_i', '{{%teacher_course}}', ['course_id']);

        $this->addForeignKey('teacher_course_teacher_id_fk', '{{%teacher_course}}', 'teacher_id', '{{%teacher}}', 'id', 'cascade', 'cascade');
        $this->addForeignKey('teacher_course_course_id_fk', '{{%teacher_course}}', 'course_id', '{{%course}}', 'id', 'cascade', 'cascade');
    }

    public function safeDown()
    {
        $this->dropForeignKey('teacher_course_teacher_id_fk', '{{%teacher_course}}');
        $this->dropForeignKey('teacher_course_course_id_fk', '{{%teacher_course}}');

        $this->dropIndex('teacher_course_i', '{{%teacher_course}}');
        $this->dropIndex('course_teacher_i', '{{%teacher_course}}');

        $this->dropTable('{{%teacher_course}}');
        $this->dropTable('{{%teacher}}');
    }
}
