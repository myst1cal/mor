<?php
/* @var $client \app\models\ars\Client */
/* @var \app\models\ars\ClientCertificate[] $certificates */
/* @var \app\models\ars\Client $client */
?>
<html xmlns:v="urn:schemas-microsoft-com:vml"
xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:w="urn:schemas-microsoft-com:office:word"
xmlns:m="http://schemas.microsoft.com/office/2004/12/omml"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1251">
<meta name=ProgId content=Word.Document>
<meta name=Generator content="Microsoft Word 15">
<meta name=Originator content="Microsoft Word 15">

<style>
<!--div.WORDSECTION1
	{width: 1020 px;
	height: 641 px;}


 /* Font Definitions */
 @font-face
	{font-family:"AGLettericaCondensedC";
	panose-1:2 4 5 3 5 4 6 3 2 4;
	mso-font-charset:1;
	mso-generic-font-family:roman;
	mso-font-pitch:variable;
	mso-font-signature:0 0 0 0 0 0;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-parent:"";
	margin:0cm;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Times New Roman";
	mso-fareast-font-family:"AGLettericaCondensedC";
	mso-fareast-theme-font:minor-fareast;}
p.msonormal0, li.msonormal0, div.msonormal0
	{mso-style-name:msonormal;
	mso-style-unhide:no;
	margin-right:0cm;
	margin-left:0cm;
}

div.WordSection1
	{page:WordSection1;}
.page-break {
    page-break-before: always;
        }
-->
</style>
</head>

<body lang=RU>
	<div class=WordSection1>
		<div align=center>
			<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 style='border-collapse:collapse;mso-yfti-tbllook:1184;  background: url(/images/bg-sert/page1.png) no-repeat; background-size: contain;'>
				<tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;height:75.55pt'>
                    <td width=879 colspan=9 style='width:659.2pt;border:none;padding:0cm 5.4pt 0cm 5.4pt;height:75.55pt;'>&nbsp;</td>
				</tr>
				<tr style='mso-yfti-irow:1'>
					<td width=683 colspan=7 valign=top style='width:513.6pt; height:20.75pt;padding:0cm 5.4pt 0cm 5.4pt'>&nbsp;</td>
					<td width=194 colspan=2 valign=top style='width:145.6pt;padding:0cm 5.4pt 0cm 5.4pt'>
						<p class=MsoNormal style='margin-left:40pt;'><b><span style='font-size:20.0pt;font-family:"AGLettericaCondensedC";'><?= $certificate->number ?></span></b></p>
					</td>
				</tr>
				<tr style='mso-yfti-irow:2'>
					<td width=877 colspan=9 valign=top style='width:659.2pt; height:21.75pt;padding:0cm 5.4pt 0cm 5.4pt'>&nbsp;</td>
				</tr>
				<tr style='mso-yfti-irow:3'>
					<td width=683 colspan=7 valign=top style='width:513.6pt; height:18.75pt;padding:0cm 5.4pt 0cm 5.4pt'>&nbsp;</td>
					<td width=194 colspan=2 valign=top style='width:145.6pt;padding:0cm 5.4pt 0cm 5.4pt'>
						<p class=MsoNormal style='margin-left:19pt;'><b><span style='font-size:16.0pt;font-family:"AGLettericaCondensedC";'><?= $certificate->getDateStart() ?></span></b></p>
					</td>
				</tr>
				<tr border=1 style='mso-yfti-irow:4'>
					<td width=877 colspan=9 valign=top style='width:659.2pt; height:18.75pt;padding:0pt 5.4pt 2pt 5.4pt;'>
					 <p class=MsoNormal style='margin-left:464pt;'><b><span style='font-size:16.0pt;font-family:"AGLettericaCondensedC";'><?= Yii::$app->formatter->asDate($certificate->client->birthday) ?></span></b></p>
					</td>
				</tr>
				<tr style='mso-yfti-irow:5'>
  					<td width=683 colspan=7 valign=top style='width:513.6pt; height:20.75pt;padding:0cm 5.4pt 0cm 5.4pt'>
						<p class=MsoNormal style='margin-left:20pt;'><span style='font-size:22.0pt;font-family:"AGLettericaCondensedC";text-transform: uppercase;'><?= $certificate->client->surname_ukr ?>&nbsp;<?= $certificate->client->name_ukr . ' ' . $certificate->client->patronymic_ukr ?></span></p>
					</td>
					<td width=194 colspan=2 valign=top style='width:145.6pt;padding:0cm 5.4pt 0cm 5.4pt'>
						<p class=MsoNormal style='margin-left:18pt;'><b><span style='font-size:16.0pt;font-family:"AGLettericaCondensedC";'><?= $certificate->getDateEnd() ?></span></b></p>
					</td>
				</tr>
				<tr style='mso-yfti-irow:6'>
  					<td width=877 colspan=9 valign=top style='width:659.2pt; height:280.75pt;padding:0cm 5.4pt 0cm 5.4pt'>
						<p class=MsoNormal style='margin-left:20pt;'><b><span style='font-size:22.0pt;font-family:"AGLettericaCondensedC";'></span></b></p>
					</td>
				</tr>
			</table>
		</div>
	</div>
<div class=page-break></div>
	<div class=WordSection1>
		<div align=center>
			<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 style='border-collapse:collapse;mso-yfti-tbllook:1184;mso-padding-alt:0cm 5.4pt 0cm 5.4pt;background: url(/images/bg-sert/bgspage2.png) no-repeat; background-size: contain;'>
				<tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;height:75.55pt'>
                    <td width=879 colspan=9 style='width:659.2pt;border:none;padding:0cm 5.4pt 0cm 5.4pt;height:75.55pt;'>&nbsp;</td>
				</tr>
				<tr style='mso-yfti-irow:1'>
					<td width=683 colspan=7 valign=top style='width:513.6pt; height:20.75pt;padding:0cm 5.4pt 0cm 5.4pt'>&nbsp;</td>
					<td width=194 colspan=2 valign=top style='width:145.6pt;padding:0cm 5.4pt 0cm 5.4pt'>
						<p class=MsoNormal style='margin-left:40pt;'><b><span style='font-size:20.0pt;font-family:"AGLettericaCondensedC";'><?= $certificate->number ?></span></b></p>
					</td>
				</tr>
<tr style='mso-yfti-irow:2'>
					<td width=877 colspan=9 valign=top style='width:659.2pt; height:21.75pt;padding:0cm 5.4pt 0cm 5.4pt'>&nbsp;</td>
				</tr>
				<tr style='mso-yfti-irow:3'>
					<td width=683 colspan=7 valign=top style='width:513.6pt; height:18.75pt;padding:0cm 5.4pt 0cm 5.4pt'>&nbsp;</td>
					<td width=194 colspan=2 valign=top style='width:145.6pt;padding:0cm 5.4pt 0cm 5.4pt'>
						<p class=MsoNormal style='margin-left:19pt;'><b><span style='font-size:16.0pt;font-family:"AGLettericaCondensedC";'><?= $certificate->getDateStart() ?></span></b></p>
					</td>
				</tr>
				<tr border=1 style='mso-yfti-irow:4'>
					<td width=877 colspan=9 valign=top style='width:659.2pt; height:18.75pt;padding:0pt 5.4pt 2pt 5.4pt;'>
					 <p class=MsoNormal style='margin-left:464pt;'><b><span style='font-size:16.0pt;font-family:"AGLettericaCondensedC";'><?= Yii::$app->formatter->asDate($certificate->client->birthday) ?></span></b></p>
					</td>
				</tr>
				<tr style='mso-yfti-irow:5'>
  					<td width=683 colspan=7 valign=top style='width:513.6pt; height:20.75pt;padding:0cm 5.4pt 0cm 5.4pt'>
						<p class=MsoNormal style='margin-left:20pt;'><span style='font-size:22.0pt;font-family:"AGLettericaCondensedC";text-transform: uppercase;'><?= $certificate->client->name_transliteration ?>&nbsp;<?= $certificate->client->surname_transliteration ?></span></p>
					</td>
					<td width=194 colspan=2 valign=top style='width:145.6pt;padding:0cm 5.4pt 0cm 5.4pt'>
						<p class=MsoNormal style='margin-left:18pt;'><b><span style='font-size:16.0pt;font-family:"AGLettericaCondensedC";'><?= $certificate->getDateEnd() ?></span></b></p>
					</td>
				</tr>
				<tr style='mso-yfti-irow:6'>
  					<td width=877 colspan=9 valign=top style='width:659.2pt; height:280.75pt;padding:0cm 5.4pt 0cm 5.4pt'>
						<p class=MsoNormal style='margin-left:20pt;'><img width=200 height=300 src="<?= $certificate->client->photoUrl() ?>" style="
    padding-top: 0px;"></p>
					</td>
				</tr>
			</table>
		</div>
	</div>
</div>
</body>

</html>
