<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ars\CourseAdditionalField */

$this->title = 'Редактирование доп. поля: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Доп. поля курсов', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="course-additional-field-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
