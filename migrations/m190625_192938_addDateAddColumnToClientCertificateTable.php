<?php

use yii\db\Migration;

/**
 * Class m190625_192937_addDateOfIssueColumnToClientCertificateTable
 */
class m190625_192938_addDateAddColumnToClientCertificateTable extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%client_certificate}}', 'date_add', $this->date()->after('client_id'));

        $certificates = \app\models\ars\ClientCertificate::find()->all();

        foreach ($certificates as $certificate) {
            $certificate->date_add = $certificate->group->start_date;

            $certificate->save();
        }
    }

    public function safeDown()
    {
        $this->dropColumn('{{%client_certificate}}', 'date_add');
    }
}
