<?php


use app\models\ars\Course;
use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $startDate string */
/* @var $endDate string */
/* @var $certificateData array */

$this->title = 'Отчет: статистика';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="report-statistic">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= Html::beginForm('', 'post', ['id' => 'report-by-dates']) ?>
    <div class="form-group">
        <div class="row">
            <div class="col-lg-3">
                <?= DatePicker::widget(
                    [
                        'name' => 'start_date',
                        'options' => ['placeholder' => 'ГГГГ-ММ-ДД', 'autocomplete' => 'off'],
                        'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                        'value' => $startDate,
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'yyyy-mm-dd',
                        ],
                    ]) ?>
            </div>
            <div class="col-lg-3">
                <?= DatePicker::widget(
                    [
                        'name' => 'end_date',
                        'options' => ['placeholder' => 'ГГГГ-ММ-ДД', 'autocomplete' => 'off'],
                        'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                        'value' => $endDate,
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'yyyy-mm-dd',
                        ],
                    ]) ?>
            </div>
            <div class="col-lg-1">
                <?= Html::submitButton('Поиск', ['class' => 'report-by-dates btn btn-success']) ?>
            </div>
        </div>
    </div>
    <?= Html::endForm() ?>

    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <th>Название курса</th>
            <th>Сертификатов, шт.</th>
            <th>Средняя цена, грн.</th>
            <th>Сумма, грн.</th>
            <th>Наличные, грн.</th>
            <th>Безналичный, грн.</th>
        </tr>
        </thead>

        <tbody>
        <?php
        $totalCertificates = 0;
        $totalSum = 0;
        $totalSumCash = 0;
        $totalSumBankTransfer = 0;
        ?>
        <?php foreach ($certificateData as $certificate): ?>
            <tr>
                <td><?= Html::a($certificate['title'], ['report/statistic-by-course', 'start_date' => $startDate, 'end_date' => $endDate, 'course_id' => $certificate['course_id']], ['target' => '_blank']) ?></td>
                <td><?= $certificate['total_certificates'] ?></td>
                <td><?= $certificate['total_sum'] ? number_format($certificate['total_sum'] / $certificate['total_certificates'], 2) : 0 ?></td>
                <td><?= number_format($certificate['total_sum'], 2) ?></td>
                <td><?= number_format($certificate['total_sum_cash'], 2) ?></td>
                <td><?= number_format($certificate['total_sum_bank_transfer'], 2) ?></td>
            </tr>
            <?php $totalCertificates += $certificate['total_certificates']; ?>
            <?php $totalSum += $certificate['total_sum']; ?>
            <?php $totalSumCash += $certificate['total_sum_cash']; ?>
            <?php $totalSumBankTransfer += $certificate['total_sum_bank_transfer']; ?>
        <?php endforeach; ?>
        <tr>
            <td><b>Всего</b></td>
            <td><?= $totalCertificates ?></td>
            <td><?= $totalSum ? number_format($totalSum / $totalCertificates, 2) : 0 ?></td>
            <td><?= number_format($totalSum, 2) ?></td>
            <td><?= number_format($totalSumCash, 2) ?></td>
            <td><?= number_format($totalSumBankTransfer, 2) ?></td>
        </tr>
        </tbody>
    </table>

    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <th></th>
            <th>Сертификатов, шт.</th>
            <th>Сумма, грн.</th>
        </tr>
        </thead>

        <tbody>
        <?php foreach ($conventionalData as $conventional): ?>
            <tr>
                <td><?= Course::getConventionalList()[$conventional['conventional']] ?></td>
                <td><?= $conventional['counter'] ?></td>
                <td><?= number_format($conventional['total_sum'], 2) ?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>


</div>
