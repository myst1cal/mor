﻿<?php

use app\helpers\AuthHelper;
use app\models\ars\ClientCertificate;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\YiiAsset;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\ars\Client */
/* @var $certificate app\models\ars\ClientCertificate */
/* @var $middlemens array */

$i = 0;
$this->title = $model->surname_ukr . ' ' . $model->name_ukr . ' ' . $model->patronymic_ukr;
$this->params['breadcrumbs'][] = ['label' => 'Клиенты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
YiiAsset::register($this);
?>
<div class="client-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Список', ['index'], ['class' => 'btn btn-success']) ?>
        <?= AuthHelper::canEditCertificate() ? Html::a('Добавить сертификаты', ['/certificate/create?client_id=' . $model->id], ['class' => 'btn btn-info']) : '' ?>
        <?= AuthHelper::canEditClient() ? Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) : '' ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'photo_name',
                'format' => 'html',
                'value' => Html::img($model->photoUrl()),
            ],
            'name_ukr',
            'patronymic_ukr',
            'surname_ukr',
            'name_transliteration',
            'surname_transliteration',
            'birthday:date',
            'phone',
            'email:email',
            'passport_series',
            'passport_number',
            'passport_date_issued',
            'passport_issued_by',
            'registration_address',
            'inn',
            [
                'attribute' => 'comment',
                'format' => 'raw',
                'value' => AuthHelper::CAN_EDIT_CLIENT_COMMENT ? Html::textarea('comment', $model->comment, ['class' => 'form-control comment', 'data-id' => $model->id]) : $model->comment,
            ],
        ],
    ]) ?>

    <p>
        <?= AuthHelper::canPrintPersonalCard() ? Html::button(
            'Персональная карта',
            ['class' => 'btn btn-dark personal-card', 'data-client_id' => $model->id]
        ) : '' ?>
        <?= AuthHelper::canPrintStatementAgreementOffer() ? Html::button(
            'Заявление, соглашение, оферта',
            ['class' => 'btn btn-info act-of-work', 'data-client_id' => $model->id]
        ) : '' ?>
        <?= AuthHelper::canPrintPaymentInvoiceActOfWork() ? Html::button(
            'Счет на оплату и акт работ',
            ['class' => 'btn btn-success payment-invoice', 'data-client_id' => $model->id]
        ) : '' ?>
    </p>
    <?php if (isset($certificates)): ?>
        <?php Pjax::begin(['id' => 'certificates-table']); ?>
        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th></th>
                <th>Курс</th>
                <th>Группа</th>
                <th>Клиент</th>
                <th>Доп поля</th>
                <th>Номер</th>
                <?php if (AuthHelper::canEditCertificate()): ?>
                    <th>Дата добавления</th>
                <?php endif; ?>
                <th>Даты обучения</th>
                <th>Прайс, грн</th>
                <?php if (AuthHelper::canSeeCertificateAdditionalData()): ?>
                    <th>Тип цены</th>
                    <th>Скидка, %</th>
                    <th>Сумма, грн</th>
                <?php endif; ?>
                <th>Тип оплаты</th>
                <th>Оплачен</th>
                <th>Распечатан</th>
                <th>Выдан</th>
                <?php if (AuthHelper::canEditCertificateIssued()): ?>
                    <th>Дата&nbsp;получения</th>
                <?php endif; ?>
                <?php if (AuthHelper::canSeeCertificateAdditionalData()): ?>
                    <th>Посредник</th>
                <?php endif; ?>
                <?php if (AuthHelper::canPrintCertificate()): ?>
                    <th></th>
                <?php endif; ?>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($certificates as $certificate): ?>
                <tr data-certificate-id="<?= $certificate->id ?>">
                    <td><?= Html::checkbox('certificate', false, ['class' => 'form-control certificate-check', 'data-id' => $certificate->id]) ?></td>
                    <td><?= ++$i ?></td>
                    <td><?= $certificate->course->title_ukr ?></td>
                    <td><?= AuthHelper::canEditCertificate() && $certificate->group_id ? $certificate->group->name : Html::dropDownList('certificate[' . $certificate->id . ']', false, $certificate->course->getAvailableGroups(), ['prompt' => 'Не выбрано', 'class' => 'form-control change-group', 'data-id' => $certificate->id]) ?></td>
                    <td><?= $certificate->client->getFullNameUkr(true) ?></td>
                    <?php if (AuthHelper::canEditCertificate()): ?>
                        <td>
                            <?php if (count($certificate->course->courseAdditionalFields)): ?>
                                <?= Html::button('Доп. поля', ['class' => "btn btn-primary", 'data-toggle' => "collapse", 'data-target' => "#collapseExample" . $certificate->id, 'aria-expanded' => "false", 'aria-controls' => "collapseExample"]) ?>
                            <?php endif; ?>
                        </td>
                    <?php endif; ?>
                    <td><?= AuthHelper::canEditCertificateNumber() && empty($certificate->number) ? Html::input('text', 'certificateSerialNumber', null, ['class' => 'form-control certificateSerialNumber', 'data-id' => $certificate->id]) : $certificate->number ?></td>
                    <td><?= Yii::$app->formatter->asDate($certificate->date_add) ?></td>
                    <td><?= isset($certificate->group) ? Yii::$app->formatter->asDate($certificate->group->start_date) . ' - ' . Yii::$app->formatter->asDate($certificate->group->end_date) : null ?></td>
                    <td class="cost_price"><?= $certificate->priceCost() ?></td>
                    <?php if (AuthHelper::canSeeCertificateAdditionalData()): ?>
                        <td><?= AuthHelper::canEditCertificateAdditionalData() ? Html::dropDownList('price_type', $certificate->price_type, ClientCertificate::priceTypeList(), ['class' => 'form-control price_type', 'data-id' => $certificate->id, 'data-attr' => 'price_type']) : $certificate->priceTypeText() ?></td>
                        <td><?= AuthHelper::canEditCertificateAdditionalData() ? Html::dropDownList('additional_sum_percent', $certificate->additional_sum_percent, array_combine(range(0, -20, -5), range(0, -20, -5)), ['class' => 'form-control additional-sum-percent', 'data-id' => $certificate->id, 'data-attr' => 'additional_sum_percent']) : $certificate->additional_sum_percent ?></td>
                        <td class="total_sum"><?= $certificate->total_sum ?></td>
                    <?php endif; ?>
                    <td><?= Html::dropDownList('pay_type', $certificate->pay_type, ClientCertificate::payTypeList(), ['class' => 'form-control pay-type', 'data-id' => $certificate->id, 'data-attribute' => 'pay_type', 'prompt' => '...']) ?></td>
                    <td><?= Html::checkbox('paid', $certificate->paid, ['class' => 'form-control ajax-status', 'data-id' => $certificate->id, 'data-attribute' => 'paid',] + (AuthHelper::canEditCertificatePaid() ? [] : ['disabled' => 'disabled'])) ?></td>
                    <td><?= Html::checkbox('printed', $certificate->printed, ['class' => 'form-control ajax-status', 'data-id' => $certificate->id, 'data-attribute' => 'printed',] + (AuthHelper::canEditCertificatePrinted() ? [] : ['disabled' => 'disabled'])) ?></td>
                    <td><?= Html::checkbox('issued', $certificate->issued, ['class' => 'form-control ajax-status', 'data-id' => $certificate->id, 'data-attribute' => 'issued',] + (AuthHelper::canEditCertificateIssued() ? [] : ['disabled' => 'disabled'])) ?></td>

                    <?php if (AuthHelper::canEditCertificateIssued()): ?>
                        <td>
                            <?= DatePicker::widget(
                                [
                                    'name' => 'date_of_issue',
                                    'options' => ['placeholder' => 'ГГГГ-ММ-ДД', 'autocomplete' => 'off', 'class' => 'date-of-issue', 'data-certificate_id' => $certificate->id],
                                    'type' => DatePicker::TYPE_INPUT,
                                    'value' => $certificate->date_of_issue,
                                    'pluginOptions' => [
                                        'autoclose' => true,
                                        'format' => 'yyyy-mm-dd',
                                    ],
                                    'pluginEvents' => [
                                        "changeDate" => "function(e) {
                                            let id = $(this).attr('data-certificate_id');
                                            let value = $(this).val();
                                    
                                            $.ajax({
                                                url: '/certificate/set-date-of-issue',
                                                method: 'get',
                                                data: {id: id, value: value},
                                                dataType: 'json',
                                                success: function (response) {
                                                    if (response.status != 'OK') {
                                                        alert('Не удалось сохранить данные. Попробуйте снова');
                                                    }
                                                }
                                            })
                                        }",
                                    ],
                                ]) ?>
                        </td>
                    <?php endif; ?>
                    <?php if (AuthHelper::canSeeCertificateAdditionalData()): ?>
                        <td><?= Html::dropDownList('middlemen', $certificate->middlemen_id, $middlemens, ['class' => 'form-control middlemen', 'data-id' => $certificate->id, 'prompt' => '...']) ?></td>
                    <?php endif; ?>
                    <?php if (AuthHelper::canPrintCertificate()): ?>
                        <td><?= Html::a('Печать',
                                ['print/certificate'], [
                                    'class' => 'print-button btn btn-success ' . ($certificate->paid ? '' : 'hide'),
                                    'target' => '_blank',
                                    'data-id' => $certificate->id,
                                    'data-method' => 'POST',
                                    'data-params' => [
                                        'certificate_id' => $certificate->id,
                                        'csrf_param' => Yii::$app->request->csrfParam,
                                        'csrf_token' => Yii::$app->request->csrfToken,
                                    ],
                                ]) ?>

                            <?= Html::a('Печать на карте',
                                ['print-card/certificate'], [
                                    'class' => 'print-button btn btn-success ' . ($certificate->paid ? '' : 'hide'),
                                    'target' => '_blank',
                                    'data-id' => $certificate->id,
                                    'data-method' => 'POST',
                                    'data-params' => [
                                        'certificate_id' => $certificate->id,
                                        'csrf_param' => Yii::$app->request->csrfParam,
                                        'csrf_token' => Yii::$app->request->csrfToken,
                                    ],
                                ]) ?>
                            <?php if ($certificate->course->has_ukr_only): ?>
                                <?= Html::a('Печать укр. шаблона',
                                    ['print/certificate'], [
                                        'class' => 'print-button btn btn-info ' . ($certificate->paid ? '' : 'hide'),
                                        'target' => '_blank',
                                        'data-id' => $certificate->id,
                                        'data-method' => 'POST',
                                        'data-params' => [
                                            'certificate_id' => $certificate->id,
                                            'ukr_only' => 1,
                                            'csrf_param' => Yii::$app->request->csrfParam,
                                            'csrf_token' => Yii::$app->request->csrfToken,
                                        ],
                                    ]) ?>
                            <?php endif; ?>
                        </td>
                    <?php endif; ?>
                </tr>
                <?php if (count($certificate->course->courseAdditionalFields)): ?>
                    <tr class="collapse" id="collapseExample<?= $certificate->id ?>">
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td colspan="7">
                            <div>
                                <div class="additional_fields">
                                    <?php foreach ($certificate->course->courseAdditionalFields as $additionalField): ?>
                                        <p>
                                            <label><?= Html::checkbox('additionalField', in_array($additionalField->id, ArrayHelper::getColumn($certificate->courseAdditionalFields, 'id')), ['class' => 'additionalField', 'data-certificate_id' => $certificate->id, 'data-additional_field_id' => $additionalField->id]) . $additionalField->name ?></label>
                                        </p>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </td>
                    </tr>
                <?php endif; ?>
            <?php endforeach; ?>
            </tbody>
        </table>
        <?php Pjax::end(); ?>

        <?php $this->registerJsFile('js/certificate.js', ['depends' => ['yii\web\JqueryAsset']]); ?>
        <?php $this->registerJsFile('js/user.js', ['depends' => ['yii\web\JqueryAsset']]); ?>
    <?php endif; ?>
</div>
