<?php

use yii\db\Migration;

/**
 * Class m190625_192937_addDateOfIssueColumnToClientCertificateTable
 */
class m190625_192937_addDateOfIssueColumnToClientCertificateTable extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%client_certificate}}', 'date_of_issue', $this->date()->null()->after('issued'));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%client_certificate}}', 'date_of_issue');
    }
}
