<?php

use app\models\ars\User;
use yii\db\Migration;

class m190126_172628_addUserTable extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id'                   => $this->primaryKey(),
            'login'                => $this->string()->notNull()->unique(),
            'auth_key'             => $this->string(32)->notNull(),
            'password_hash'        => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'email'                => $this->string()->notNull()->unique(),

            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'deleted'    => $this->smallInteger()->defaultValue(User::NOT_DELETED),
        ], $tableOptions);

        $user = new User();
        $user->login = 'admin';
        $user->email = 'admin@admin.ru';
        $user->setPassword('admin');
        $user->generateAuthKey();
        $user->save(false);
    }

    public function down()
    {
        $this->dropTable('{{%user}}');
    }
}
