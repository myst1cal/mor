<?php


namespace app\helpers;


use app\models\ars\User;
use Yii;

class AuthHelper
{
    const CAN_SEE_MENU_REPORT                   = [User::ROLE_ADMINISTRATOR, User::ROLE_ACCOUNTANT, User::ROLE_AUDITOR];
    const CAN_SEE_MENU_CLIENTS                  = [User::ROLE_ADMINISTRATOR, User::ROLE_ACCOUNTANT, User::ROLE_MANAGER];
    const CAN_SEE_MENU_TEACHERS                 = [User::ROLE_ADMINISTRATOR, User::ROLE_ACCOUNTANT, User::ROLE_MANAGER];
    const CAN_SEE_MENU_MIDDLEMENS               = [User::ROLE_ADMINISTRATOR, User::ROLE_ACCOUNTANT];
    const CAN_SEE_MENU_COURSES                  = [
        User::ROLE_ADMINISTRATOR,
        User::ROLE_ACCOUNTANT,
        User::ROLE_MANAGER,
        User::ROLE_METHODIST
    ];
    const CAN_SEE_MENU_GROUPS                   = [User::ROLE_ADMINISTRATOR, User::ROLE_MANAGER, User::ROLE_METHODIST];
    const CAN_SEE_MENU_USERS                    = [User::ROLE_ADMINISTRATOR];
    const CAN_SEE_MENU_CERTIFICATES             = [User::ROLE_ADMINISTRATOR];
    const CAN_EDIT_CLIENT                       = [User::ROLE_ADMINISTRATOR, User::ROLE_MANAGER];
    const CAN_EDIT_CLIENT_COMMENT               = [User::ROLE_ADMINISTRATOR, User::ROLE_MANAGER];
    const CAN_EDIT_CERTIFICATE                  = [User::ROLE_ADMINISTRATOR, User::ROLE_MANAGER];
    const CAN_SEE_CERTIFICATE_ADDITIONAL_DATA   = [User::ROLE_ADMINISTRATOR, User::ROLE_ACCOUNTANT];
    const CAN_EDIT_CERTIFICATE_ADDITIONAL_DATA  = [User::ROLE_ADMINISTRATOR, User::ROLE_ACCOUNTANT];
    const CAN_EDIT_CERTIFICATE_DATE_ADD         = [User::ROLE_ADMINISTRATOR];
    const CAN_EDIT_CERTIFICATE_NUMBER           = [User::ROLE_ADMINISTRATOR, User::ROLE_ACCOUNTANT, User::ROLE_MANAGER];
    const CAN_PRINT_CERTIFICATE                 = [User::ROLE_ADMINISTRATOR, User::ROLE_ACCOUNTANT, User::ROLE_MANAGER];
    const CAN_PRINT_AGREEMENT                   = [User::ROLE_ADMINISTRATOR, User::ROLE_ACCOUNTANT, User::ROLE_MANAGER];
    const CAN_PRINT_STATEMENT_AGREEMENT_OFFER   = [User::ROLE_ADMINISTRATOR, User::ROLE_ACCOUNTANT, User::ROLE_MANAGER];
    const CAN_PRINT_PERSONAL_CARD               = [User::ROLE_ADMINISTRATOR, User::ROLE_ACCOUNTANT, User::ROLE_MANAGER];
    const CAN_PRINT_PAYMENT_INVOICE_ACT_OF_WORK = [User::ROLE_ADMINISTRATOR, User::ROLE_ACCOUNTANT, User::ROLE_MANAGER];
    const CAN_PRINT_GROUP                       = [
        User::ROLE_ADMINISTRATOR,
        User::ROLE_ACCOUNTANT,
        User::ROLE_MANAGER,
        User::ROLE_METHODIST
    ];
    const CAN_EDIT_CERTIFICATE_PAID             = [User::ROLE_ADMINISTRATOR, User::ROLE_ACCOUNTANT];
    const CAN_EDIT_CERTIFICATE_PRINTED          = [User::ROLE_ADMINISTRATOR, User::ROLE_MANAGER];
    const CAN_EDIT_CERTIFICATE_ISSUED           = [User::ROLE_ADMINISTRATOR, User::ROLE_MANAGER];
    const CAN_EDIT_TEACHER                      = [User::ROLE_ADMINISTRATOR];
    const CAN_EDIT_MIDDLEMEN                    = [User::ROLE_ADMINISTRATOR];
    const CAN_EDIT_COURSE                       = [User::ROLE_ADMINISTRATOR];
    const CAN_EDIT_GROUP                        = [User::ROLE_ADMINISTRATOR, User::ROLE_METHODIST, User::ROLE_MANAGER];
    const CAN_VIEW_LOGS                         = [User::ROLE_ADMINISTRATOR];

    public static function canSeeMenuReport(User $user = null)
    {
        if (empty($user)) {
            $user = Yii::$app->user->getIdentity();
        }

        $role = $user->getRole();

        return in_array($role, self::CAN_SEE_MENU_REPORT);
    }

    public static function canSeeMenuClients(User $user = null)
    {
        if (empty($user)) {
            $user = Yii::$app->user->getIdentity();
        }

        $role = $user->getRole();

        return in_array($role, self::CAN_SEE_MENU_CLIENTS);
    }

    public static function canSeeMenuTeachers(User $user = null)
    {
        if (empty($user)) {
            $user = Yii::$app->user->getIdentity();
        }

        $role = $user->getRole();

        return in_array($role, self::CAN_SEE_MENU_TEACHERS);
    }

    public static function canSeeMenuMiddlemens(User $user = null)
    {
        if (empty($user)) {
            $user = Yii::$app->user->getIdentity();
        }

        $role = $user->getRole();

        return in_array($role, self::CAN_SEE_MENU_MIDDLEMENS);
    }

    public static function canSeeMenuCourses(User $user = null)
    {
        if (empty($user)) {
            $user = Yii::$app->user->getIdentity();
        }

        $role = $user->getRole();

        return in_array($role, self::CAN_SEE_MENU_COURSES);
    }

    public static function canSeeMenuGroups(User $user = null)
    {
        if (empty($user)) {
            $user = Yii::$app->user->getIdentity();
        }

        $role = $user->getRole();

        return in_array($role, self::CAN_SEE_MENU_GROUPS);
    }

    public static function canSeeMenuUsers(User $user = null)
    {
        if (empty($user)) {
            $user = Yii::$app->user->getIdentity();
        }

        $role = $user->getRole();

        return in_array($role, self::CAN_SEE_MENU_USERS);
    }

    public static function canSeeMenuCertificates(User $user = null)
    {
        if (empty($user)) {
            $user = Yii::$app->user->getIdentity();
        }

        $role = $user->getRole();

        return in_array($role, self::CAN_SEE_MENU_CERTIFICATES);
    }

    public static function canEditClient(User $user = null)
    {
        if (empty($user)) {
            $user = Yii::$app->user->getIdentity();
        }

        $role = $user->getRole();

        return in_array($role, self::CAN_EDIT_CLIENT);
    }

    public static function canEditClientComment(User $user = null)
    {
        if (empty($user)) {
            $user = Yii::$app->user->getIdentity();
        }

        $role = $user->getRole();

        return in_array($role, self::CAN_EDIT_CLIENT_COMMENT);
    }

    public static function canEditCertificate(User $user = null)
    {
        if (empty($user)) {
            $user = Yii::$app->user->getIdentity();
        }

        $role = $user->getRole();

        return in_array($role, self::CAN_EDIT_CERTIFICATE);
    }

    public static function canSeeCertificateAdditionalData(User $user = null)
    {
        if (empty($user)) {
            $user = Yii::$app->user->getIdentity();
        }

        $role = $user->getRole();

        return in_array($role, self::CAN_SEE_CERTIFICATE_ADDITIONAL_DATA);
    }

    public static function canEditCertificateAdditionalData(User $user = null)
    {
        if (empty($user)) {
            $user = Yii::$app->user->getIdentity();
        }

        $role = $user->getRole();

        return in_array($role, self::CAN_EDIT_CERTIFICATE_ADDITIONAL_DATA);
    }

    public static function canEditCertificateDateAdd(User $user = null)
    {
        if (empty($user)) {
            $user = Yii::$app->user->getIdentity();
        }

        $role = $user->getRole();

        return in_array($role, self::CAN_EDIT_CERTIFICATE_DATE_ADD);
    }

    public static function canEditCertificateNumber(User $user = null)
    {
        if (empty($user)) {
            $user = Yii::$app->user->getIdentity();
        }

        $role = $user->getRole();

        return in_array($role, self::CAN_EDIT_CERTIFICATE_NUMBER);
    }

    public static function canPrintCertificate(User $user = null)
    {
        if (empty($user)) {
            $user = Yii::$app->user->getIdentity();
        }

        $role = $user->getRole();

        return in_array($role, self::CAN_PRINT_CERTIFICATE);
    }

    public static function canPrintAgreement(User $user = null)
    {
        if (empty($user)) {
            $user = Yii::$app->user->getIdentity();
        }

        $role = $user->getRole();

        return in_array($role, self::CAN_PRINT_AGREEMENT);
    }

    public static function canPrintStatementAgreementOffer(User $user = null)
    {
        if (empty($user)) {
            $user = Yii::$app->user->getIdentity();
        }

        $role = $user->getRole();

        return in_array($role, self::CAN_PRINT_STATEMENT_AGREEMENT_OFFER);
    }

    public static function canPrintPersonalCard(User $user = null)
    {
        if (empty($user)) {
            $user = Yii::$app->user->getIdentity();
        }

        $role = $user->getRole();

        return in_array($role, self::CAN_PRINT_PERSONAL_CARD);
    }

    public static function canPrintPaymentInvoiceActOfWork(User $user = null)
    {
        if (empty($user)) {
            $user = Yii::$app->user->getIdentity();
        }

        $role = $user->getRole();

        return in_array($role, self::CAN_PRINT_PAYMENT_INVOICE_ACT_OF_WORK);
    }

    public static function canPrintGroup(User $user = null)
    {
        if (empty($user)) {
            $user = Yii::$app->user->getIdentity();
        }

        $role = $user->getRole();

        return in_array($role, self::CAN_PRINT_GROUP);
    }

    public static function canEditCertificatePaid(User $user = null)
    {
        if (empty($user)) {
            $user = Yii::$app->user->getIdentity();
        }

        $role = $user->getRole();

        return in_array($role, self::CAN_EDIT_CERTIFICATE_PAID);
    }

    public static function canEditCertificatePrinted(User $user = null)
    {
        if (empty($user)) {
            $user = Yii::$app->user->getIdentity();
        }

        $role = $user->getRole();

        return in_array($role, self::CAN_EDIT_CERTIFICATE_PRINTED);
    }

    public static function canEditCertificateIssued(User $user = null)
    {
        if (empty($user)) {
            $user = Yii::$app->user->getIdentity();
        }

        $role = $user->getRole();

        return in_array($role, self::CAN_EDIT_CERTIFICATE_ISSUED);
    }

    public static function canEditTeacher(User $user = null)
    {
        if (empty($user)) {
            $user = Yii::$app->user->getIdentity();
        }

        $role = $user->getRole();

        return in_array($role, self::CAN_EDIT_TEACHER);
    }

    public static function canEditMiddlemen(User $user = null)
    {
        if (empty($user)) {
            $user = Yii::$app->user->getIdentity();
        }

        $role = $user->getRole();

        return in_array($role, self::CAN_EDIT_MIDDLEMEN);
    }

    public static function canEditCourse(User $user = null)
    {
        if (empty($user)) {
            $user = Yii::$app->user->getIdentity();
        }

        $role = $user->getRole();

        return in_array($role, self::CAN_EDIT_COURSE);
    }

    public static function canEditGroup(User $user = null)
    {
        if (empty($user)) {
            $user = Yii::$app->user->getIdentity();
        }

        $role = $user->getRole();

        return in_array($role, self::CAN_EDIT_GROUP);
    }

    public static function canViewLogs(User $user = null)
    {
        if (empty($user)) {
            $user = Yii::$app->user->getIdentity();
        }

        $role = $user->getRole();

        return in_array($role, self::CAN_VIEW_LOGS);
    }
}