<?php

use app\models\ars\Client;
use app\models\ars\ClientCertificate;
use app\models\ars\Course;
use yii\db\Migration;

class m210307_142150_add_pay_type_and_conventional extends Migration
{
    public function safeUp()
    {
        $this->addColumn(ClientCertificate::tableName(), 'pay_type', $this->smallInteger()->null());
        $this->addColumn(Course::tableName(), 'conventional', $this->smallInteger()->null());
    }

    public function safeDown()
    {
        $this->dropColumn(ClientCertificate::tableName(), 'pay_type');
        $this->dropColumn(Course::tableName(), 'conventional');
    }
}
