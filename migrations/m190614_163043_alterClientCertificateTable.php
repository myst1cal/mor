<?php

use yii\db\Migration;

/**
 * Class m190614_163043_alterClientCertificateTable
 */
class m190614_163043_alterClientCertificateTable extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('client_certificate', 'status');

        $this->addColumn('client_certificate', 'printed', $this->tinyInteger()->defaultValue(0)->after('paid'));
        $this->addColumn('client_certificate', 'additional_sum_percent', $this->tinyInteger()->defaultValue(0)->after('additional_sum'));
    }

    public function safeDown()
    {
        $this->dropColumn('client_certificate', 'printed');
        $this->dropColumn('client_certificate', 'additional_sum_percent');

        $this->addColumn('client_certificate', 'status', $this->integer()->notNull());
    }
}
