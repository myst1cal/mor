<?php

use kartik\date\DatePicker;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ars\ClientCertificate */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="client-certificate-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-lg-4">
            <?= $form->field($model, 'group_id')->dropDownList($model->course->getAvailableGroups()) ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'client_id')->dropDownList([$model->client_id => $model->client->getFullNameUkr(true)], ['readonly' => 'readonly']) ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'number')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-2">
            <?= $form->field($model, 'cost_price')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-2">
            <?= $form->field($model, 'additional_sum')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-2">
            <?= $form->field($model, 'additional_sum_percent')->textInput() ?>
        </div>
        <div class="col-lg-2">
            <?= $form->field($model, 'total_sum')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3">
            <?= $form->field($model, 'date_add')->widget(DatePicker::classname(),
                [
                        'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                        'options' => ['autocomplete' => 'off'],
                        'pluginOptions' => ['autoclose' => true, 'format' => 'yyyy-mm-dd']
                ]) ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($model, 'date_of_issue')->widget(DatePicker::classname(),
                [
                        'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                        'options' => ['autocomplete' => 'off'],
                        'pluginOptions' => ['autoclose' => true, 'format'    => 'yyyy-mm-dd']
                ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-2">
            <?= $form->field($model, 'paid')->checkbox() ?>
        </div>
        <div class="col-lg-2">
            <?= $form->field($model, 'printed')->checkbox() ?>
        </div>
        <div class="col-lg-2">
            <?= $form->field($model, 'issued')->checkbox() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4">
            <?= $form->field($model, 'middlemen_id')->dropDownList($middlemens, ['prompt' => '...']) ?>
        </div>
        <div class="col-lg-4">

        </div>
        <div class="col-lg-4">

        </div>
    </div>


    <div class="form-group">
        <?= \yii\helpers\Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
