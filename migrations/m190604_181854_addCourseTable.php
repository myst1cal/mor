<?php

use yii\db\Migration;

/**
 * Class m190604_181854_addCourseTable
 */
class m190604_181854_addCourseTable extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%course}}', [
            'id'        => $this->primaryKey(),
            'title_ukr' => $this->text()->notNull(),
            'title_eng' => $this->text()->notNull(),
            'price'     => $this->decimal(10, 2),
            'template'  => $this->string()->notNull(),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%course}}');
    }
}
