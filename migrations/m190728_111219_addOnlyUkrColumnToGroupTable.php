<?php

use yii\db\Migration;

/**
 * Class m190728_111219_addOnlyUkrColumnToGroupTable
 */
class m190728_111219_addOnlyUkrColumnToGroupTable extends Migration
{

    public function safeUp()
    {
        $this->addColumn('course', 'has_ukr_only', $this->boolean()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn('course', 'has_ukr_only');
    }
}
