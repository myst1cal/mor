<?php
/** @var $certificate \app\models\ars\ClientCertificate */
?>
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1251">
<meta name=Generator content="Microsoft Word 14 (filtered)">
<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin-top:0cm;
	margin-right:0cm;
	margin-bottom:10.0pt;
	margin-left:0cm;
	line-height:115%;
	font-size:11.0pt;
	font-family:"Calibri","sans-serif";}
.MsoChpDefault
	{font-family:"Calibri","sans-serif";}
.MsoPapDefault
	{margin-bottom:10.0pt;
	line-height:115%;}
@page WordSection1
	{size:21.0cm 419.55pt;
	margin:0cm 0cm 0cm 0cm;}
div.WordSection1
	{page:WordSection1;}
-->
</style>
</head>

<body>
 <div class=WordSection1>
  <div align=center>
   <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=809 style='width:606.9pt;border-collapse:collapse'>
    <tr style='height:68.25pt'>
     <td width=123 nowrap colspan=5 align=center valign=center style='width:92.25pt;padding:0cm 5.4pt 0cm 5.4pt;height:68.25pt'><p class=MsoNormal align=center ><span style='font-size:8.0pt; font-family:"Times New Roman","serif"'>м.п. <br />закладу</span></p></td>
     <td width=51 rowspan=7 style='width:38.1pt;padding:0cm 5.4pt 0cm 5.4pt;height:419.25pt'>
      <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt; writing-mode: vertical-rl; text-align:center;line-height:normal'><b><span style='font-size:10.0pt;font-family:"Times New Roman","serif"'>ПІДГОТОВКА ТА ІНСТРУКТАЖ З ПИТАНЬ ОХОРОНИ ДЛЯ ВСІХ МОРЯКІВ</span></b></p>
     </td>
     <td width=28 rowspan=7 style='width:20.85pt;padding:0cm 5.4pt 0cm 5.4pt;height:419.25pt'>
      <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt; writing-mode: vertical-rl; text-align:center;line-height:normal'><span style='font-size:8.0pt; font-family:"Times New Roman","serif"'>успішно пройшов підготовку за схваленим дистанційним теоретичним курсом</span></p>
     </td>
     <td width=28 nowrap style='width:20.85pt;padding:0cm 5.4pt 0cm 5.4pt;height:68.25pt'>
      <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt; writing-mode: vertical-rl; text-align:center;line-height:normal'><span style='font-size:8.0pt; font-family:"Times New Roman","serif"'>Дата народження:</span></p>
     </td>
     <td width=66 nowrap colspan=2 valign=center style='width:49.2pt;padding:0cm 5.4pt 0cm 20.0pt;height:88.25pt'>
      <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;line-height: normal; writing-mode: vertical-rl;'><span style='font-size:8.0pt;font-family:"Times New Roman","serif"; color:black'>Цим засвідчується, що:</span></p>
     </td>
     <td width=34 nowrap rowspan=7 valign=center style='width:25.35pt;padding:0cm 5.4pt 0cm 5.4pt;height:419.25pt'>
      <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt; writing-mode: vertical-rl; text-align:center;line-height:normal'><b><span style='font-size:12.0pt; font-family:"Times New Roman","serif"'>СВІДОЦТВО </span><span style='display: inline-block;transform: rotate(90deg);font-size:12.0pt; font-family:"Times New Roman","serif"'>№ </span><span style='font-size:12.0pt; font-family:"Times New Roman","serif"'> <?= $certificate->number ?></span></b></p>
     </td>
     <td width=83 nowrap rowspan=7 valign=bottom style='width:62.45pt;padding:0cm 5.4pt 0cm 5.4pt;height:68.25pt'></td>
     <td width=84 nowrap rowspan=7 valign=bottom style='width:63.25pt;padding:0cm 5.4pt 0cm 5.4pt;height:68.25pt'></td>
     <td width=34 nowrap rowspan=7 valign=center style='width:25.35pt;padding:0cm 5.4pt 0cm 5.4pt;height:419.25pt'>
      <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt; writing-mode: vertical-lr; transform: rotate(-180deg);text-align:center;line-height:normal'><b><span style='font-size:12.0pt; font-family:"Times New Roman","serif"'>CERTIFICATE </span><span style='display: inline-block;transform: rotate(90deg);font-size:12.0pt; font-family:"Times New Roman","serif"'>№ </span><span style='font-size:12.0pt; font-family:"Times New Roman","serif"'> <?= $certificate->number ?></span></b></p>
     </td>
     <td width=34 nowrap rowspan=6 valign=center style='width:25.35pt;padding:0cm 2.4pt 0cm 2.4pt; height:68.25pt'>
      <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;writing-mode: vertical-lr; transform: rotate(-180deg);text-align:center;line-height:normal'><b><i><span style='font-size:14.0pt;font-family:"Times New Roman","serif";'><?= $certificate->client->name_transliteration ?></span></i></b></p>
     </td>
     <td width=34 nowrap rowspan=6 valign=center style='width:25.35pt;padding:0cm 2.4pt 0cm 2.4pt; height:68.25pt'>
      <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;writing-mode: vertical-lr; transform: rotate(-180deg);text-align:center; line-height:normal'><b><i><span style='font-size:14.0pt;font-family:"Times New Roman","serif";'><?= $certificate->client->surname_transliteration ?></span></i></b></p>
     </td>
     <td width=28 nowrap rowspan=6 valign=center style='width:20.85pt;padding:0cm 2.4pt 0cm 2.4pt; height:68.25pt'>
      <p class=MsoNormal align=center style='margin-bottom: 0cm;margin-bottom:.0001pt;writing-mode: vertical-lr; transform: rotate(-180deg);line-height:normal'><b><i><span style='font-size:14.0pt;font-family:"Times New Roman","serif";'><?= Yii::$app->formatter->asDate($certificate->client->birthday) ?></span></i></b></p>
     </td>
     <td width=28 rowspan=7 style='width:20.85pt;padding:0cm 5.4pt 0cm 5.4pt;height:419.25pt'>
      <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;writing-mode: vertical-lr; transform: rotate(-180deg); text-align:center;line-height:normal'><span lang=EN-US style='font-size:8.0pt; font-family:"Times New Roman","serif"'>has successfully completed the approved distance theoretical course of training </span></p>
     </td>
     <td width=30 rowspan=7 style='width:22.35pt;padding:0cm 5.4pt 0cm 5.4pt;height:419.25pt'>
      <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;writing-mode: vertical-rl; transform: rotate(-180deg); text-align:center;line-height:normal'><b><span lang=EN-US style='font-size: 10.0pt;font-family:"Times New Roman","serif"'>SECURITY - RELATED TRAINING AND INSTRUCTION FOR ALL SEAFARERS</span></b></p>
     </td>
     <td width=28 rowspan=6 valign=bottom style='width:20.85pt;padding:0cm 5.4pt 0cm 5.4pt;height:351.25pt'>
      <p class=MsoNormal align=center style='margin-bottom:0.7cm;writing-mode: vertical-rl; transform: rotate(-180deg); text-align:left;line-height:normal'><span lang=EN-US style='font-size:8.0pt; font-family:"Times New Roman","serif"'>based on IMO сourse 3.27 "Security awareness training for all seafarers", edition 2012 and in accordance with the provisions of Regulation VI/6 paragraphs 1, 2 of the STCW Convention, section A-VI/6 paragraphs 1-4 of the STCW Code</span></p>
     </td>
     <td width=28 rowspan=6 valign=center style='width:20.85pt;padding:0cm 5.4pt 0cm 5.4pt;height:351.25pt'>
      <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;writing-mode: vertical-rl; transform: rotate(-180deg); text-align:center;line-height:normal'><span style='font-size:8.0pt; font-family:"Times New Roman","serif"'>Manager..............………             Registrar…….…………</span></p>
     </td>
     <td width=56 nowrap colspan=2 rowspan=4 valign=bottom style='width:41.7pt;padding:0cm 5.4pt 0cm 5.4pt; height:68.25pt'></td>
     <td width=15 rowspan=6 valign=bottom style='width:11.1pt;padding:0cm 5.4pt 0cm 5.4pt;height:68.25pt'></td>
     <td style='height:68.25pt;border:none' width=0 height=91></td>
	</tr>
	<tr style='height:48.0pt'>
     <td width=15 rowspan=6 valign=top style='width:11.1pt;padding:0cm 5.4pt 0cm 5.4pt;height:48.0pt'></td>
     <td width=28 nowrap style='width:20.85pt; padding:0cm 5.4pt 0cm 5.4pt;height:48.0pt'>
      <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;writing-mode: vertical-rl;line-height: normal'><span style='font-size:8.0pt;font-family:"Times New Roman","serif"'>Дійсно до:</span></p>
     </td>
     <td width=28 nowrap style='width:20.85pt; padding:0cm 5.4pt 0cm 5.4pt;height:48.0pt'>
      <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;writing-mode: vertical-rl;line-height: normal'><span style='font-size:8.0pt;font-family:"Times New Roman","serif"'>Дата видачі:</span></p>
     </td>
	 <td width=25 rowspan=6 valign=center style='width:18.6pt;padding:0cm 5.4pt 0cm 5.4pt;height:351.0pt'>
      <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;writing-mode: vertical-rl;text-align:center;line-height:normal'><b><i><span style='font-size:6.0pt; font-family:"Times New Roman","serif"'>Керівник
  ..…………………………………                  Реєстратор.……………........................</span></i></b></p>
     </td>
     <td width=28 rowspan=6 valign=top style='width:20.85pt; padding:0cm 5.4pt 0cm 5.4pt;height:351.0pt'>
      <p class=MsoNormal align=center style='margin-bottom:0.5cm;writing-mode: vertical-rl; text-align:left;line-height:normal'><span style='font-size:8.0pt; font-family:"Times New Roman","serif"'>який базується на типовому курсі IMO 3.27 "Підготовка та інструктаж з питань охорони для усіх моряків", видання 2012 року та у відповідності з вимогами Правила VI/6 пункти 1, 2 Конвенції ПДНВ, розділу А-VI/6 пункти 1-4 Кодексу ПДНВ</span></p>
     </td>
     <td width=28 nowrap rowspan=6 style='width:20.85pt; padding:0cm 2.4pt 0cm 2.4pt;'>
      <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;writing-mode: vertical-rl;line-height: normal'><b><i><span style='font-size:14.0pt;font-family:"Times New Roman","serif"'><?= Yii::$app->formatter->asDate($certificate->client->birthday) ?></span></i></b></p>
     </td>
     <td width=33 nowrap rowspan=6 style='width:24.6pt; padding:0cm 2.4pt 0cm 2.4pt;'>
      <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt; writing-mode: vertical-rl; text-align:center;line-height:normal'><b><i><span style='font-size:14.0pt;font-family:"Times New Roman","serif"'><?= $certificate->client->name_ukr . '&nbsp;' . $certificate->client->patronymic_ukr ?></span></i></b></p>
     </td>
     <td width=33 nowrap rowspan=6 style='width:24.6pt; padding:0cm 2.4pt 0cm 2.4pt;'>
      <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;writing-mode: vertical-rl; text-align:center;line-height:normal'><b><i><span style='font-size:14.0pt;font-family:"Times New Roman","serif";'><?= $certificate->client->surname_ukr ?></span></i></b></p>
     </td>
     <td style='border:none' width=0></td>
	</tr>
	<tr style='height:47.25pt'>
	 <td width=28 nowrap style='width:20.85pt; padding:0cm 5.4pt 0cm 5.4pt;height:47.25pt'>
      <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt; writing-mode: vertical-rl;line-height: normal'><b><span style='font-size:8.0pt;font-family:"Times New Roman","serif"'><?= $certificate->getDateEnd() ?></span></b></p>
     </td>
     <td width=28 nowrap style='width:20.85pt; padding:0cm 5.4pt 0cm 5.4pt;height:47.25pt'>
      <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt; writing-mode: vertical-rl; line-height: normal'><b><span style='font-size:8.0pt;font-family:"Times New Roman","serif"'><?= $certificate->getDateStart() ?></span></b></p>
     </td>
     <td style='height:47.25pt;border:none' width=0 height=63></td>
	</tr>
	<tr style='height:41.25pt'>
	 <td width=56 nowrap colspan=2 rowspan=4 valign=bottom style='width:41.7pt; padding:0cm 5.4pt 0cm 5.4pt;height:41.25pt'></td>
     <td style='height:41.25pt;border:none' width=0 height=55></td>
	</tr>
	<tr style='height:48.0pt'>
	 <td width=28 nowrap style='width:20.85pt; padding:0cm 5.4pt 0cm 5.4pt;height:48.0pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;writing-mode: vertical-rl; transform: rotate(-180deg);line-height: normal'><b><span style='font-size:8.0pt;font-family:"Times New Roman","serif"'><?= $certificate->getDateStart() ?></span></b></p>
	 </td>
     <td width=28 nowrap style='width:20.85pt; padding:0cm 5.4pt 0cm 5.4pt;height:48.0pt'>
	  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;writing-mode: vertical-rl; transform: rotate(-180deg);line-height:normal'><b><span style='font-size:8.0pt;font-family:"Times New Roman","serif"; color:black'><?= $certificate->getDateEnd() ?></span></b></p>
     </td>
      <td style='height:48.0pt;border:none' width=0 height=64></td>
     </tr>
     <tr style='height:46.25pt'>
      <td width=28 style='width:20.85pt; padding:0cm 5.4pt 0cm 5.4pt;height:46.25pt'>
       <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;writing-mode: vertical-rl; transform: rotate(-180deg);line-height:normal'><span style='font-size:8.0pt;font-family:"Times New Roman","serif"'>Date&nbsp;of&nbsp;issue:</span></p>
      </td>
      <td width=28 nowrap style='width:20.85pt; padding:0cm 5.4pt 0cm 5.4pt;height:46.25pt'>
       <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;writing-mode: vertical-rl; transform: rotate(-180deg);line-height:normal'><span style='font-size:8.0pt;font-family:"Times New Roman","serif";color:black'>Date&nbsp;of&nbsp;expire:</span></p>
      </td>
      <td style='height:86.25pt;border:none' width=0 height=115></td>
     </tr>
     <tr style='page-break-inside:avoid;height:74.25pt'>
      <td width=68 colspan=2 style='width:50.7pt; padding:0cm 5.4pt 0cm 20.4pt;height:74.25pt'>
       <p class=MsoNormal style='margin-bottom:0.5cm;writing-mode: vertical-rl; transform: rotate(-180deg);line-height:normal'><span lang=EN-US style='font-size:8.0pt;font-family:"Times New Roman","serif"; color:black'>This is to certify that:</span></p>
      </td>
      <td width=28 nowrap style='width:20.85pt; padding:0cm 5.4pt 0cm 5.4pt;height:74.25pt'>
       <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;writing-mode: vertical-rl; transform: rotate(-180deg);line-height:normal'><span style='font-size:8.0pt;font-family:"Times New Roman","serif";color:black'>Date&nbsp;of&nbsp;birth:</span></p>
      </td>
      <td width=126 colspan=5 style='width:94.5pt; padding:0cm 5.4pt 0cm 5.4pt;height:74.25pt'></td>
      <td style='height:74.25pt;border:none' width=0 height=99></td>
     </tr>
    </table>
   </div>
  </div>
 </body>
</html>