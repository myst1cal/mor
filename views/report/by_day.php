<?php

use app\models\ars\ClientCertificate;
use kartik\date\DatePicker;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\searches\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $certificate ClientCertificate */
/* @var $sum integer */

$this->title = 'Отчет: дневной';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="report-by_day">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= Html::beginForm('', 'post', ['id' => 'report-by-day']) ?>
    <div class="form-group">
        <div class="row">
            <div class="col-lg-3">
                <?= DatePicker::widget(
                    [
                        'name'          => 'day',
                        'options'       => ['placeholder' => 'ГГГГ-ММ-ДД', 'autocomplete' => 'off'],
                        'type'          => DatePicker::TYPE_COMPONENT_PREPEND,
                        'value'         => $day,
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format'    => 'yyyy-mm-dd',
                        ],
                    ]) ?>
            </div>
            <?= Html::hiddenInput('to_xls', 0, ['class' => 'toXls'])?>
            <div class="col-lg-1">
                <?= Html::button('Поиск', ['class' => 'report-by-day btn btn-success']) ?>
            </div>
            <div class="col-lg-1">
                <?= Html::button('В excel', ['class' => 'report-by-day-to-xls btn btn-info']) ?>
            </div>
        </div>
    </div>
    <?= Html::endForm() ?>

    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <th>ФИО</th>
            <th>Сумма, грн.</th>
        </tr>
        </thead>

        <tbody>
        <?php foreach ($data as $certificate): ?>
            <tr>
                <td><?= $certificate['surname_ukr'] . ' ' . $certificate['name_ukr'] . ' ' . $certificate['patronymic_ukr'] ?></td>
                <td><?= $certificate['total_sum'] ?></td>
            </tr>
        <?php endforeach; ?>
        <?php if ($sum): ?>
        <tr>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>Всего</td>
            <td><?= $sum ?></td>
        </tr>
        <?php endif; ?>
        </tbody>
    </table>

    <?php $this->registerJsFile('js/report-by-day.js', ['depends' => ['yii\web\JqueryAsset']]); ?>
</div>
