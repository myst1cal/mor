<?php

use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $middlemen \app\models\ars\Middlemen */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $model \app\models\ars\ClientCertificate */

$this->title = 'Сертификаты' . (isset($middlemen) ? ': посредник ' . $middlemen->name : '' );
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="client-certificate-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'group_id',
                'value' => function($model) {
                    /** @var $model \app\models\ars\ClientCertificate */
                    return isset($model->group) ? $model->group->name : '';
                }
            ],
            [
                'attribute' => 'client_id',
                'value' => function($model) {
                    /** @var $model \app\models\ars\ClientCertificate */
                    return $model->client->getFullNameUkr(true);
                }
            ],
            'number',
            'cost_price',
            'additional_sum',
            'additional_sum_percent',
            'total_sum',

            [
                'attribute' => 'paid',
                'value' => function($model) {
                    /** @var $model \app\models\ars\ClientCertificate */
                    return $model->paid ? 'Да' : 'Нет';
                }
            ],
            [
                'attribute' => 'printed',
                'value' => function($model) {
                    /** @var $model \app\models\ars\ClientCertificate */
                    return $model->printed ? 'Да' : 'Нет';
                }
            ],
            [
                'attribute' => 'issued',
                'value' => function($model) {
                    /** @var $model \app\models\ars\ClientCertificate */
                    return $model->issued ? 'Да' : 'Нет';
                }
            ],
        ],
    ]); ?>


</div>
