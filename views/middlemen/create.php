<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ars\Middlemen */

$this->title = 'Добавление посредника';
$this->params['breadcrumbs'][] = ['label' => 'Посредники', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="middlemen-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
