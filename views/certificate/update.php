<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ars\ClientCertificate */

$this->title = 'Редактирование сертификата: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Сертификаты', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="client-certificate-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'middlemens' => $middlemens,
    ]) ?>

</div>
