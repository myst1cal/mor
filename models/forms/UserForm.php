<?php

namespace app\models\forms;

use yii\base\Model;

class UserForm extends Model
{
    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';

    public $id;
    public $login;
    public $email;
    public $password;
    public $role;

    public function attributeLabels()
    {
        return [
            'login'    => 'Логин',
            'email'    => 'Почта',
            'password' => 'Пароль',
            'role'     => 'Роль',
        ];
    }

    public function rules()
    {
        return [
            ['id', 'integer'],

            ['login', 'trim'],
            ['login', 'required'],
            ['login', 'unique', 'targetClass' => '\app\models\ars\User', 'message' => 'This username has already been taken.', 'except' => self::SCENARIO_UPDATE],
            ['login', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\app\models\ars\User', 'message' => 'This email address has already been taken.', 'except' => self::SCENARIO_UPDATE],

            ['password', 'required', 'except' => self::SCENARIO_UPDATE],
            ['password', 'string', 'min' => 6],

            ['role', 'required'],
            ['role', 'string'],
        ];
    }
}
