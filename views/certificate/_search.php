<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\searches\CertificateSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="client-certificate-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'group_id') ?>

    <?= $form->field($model, 'client_id') ?>

    <?= $form->field($model, 'number') ?>

    <?= $form->field($model, 'cost_price') ?>

    <?php // echo $form->field($model, 'additional_sum') ?>

    <?php // echo $form->field($model, 'additional_sum_percent') ?>

    <?php // echo $form->field($model, 'total_sum') ?>

    <?php // echo $form->field($model, 'paid') ?>

    <?php // echo $form->field($model, 'printed') ?>

    <?php // echo $form->field($model, 'issued') ?>

    <?php // echo $form->field($model, 'middlemen_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
