<?php

use app\models\ars\ClientCertificate;
use yii\db\Migration;

/**
 * Class m190818_104459_addOverpaymentColumnToClientCertificateTable
 */
class m190818_104459_addOverpaymentColumnToClientCertificateTable extends Migration
{
    public function safeUp()
    {
        $this->addColumn(ClientCertificate::tableName(), 'overpayment', $this->decimal(12, 2)->null()->after('additional_sum')->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn(ClientCertificate::tableName(), 'overpayment');
    }
}
