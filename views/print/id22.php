<?php
/** @var $certificate \app\models\ars\ClientCertificate */
?>
<html xmlns:v="urn:schemas-microsoft-com:vml"
      xmlns:o="urn:schemas-microsoft-com:office:office"
      xmlns="http://www.w3.org/TR/REC-html40">

<head>
    <meta http-equiv=Content-Type content="text/html; charset=windows-1251">
    <meta name=ProgId content=Word.Document>
    <meta name=Generator content="Microsoft Word 14">
    <meta name=Originator content="Microsoft Word 14">
    <style>
        <!--
        /* Font Definitions */
        @font-face {
            font-family: Calibri;
            panose-1: 2 15 5 2 2 2 4 3 2 4;
            mso-font-charset: 204;
            mso-generic-font-family: swiss;
            mso-font-pitch: variable;
            mso-font-signature: -536870145 1073786111 1 0 415 0;
        }

        @font-face {
            font-family: Tahoma;
            panose-1: 2 11 6 4 3 5 4 4 2 4;
            mso-font-charset: 204;
            mso-generic-font-family: swiss;
            mso-font-pitch: variable;
            mso-font-signature: -520081665 -1073717157 41 0 66047 0;
        }

        /* Style Definitions */
        p.MsoNormal, li.MsoNormal, div.MsoNormal {
            mso-style-unhide: no;
            mso-style-qformat: yes;
            mso-style-parent: "";
            margin-top: 0cm;
            margin-right: 0cm;
            margin-bottom: 10.0pt;
            margin-left: 0cm;
            line-height: 115%;
            mso-pagination: widow-orphan;
            font-size: 11.0pt;
            font-family: "Calibri", "sans-serif";
            mso-fareast-font-family: "Times New Roman";
            mso-fareast-theme-font: minor-fareast;
            mso-bidi-font-family: "Times New Roman";
            text-decoration: none !important;
        }

        a:-webkit-any-link {
            color: #000;
            cursor: pointer;
            text-decoration: none !important;
        }

        p.MsoAcetate, li.MsoAcetate, div.MsoAcetate {
            mso-style-noshow: yes;
            mso-style-priority: 99;
            mso-style-link: "Текст выноски Знак";
            margin: 0cm;
            margin-bottom: .0001pt;
            mso-pagination: widow-orphan;
            font-size: 8.0pt;
            font-family: "Tahoma", "sans-serif";
            mso-fareast-font-family: "Times New Roman";
            mso-fareast-theme-font: minor-fareast;
        }

        span.a {
            mso-style-name: "Текст выноски Знак";
            text-decoration: none !important;
            mso-style-noshow: yes;
            mso-style-priority: 99;
            mso-style-unhide: no;
            mso-style-locked: yes;
            mso-style-link: "Текст выноски";
            font-family: "Tahoma", "sans-serif";
            mso-ascii-font-family: Tahoma;
            mso-hansi-font-family: Tahoma;
            mso-bidi-font-family: Tahoma;
        }

        p.msopapdefault, li.msopapdefault, div.msopapdefault {
            mso-style-name: msopapdefault;
            mso-style-unhide: no;
            mso-margin-top-alt: auto;
            margin-right: 0cm;
            margin-bottom: 10.0pt;
            margin-left: 0cm;
            line-height: 115%;
            mso-pagination: widow-orphan;
            font-size: 12.0pt;
            font-family: "Times New Roman", "serif";
            mso-fareast-font-family: "Times New Roman";
            mso-fareast-theme-font: minor-fareast;
        }

        span.SpellE {
            mso-style-name: "";
            mso-spl-e: yes;
        }

        span.GramE {
            mso-style-name: "";
            mso-gram-e: yes;
        }

        .MsoChpDefault {
            mso-style-type: export-only;
            mso-default-props: yes;
            font-size: 10.0pt;
            mso-ansi-font-size: 10.0pt;
            mso-bidi-font-size: 10.0pt;
        }

        @page WordSection1 {
            size: 595.3pt 841.9pt;
            margin: 0cm 0cm 0cm 0cm;
            mso-header-margin: 35.4pt;
            mso-footer-margin: 35.4pt;
            mso-paper-source: 0;
        }

        div.WordSection1 {
            page: WordSection1;
        }

        -->
    </style>
</head>

<body lang=RU style='tab-interval:35.4pt'>
<div class=WordSection1>
    <div align=center>
        <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=794 style='width:595.5pt; border-collapse: collapse; mso-yfti-tbllook:1184;mso-padding-alt:0cm 0cm 0cm 0cm'>
            <tr style 'mso-yfti-irow: 0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes; page-break-inside:avoid;height:419.6pt; mso-height-rule:exactly'>
                <td width="50%" valign=top style='width:50.0%;padding:2.85pt 5.4pt 2.85pt 5.4pt; height:419.6pt; mso-height-rule: exactly'>
                    <div align=center>
                        <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=381 style='width:285.95pt; border-collapse:collapse;mso-yfti-tbllook:1184; mso-padding-alt:0cm 0cm 0cm 0cm'>
                            <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;height:20.0pt;mso-height-rule: exactly'>
                                <td width=114 colspan=3 rowspan=2 style='width:85.8pt;padding:0cm 5.4pt 0cm 5.4pt; height:20.0pt;mso-height-rule:exactly'></td>
                                <td width=267 colspan=5 style='width:200.15pt; padding:0cm 5.4pt 0cm 5.4pt;height:20.0pt;mso-height-rule:exactly'></td>
                            </tr>
                            <tr style='mso-yfti-irow:1;height:20.85pt;mso-height-rule:exactly'>
                                <td width=133 colspan=3 style='width:100.0pt;padding:0cm 5.4pt 0cm 5.4pt;height:20.85pt;mso-height-rule:exactly'></td>
                                <td width=134 colspan=2 style='width:100.15pt;padding:0cm 5.4pt 0cm 5.4pt;height:20.85pt;mso-height-rule:exactly'></td>
                            </tr>
                            <tr style='mso-yfti-irow:2;height:19.85pt'>
                                <td width=381 colspan=8 style='width:285.95pt;padding:0cm 5.4pt 0cm 5.4pt;height:19.85pt'>
                                    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom: .0001pt;text-align:center;line-height:normal'>
                                        <b>
                                            <span style='font-size:12.0pt;font-family:"Times New Roman","serif"'>СВІДОЦТВО ФАХІВЦЯ</span>&nbsp;
                                            <span style='font-size:12.0pt;font-family:"Times New Roman","serif"'>№ <?= $certificate->number ?></span>
                                        </b>
                                    </p>
                                </td>
                            </tr>
                            <tr style='mso-yfti-irow:3;height:20.7pt'>
                                <td width=378 colspan=8 valign=top style='width:10.0cm;padding:0cm 5.4pt 0cm 5.4pt; height:20.7pt'>
									<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt; line-height:normal'><span style='font-size:7.0pt;font-family:"Times New Roman","serif"'>Видане на підставі положень Міжнародної конвенції про підготовку і дипломування моряків та несення вахти 1978 року, з поправками (урахування Манільських), і національних вимог</span></p>
                                </td>
                            </tr>
                            <tr style='mso-yfti-irow:4;height:9.35pt'>
                                <td width=381 colspan=8 style='width:285.95pt;padding:0cm 5.4pt 0cm 5.4pt; height:9.35pt'>
                                    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom: .0001pt; line-height:normal'><span style='font-size:8.0pt; font-family:"Times New Roman","serif"'>Цим засвідчується, що:</span></p>
                                </td>
                            </tr>
                            <tr style='mso-yfti-irow:5;height:14.8pt'>
                                <td width=381 colspan=8 style='width:285.95pt;padding:0cm 5.4pt 0cm 5.4pt; height:14.8pt'>
                                    <p class=MsoNormal align=center valign=top style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:center;line-height:normal'><b><span style='font-size:14.0pt;font-family:"Times New Roman","serif";'><?= $certificate->client->surname_ukr ?></span></b>
                                    </p>
                                </td>
                            </tr>
                            <tr style='mso-yfti-irow:6;height:14.8pt'>
                                <td width=381 colspan=8 style='width:285.95pt;padding:0cm 5.4pt 0cm 5.4pt;height:14.8pt'>
                                    <p class=MsoNormal align=center valign=top style='margin-bottom:0cm;margin-bottom: .0001pt;text-align:center;line-height:normal'><b><span style='font-size:14.0pt;font-family:"Times New Roman","serif";'> <?= $certificate->client->name_ukr . ' ' . $certificate->client->patronymic_ukr ?></span></b>
                                    </p>
                                </td>
                            </tr>
                            <tr style='mso-yfti-irow:7;height:16.0pt;mso-height-rule:exactly'>
                                <td width=114 colspan=3 style='width:85.8pt;padding:0cm 5.4pt 0cm 5.4pt;height:16.0pt;mso-height-rule:exactly'>
                                    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt; line-height:normal'>
                                        <b><span style='font-size:9.0pt;font-family:"Times New Roman","serif"'>Дата&nbsp;народження:</span></b>
                                    </p>
                                </td>
                                <td width=152 colspan=4 style='width:113.9pt;padding:0cm 5.4pt 0cm 5.4pt;height:16.0pt;mso-height-rule:exactly'>
                                    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:center;line-height:normal'><b><span style='font-size:12.0pt;font-family:"Times New Roman","serif";'><?= Yii::$app->formatter->asDate($certificate->client->birthday) ?></span></b></p>
                                </td>
                                <td width=115 valign=top style='width:86.25pt;padding:0cm 5.4pt 0cm 5.4pt;height:16.0pt;mso-height-rule:exactly'>&nbsp;</td>
                            </tr>
                            <tr style='mso-yfti-irow:8;height:19.85pt'>
                                <td width=381 colspan=8 style='width:285.95pt;padding:0cm 5.4pt 0cm 5.4pt; height:19.85pt'>
                               <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal'><span style='font-size:8.0pt; font-family:"Times New Roman","serif"'>успішно пройшов підготовку за схваленим Морською Адміністрацією України курсом</span></p>
                                </td>
                            </tr>
                            <tr style='mso-yfti-irow:9;page-break-inside:avoid;height:51.05pt'>
                                <td width=381 colspan=8 style='width:285.95pt;padding:1.4pt 5.4pt; height:51.05pt'>
                                    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom: .0001pt;text-align:center;line-height:normal'>
                                        <b><span style='font-size: 11.0pt;font-family:"Times New Roman","serif";color:black'>ПІДГОТОВКА ОСІБ КОМАНДНОГО ТА РЯДОВОГО СКЛАДУ, ЯКІ ВІДПОВІДАЮТЬ ЗА ВАНТАЖНІ ОПЕРАЦІЇ НА СУДНАХ, ЩО ПЕРЕВОЗЯТЬ НЕБЕЗПЕЧНІ ВАНТАЖІ НАВАЛОМ ТА В УПАКОВЦІ</span></b></p>
                                </td>
                            </tr>
                            <tr style='mso-yfti-irow:10;height:20.0pt'>
                                <td width=381 colspan=8 valign=top style='width:285.95pt;padding:0cm 5.4pt 0cm 5.4pt; height:20.0pt'>
                                    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal'><span style='font-size:8.0pt;font-family:"Times New Roman","serif"'>який базується на типовому курсі IMO 1.10 "Небезпечні, шкідливі та потенційно небезпечні вантажі" з рекомендаціями та у відповідності з вимогами розділів В-V/b, В-V/c Кодексу ПДНВ з урахуванням вимог 49 Кодексу федеральних правил берегової охорони США та підтвердив рівень компетентності відповідно до стандартів,визначених у вищезгаданих розділах Кодексу</span></p>
                                </td>
                            </tr>
                            <tr style='mso-yfti-irow:11;height:20.7pt'>
                                <td width=92 colspan=2 rowspan=4 style='width:69.35pt;padding:0cm 5.4pt 0cm 5.4pt; height:20.7pt'>&nbsp;</td>
                                <td width=174 colspan=5 valign=bottom style='width:130.35pt;padding:0cm 5.4pt 0cm 5.4pt; height:20.7pt'>
                                    <p class=MsoNormal align=right style='margin-bottom:0cm;margin-bottom:.0001pt; text-align:right;line-height:normal'><span style='font-size:8.0pt;font-family:"Times New Roman","serif"'>Керівник&nbsp;&nbsp;&nbsp;&nbsp;..……………......…...</span></p>
                                </td>
                               <td width=115 rowspan=2 valign=center style='width:86.25pt;padding:0cm 5.4pt 0cm 5.4pt; height:20.7pt'></td>
                            </tr>
                            <tr style='mso-yfti-irow:12;height:20.7pt'>
                                <td width=174 colspan=5 valign=bottom style='width:130.35pt;padding:0cm 5.4pt 0cm 5.4pt;height:20.7pt'>
                                    <p class=MsoNormal align=right style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:right;line-height:normal'><span style='font-size:8.0pt;font-family:"Times New Roman","serif"'>Реєстратор&nbsp;&nbsp;..……………............</span></p>
                                </td>
                            </tr>
                            <tr style='mso-yfti-irow:13;height:20.7pt'>
                                <td width=77 colspan=3 valign=bottom style='width:57.9pt;padding:0cm 5.4pt 0cm 5.4pt; height:20.7pt'>
                                   <p class=MsoNormal align=right style='margin-bottom:0cm;margin-bottom:1pt; text-align:right;line-height:normal'><span style='font-size:8.0pt; font-family:"Times New Roman","serif"'>Дата видачі:</span></p>
                                </td>
                                <td width=97 nowrap colspan=2 valign=bottom style='width:72.45pt; padding:0cm 2.85pt 0cm 2.85pt;height:20.7pt'>
                                    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal'><b><span style='font-size:12.0pt;font-family: "Times New Roman","serif";mso-ansi-language:EN-US'><?= $certificate->getDateStart() ?></span></b>
                                    </p>
                                </td>
                                <td width=115 rowspan=2 valign=center align=center style='width:86.25pt;padding:0cm 0cm 0cm 0cm; height:20.7pt'><img width=53 height=53 src="/assets/img/qr-code.gif" style="padding-top: 10px;"></td>
                            </tr>
                            <tr style='mso-yfti-irow:14;height:20.7pt;mso-height-rule:exactly'>
                                <td width=77 colspan=3 valign=bottom style='width:57.9pt;padding:0cm 5.4pt 0cm 5.4pt; height:20.7pt;mso-height-rule:exactly'>
                                    <p class=MsoNormal align=right style='margin-bottom:0cm;margin-bottom:1pt;text-align:right;line-height:normal'><span style='font-size:8.0pt;font-family:"Times New Roman","serif"'>Дійсно до</span></p>
                                </td>
                                <td width=97 colspan=2 valign=bottom style='width:72.45pt;padding:0cm 2.85pt 0cm 2.85pt; height:20.7pt; mso-height-rule:exactly'>
                                    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal'><b><span style='font-size:12.0pt; font-family: "Times New Roman","serif";'><?= $certificate->getDateEnd() ?></span></b></p>
                                </td>
                            </tr>
                            <tr style='mso-yfti-irow:15;height:36.85pt'>
                                <td width=381 colspan=8 style='width:285.95pt;padding:0cm 0cm 0cm 0cm; height:36.85pt'> </td>
                            </tr>
                            <tr style='mso-yfti-irow:16;mso-yfti-lastrow:yes'>
                                <td width=76 style='width:57.2pt;padding:0cm 0cm 0cm 0cm'></td>
                                <td width=38 colspan=2 style='width:28.6pt;padding:0cm 0cm 0cm 0cm'></td>
                                <td width=38 style='width:28.6pt;padding:0cm 0cm 0cm 0cm'></td>
                                <td width=95 colspan=2 style='width:71.4pt;padding:0cm 0cm 0cm 0cm'></td>
                                <td width=19 style='width:13.9pt;padding:0cm 0cm 0cm 0cm'></td>
                                <td width=115 style='width:86.25pt;padding:0cm 0cm 0cm 0cm'></td>
                            </tr>
                            <![if !supportMisalignedColumns]>
                            <tr height=0>
                                <td width=76 style='border:none'></td>
                                <td width=16 style='border:none'></td>
                                <td width=22 style='border:none'></td>
                                <td width=39 style='border:none'></td>
                                <td width=17 style='border:none'></td>
                                <td width=79 style='border:none'></td>
                                <td width=19 style='border:none'></td>
                                <td width=113 style='border:none'></td>
                            </tr>
                            <![endif]>
                        </table>
                    </div>
                </td>
                <td width="50%" valign=top style='width:50.0%;padding:2.85pt 5.4pt 0cm 5.4pt;height:419.6pt; mso-height-rule: exactly'>
                    <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=381 style='width:285.95pt; border-collapse:collapse;mso-yfti-tbllook:1184; mso-padding-alt:0cm 0cm 0cm 0cm'>
                        <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;height:20.0pt;mso-height-rule: exactly'>
                                <td width=114 colspan=3 rowspan=2 style='width:85.8pt;padding:0cm 5.4pt 0cm 5.4pt; height:20.0pt;mso-height-rule:exactly'></td>
                                <td width=267 colspan=5 valign=top style='width:200.15pt; padding:0cm 5.4pt 0cm 5.4pt; height:20.0pt;mso-height-rule:exactly'></td>
                            </tr>
                            <tr style='mso-yfti-irow:1;height:20.85pt;mso-height-rule:exactly'>
                                <td width=133 colspan=3 style='width:100.0pt;padding:0cm 5.4pt 0cm 5.4pt; height:20.85pt;mso-height-rule:exactly'></td>
                                <td width=134 colspan=2 style='width:100.15pt;padding:0cm 5.4pt 0cm 5.4pt; height:20.85pt;mso-height-rule:exactly'></td>
                            </tr>
                        <tr style='mso-yfti-irow:2;height:19.85pt'>
                            <td width=396 colspan=8 style='width:296.85pt;padding:0cm 5.4pt 0cm 5.4pt; height:19.85pt'>
                                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt; text-align:center;line-height:normal'><b><span
                                                style='font-size:12.0pt;font-family:"Times New Roman","serif";'>CERTIFICATE OF PROFICIENCY</span>&nbsp;<span
                                                style='font-size:12.0pt;font-family:"Times New Roman","serif";'>№ <?= $certificate->number ?></span></b>
                                </p>
                            </td>
                        </tr>
                        <tr style='mso-yfti-irow:3;height:20.7pt'>
                            <td width=396 colspan=8 valign=top style='width:296.85pt;padding:0cm 5.4pt 0cm 5.4pt; height:20.7pt'>
                                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt; line-height:normal'><span lang=EN-US style='font-size:7.0pt;font-family: "Times New Roman","serif"; mso-ansi-language:EN-US'>Issued under the provisions of the International Convention on Standards of Training, Certification and Watchkeeping for Seafarers, 1978, as amended (with the Manila Amendments), and national requirements</span></p>
                            </td>
                        </tr>
                        <tr style='mso-yfti-irow:4;height:9.05pt'>
                            <td width=396 colspan=8 style='width:296.85pt;padding:0cm 5.4pt 0cm 5.4pt; height:9.05pt'>
                                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt; line-height:normal'><span lang=EN-US style='font-size:8.0pt;font-family: "Times New Roman","serif";mso-ansi-language:EN-US'>This is to certify&nbsp;that:</span></p>
                            </td>
                        </tr>
                        <tr style='mso-yfti-irow:5;height:14.8pt'>
                            <td width=396 colspan=8 style='width:296.85pt;padding:0cm 5.4pt 0cm 5.4pt; height:14.8pt'>
                                <p class=MsoNormal align=center valign=top style='margin-bottom:0cm;margin-bottom: .0001pt; text-align:center;line-height:normal'><b><span lang=EN-US style='font-size:14.0pt;font-family:"Times New Roman","serif";mso-ansi-language: EN-US'><?= $certificate->client->name_transliteration ?></span></b></p>
                            </td>
                        </tr>
                        <tr style='mso-yfti-irow:6;height:14.8pt'>
                            <td width=396 colspan=8 style='width:296.85pt;padding:0cm 5.4pt 0cm 5.4pt; height:14.8pt'>
                                <p class=MsoNormal align=center valign=top style='margin-bottom:0cm;margin-bottom: .0001pt;text-align:center;line-height:normal'><b><span lang=EN-US style='font-size:14.0pt;font-family:"Times New Roman","serif";mso-ansi-language: EN-US'><?= $certificate->client->surname_transliteration ?></b></p>
                            </td>
                        </tr>
                        <tr style='mso-yfti-irow:7;height:16.0pt;mso-height-rule:exactly'>
                            <td width=114 style='width:85.8pt;padding:0cm 5.4pt 0cm 5.4pt;height:16.0pt; mso-height-rule:exactly'>
                                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt; line-height:normal'><b><span lang=EN-US style='font-size:9.0pt;font-family: "Times New Roman","serif";mso-ansi-language:EN-US'>Date of&nbsp;birth:</span></b></p>
                            </td>
                            <td width=175 colspan=5 style='width:131.05pt;padding:0cm 5.4pt 0cm 5.4pt; height:16.0pt;mso-height-rule:exactly'>
                                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom: .0001pt;text-align:center;line-height:normal'><b><span lang=EN-US style='font-size:12.0pt;font-family:"Times New Roman","serif";mso-ansi-language: EN-US'><?= Yii::$app->formatter->asDate($certificate->client->birthday) ?></span></b></p>
                            </td>
                            <td width=107 colspan=2 valign=top style='width:80.0pt;padding:0cm 5.4pt 0cm 5.4pt; height:16.0pt;mso-height-rule:exactly'>&nbsp;</td>
                        </tr>
                        <tr style='mso-yfti-irow:8;height:19.85pt'>
                            <td width=396 colspan=8 style='width:296.85pt;padding:0cm 5.4pt 0cm 5.4pt; height:19.85pt'>
                                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt; line-height:normal'><span lang=EN-US style='font-size:7.0pt;font-family: "Times New Roman","serif";mso-ansi-language:EN-US'>has successfully completed the course of training, approved by the Maritime Administration of Ukraine on the syllabus</span></p>
                            </td>
                        </tr>
                        <tr style='mso-yfti-irow:9;height:51.05pt'>
                            <td width=396 colspan=8 style='width:296.85pt;padding:1.4pt 5.4pt; height:51.05pt'>
                                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:center;line-height:normal'><b><span lang=EN-US style='font-size:11.0pt;font-family:"Times New Roman","serif";mso-ansi-language:EN-US'>TRAINING OF OFFICERS AND RATINGS RESPONSIBLE FOR CARGO HANDLING ON SHIPS CARRYING DANGEROUS AND HAZARDOUS SUBSTANCES IN SOLID FORM IN BULK AND IN PACKAGED FORM</span></b>
                                </p>
                            </td>
                        </tr>
                        <tr style='mso-yfti-irow:10;height:20.0pt'>
                            <td width=396 colspan=8 valign=top style='width:296.85pt;padding:0cm 5.4pt 0cm 5.4pt; height:20.0pt'>
                                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal'><span lang=EN-US style='font-size:8.0pt;font-family:"Times New Roman","serif";mso-ansi-language:EN-US'>based on IMO course 1.10 "Dangerous, hazardous and harmful cargoes plus compendium" with recommendations and in accordance with the provisions of Regulation В-V/b, В-V/c of the  STCW Code including the US Coastguard reguirements of the 49 Code of Federal Regulations and meets the standards of competense specified in the above mentioned Sections of the Code<br />&nbsp;</span></p>
                            </td>
                        </tr>
                        <tr style='mso-yfti-irow:11;height:20.7pt'>
                            <td width=114 rowspan=5 style='width:85.8pt;padding:0cm 5.4pt 0cm 5.4pt; height:20.7pt'>&nbsp;</td>
                            <td width=164 colspan=4 valign=bottom style='width:123.25pt;padding:0cm 5.4pt 0cm 5.4pt; height:20.7pt'>
                                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt; line-height:normal'><span lang=EN-US style='font-size:8.0pt;font-family: "Times New Roman","serif";mso-ansi-language:EN-US'>Manager&nbsp;&nbsp;&nbsp;&nbsp;..……………......…........</span></p>
                            </td>
                            <td width=117 colspan=3 rowspan=2 style='width:87.8pt;padding:0cm 0cm 0cm 0cm; height:20.7pt'> </td>
                        </tr>
                        <tr style='mso-yfti-irow:12;height:20.7pt'>
                            <td width=164 colspan=4 valign=bottom style='width:123.25pt;padding:0cm 5.4pt 0cm 5.4pt; height:20.7pt'>
                                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt; line-height:normal'><span lang=EN-US style='font-size:8.0pt;font-family: "Times New Roman", "serif"; mso-ansi-language:EN-US'>Registrar&nbsp;&nbsp;&nbsp;&nbsp; ..……………......…........</span></p>
                            </td>
                        </tr>
                        <tr style='mso-yfti-irow:13;height:20.7pt'>
                            <td width=78 valign=bottom style='width:58.45pt;padding:0cm 5.4pt 0cm 5.4pt; height:20.7pt'>
                                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:1pt; line-height:normal'><span lang=EN-US style='font-size:8.0pt;font-family: "Times New Roman","serif";mso-ansi-language:EN-US'>Date of issue</span></p>
                            </td>
                            <td width=86 valign=bottom colspan=3 style='width:64.8pt;padding:7.4pt 2.4pt 0cm 2.4pt; height:20.7pt'>
                                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom: .0001pt; text-align: center;line-height:normal'><b><span lang=EN-US style='font-size:12.0pt; font-family:"Times New Roman","serif";mso-ansi-language: EN-US'><?= $certificate->getDateStart() ?></span></b></p>
                            </td>
                            <td width=117 colspan=3 rowspan=2 valign=center align=center style='width:87.8pt; padding:0cm 5.4pt 0cm 5.4pt;height:20.7pt'><img width=53 height=53 src="/assets/img/qr-code.gif" style="padding-top: 10px;"></td>
                        </tr>
                        <tr style='mso-yfti-irow:14;height:20.7pt;mso-height-rule:exactly'>
                            <td width=78 valign=bottom style='width:60.45pt;padding:0cm 5.4pt 0cm 5.4pt; height:20.7pt;mso-height-rule:exactly'>
                                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:1pt; line-height:normal'><span lang=EN-US style='font-size:8.0pt;font-family: "Times New Roman","serif";mso-ansi-language:EN-US'>Date of expire</span></p>
                            </td>
                            <td width=86 colspan=3 valign=bottom style='width:64.8pt;padding:7.4pt 5.4pt 0cm 5.4pt; height:20.7pt;mso-height-rule:exactly'>
                                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt; text-align:center;line-height:normal'><b><span lang=EN-US style='font-size:12.0pt; font-family:"Times New Roman","serif";mso-ansi-language: EN-US'><?= $certificate->getDateEnd() ?></span></b></p>
                            </td>
                        </tr>
                        <tr style='mso-yfti-irow:15;height:36.85pt'>
                            <td width=281 colspan=7 style='width:211.05pt;padding:0cm 0cm 0cm 0cm; height:36.85pt'> </td>
                        </tr>
                        <tr style='mso-yfti-irow:16;mso-yfti-lastrow:yes'>
                            <td width=114 style='width:85.8pt;padding:0cm 0cm 0cm 0cm'></td>
                            <td width=134 colspan=2 style='width:100.2pt;padding:0cm 0cm 0cm 0cm'></td>
                            <td width=26 style='width:19.4pt;padding:0cm 0cm 0cm 0cm'></td>
                            <td width=5 style='width:3.65pt;padding:0cm 0cm 0cm 0cm'></td>
                            <td width=54 colspan=2 style='width:40.4pt;padding:0cm 0cm 0cm 0cm'></td>
                            <td width=63 style='width:47.4pt;padding:0cm 0cm 0cm 0cm'></td>
                        </tr>
                        <![if !supportMisalignedColumns]>
                        <tr height=0>
                            <td width=114 style='border:none'></td>
                            <td width=78 style='border:none'></td>
                            <td width=67 style='border:none'></td>
                            <td width=31 style='border:none'></td>
                            <td width=6 style='border:none'></td>
                            <td width=14 style='border:none'></td>
                            <td width=44 style='border:none'></td>
                            <td width=59 style='border:none'></td>
                        </tr>
                        <![endif]>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</div>
</body>
</html>