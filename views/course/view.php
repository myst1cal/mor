<?php

use app\helpers\AuthHelper;
use yii\helpers\Html;
use yii\web\YiiAsset;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ars\Course */

$this->title = $model->title_ukr;
$this->params['breadcrumbs'][] = ['label' => 'Курсы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
YiiAsset::register($this);
?>
<div class="course-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Список', ['index'], ['class' => 'btn btn-success']) ?>
        <?= AuthHelper::canEditCourse() ? Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) : '' ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title_ukr:ntext',
            'title_eng:ntext',
            'price',
            'template',
            [
                'attribute' => 'has_ukr_only',
                'value' => $model->has_ukr_only ? 'Да' : 'Нет',
            ],
            [
                'attribute' => 'conventional',
                'value' => $model->conventional ? 'Да' : 'Нет',
            ],
        ],
    ]) ?>


    <div class="row">
        <div class="col-lg-6">

            <h3>Преподаватели</h3>
            <?php foreach ($model->teachers as $teacher): ?>
                <h4>
                    <div class="label label-info">
                        <?= $teacher->full_name . ' ' ?>
                    </div>
                </h4>
            <?php endforeach; ?>
        </div>
        <div class="col-lg-6">

            <h3>Доп поля</h3>
            <?php foreach ($model->courseAdditionalFields as $additionalField): ?>
                <h4>
                    <div class="label label-info">
                        <?= $additionalField->name . ' ' ?>
                    </div>
                </h4>
            <?php endforeach; ?>
        </div>
    </div>

</div>
