$(function () {
    $('.act-of-work').on('click', function () {
        let clientId = $(this).attr('data-client_id');
        let certificateIds = [];

        $('.certificate-check').each(function () {
            if ($(this).prop('checked')) {
                certificateIds.push($(this).attr('data-id'));
            }
        });

        let win = window.open('/print/act-of-work?client_id=' + clientId + '&certificate_ids=' + certificateIds, '_blank');
        win.focus();
    });

    $('.payment-invoice').on('click', function () {
        let clientId = $(this).attr('data-client_id');
        let certificateIds = [];

        $('.certificate-check').each(function () {
            if ($(this).prop('checked')) {
                certificateIds.push($(this).attr('data-id'));
            }
        });

        let win = window.open('/print/payment-invoice?client_id=' + clientId + '&certificate_ids=' + certificateIds, '_blank');
        win.focus();
    });

    $('.personal-card').on('click', function () {
        let clientId = $(this).attr('data-client_id');

        let certificateId = $('.certificate-check:checked').attr('data-id');

        let win = window.open('/print/personal-card?client_id=' + clientId + '&certificate_id=' + certificateId, '_blank');
        win.focus();
    });
});