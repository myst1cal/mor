<?php

namespace app\controllers;

use app\helpers\AuthHelper;
use app\models\ars\Client;
use app\models\ars\ClientCertificate;
use app\models\ars\Counter;
use app\models\ars\Course;
use app\models\ars\Group;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;

class PrintController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['certificate'],
                        'allow' => true,
                        'roles' => AuthHelper::CAN_PRINT_CERTIFICATE,
                    ],
                    [
                        'actions' => ['agreement'],
                        'allow' => true,
                        'roles' => AuthHelper::CAN_PRINT_AGREEMENT,
                    ],
                    [
                        'actions' => ['act-of-work'],
                        'allow' => true,
                        'roles' => AuthHelper::CAN_PRINT_STATEMENT_AGREEMENT_OFFER,
                    ],
                    [
                        'actions' => ['payment-invoice'],
                        'allow' => true,
                        'roles' => AuthHelper::CAN_PRINT_PAYMENT_INVOICE_ACT_OF_WORK,
                    ],
                    [
                        'actions' => ['payment-invoice'],
                        'allow' => true,
                        'roles' => AuthHelper::CAN_PRINT_PAYMENT_INVOICE_ACT_OF_WORK,
                    ],
                    [
                        'actions' => ['personal-card'],
                        'allow' => true,
                        'roles' => AuthHelper::CAN_PRINT_PERSONAL_CARD,
                    ],
                    [
                        'actions' => ['group'],
                        'allow' => true,
                        'roles' => AuthHelper::CAN_PRINT_GROUP,
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionCertificate()
    {
        $certificateId = Yii::$app->request->post('certificate_id');
        $ukrOnly = Yii::$app->request->post('ukr_only');

        $certificate = ClientCertificate::findOne($certificateId);
        $course = $certificate->course;

        return $this->renderPartial($course->template, ['certificate' => $certificate, 'ukrOnly' => $ukrOnly]);
    }

    public function actionAgreement()
    {
        $clientId = Yii::$app->request->get('client_id');

        $client = Client::findOne($clientId);

        return $this->renderPartial('personal_data_processing_agreement', ['client' => $client]);
    }

    public function actionActOfWork()
    {
        $clientId = Yii::$app->request->get('client_id');
        $certificateIds = explode(',', Yii::$app->request->get('certificate_ids'));

        $client = Client::findOne($clientId);
        $certificates = ClientCertificate::find()->where(['id' => $certificateIds])->all();
        $sum = array_sum(ArrayHelper::getColumn($certificates, 'cost_price'));
        $counter = Counter::findOne(['name' => Counter::CERTIFICATE_NUMBER]);
        $counter->updateCounters(['value' => 1]);

        return $this->renderPartial('act_of_work', [
            'client' => $client,
            'sum' => $sum,
            'counter' => $counter->value,
            'certificates' => $certificates,
        ]);
    }

    public function actionPaymentInvoice()
    {
        $clientId = Yii::$app->request->get('client_id');
        $certificateIds = explode(',', Yii::$app->request->get('certificate_ids'));

        $client = Client::findOne($clientId);
        $certificates = ClientCertificate::find()->where(['id' => $certificateIds])->all();

        $organizationName = $_SERVER['SERVER_ADDR'] == '91.200.41.73' ? 'ЧОРНОМОРСЬКИЙ ТРЕНАЖЕРНИЙ ЦЕНТР' : 'ЄВРОПЕЙСЬКИЙ МОРСЬКИЙ ТРЕНАЖЕРНИЙ ЦЕНТР';
        $requisites = $_SERVER['SERVER_ADDR'] == '91.200.41.73' ? 'п/р UA143806340000026008141496001 у банку ПуАТ "КБ "АКОРДБАНК",<br>
                                        65014, Одеса, Базарна, будинок № 3 А, тел.: 0989759517,<br>
                                        код за ЄДРПОУ 42959524, ІПН 42959521553' : 'п/р UA363806340000026009141514001 у банку ПуАТ "КБ "АКОРДБАНК",<br>
                                        65003, Одеса, Атамана Головатого, будинок № 24, тел.: 0667882424,<br> 
                                        код за ЄДРПОУ 43116969, ІПН 431169615534';

        return $this->renderPartial('payment_invoice', ['client' => $client, 'certificates' => $certificates, 'organizationName' => $organizationName, 'requisites' => $requisites]);
    }

    public function actionPersonalCard()
    {
        $clientId = Yii::$app->request->get('client_id');
        $certificateId = Yii::$app->request->get('certificate_id');

        $client = Client::findOne($clientId);
        $certificate = ClientCertificate::findOne($certificateId);

        return $this->renderPartial('personal_card', ['client' => $client, 'certificate' => $certificate]);
    }

    public function actionGroup()
    {
        $groupId = Yii::$app->request->get('group_id');

        $group = Group::findOne($groupId);

        $period = $period = new \DatePeriod(
            new \DateTime($group->start_date),
            new \DateInterval('P1D'),
            (new \DateTime($group->end_date))->modify('+1 day')
        );

        $dates = [];
        foreach ($period as $key => $value) {
            if (!$this->isWeekend($value->format('U'))) {
                $dates[] = $value->format('d.m.Y');
            }
        }

        $clients = Client::find()
            ->leftJoin(ClientCertificate::tableName(), 'client_certificate.client_id=client.id')
            ->where(['client_certificate.group_id' => $group->id])
            ->all();

        return $this->renderPartial('group', ['dates' => $dates, 'clients' => $clients]);
    }

    private function isWeekend($date) {
        return (date('N', $date) >= 6);
    }
}
