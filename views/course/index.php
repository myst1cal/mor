<?php

use app\helpers\AuthHelper;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\searches\CourseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Курсы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="course-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= AuthHelper::canEditCourse() ? Html::a('Добавить курс', ['create'], ['class' => 'btn btn-success']) : '' ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],

            'title_ukr:ntext',
            'title_eng:ntext',
            'price',
            'template',
            [
                'attribute' => 'conventional',
                'format' => 'boolean'
            ],
            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => AuthHelper::canEditCourse() ? '{view} {update}' : '{view}',
            ],
        ],
    ]); ?>


</div>
