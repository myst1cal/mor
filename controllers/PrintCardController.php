<?php

namespace app\controllers;

use app\helpers\AuthHelper;
use app\models\ars\Client;
use app\models\ars\ClientCertificate;
use app\models\ars\Counter;
use app\models\ars\Course;
use app\models\ars\Group;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;

class PrintCardController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['certificate'],
                        'allow' => true,
                        'roles' => AuthHelper::CAN_PRINT_CERTIFICATE,
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionCertificate()
    {
        $certificateId = Yii::$app->request->post('certificate_id');
        $ukrOnly = Yii::$app->request->post('ukr_only');

        $certificate = ClientCertificate::findOne($certificateId);
        $course = $certificate->course;

        return $this->renderPartial($course->template, ['certificate' => $certificate, 'ukrOnly' => $ukrOnly]);
    }
}
