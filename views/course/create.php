<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ars\Course */

$this->title = 'Добавление курса';
$this->params['breadcrumbs'][] = ['label' => 'Курсы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="course-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'teachers' => $teachers,
        'additionalFields' => $additionalFields,
        'currentTeachers' => $currentTeachers,
        'currentAdditionalFields' => $currentAdditionalFields,
        'currentAdditionalFieldSort' => $currentAdditionalFieldSort,
    ]) ?>

</div>
