<?php

use app\helpers\AuthHelper;
use kartik\date\DatePicker;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $models app\models\ars\Course[] */

$this->title = 'Добавление пользователю курсов';
$this->params['breadcrumbs'][] = ['label' => 'Сертификаты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="client-certificate-create">

    <h1><?= Html::encode($this->title) ?></h1>


    <?= Html::beginForm() ?>
    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <th>&nbsp;&nbsp;&nbsp;&nbsp;</th>
            <th>Курс</th>
            <?php if (AuthHelper::canEditCertificateDateAdd()): ?>
                <th>Дата добавления</th>
            <?php endif; ?>
            <th>Цена, грн.</th>
        </tr>
        </thead>

        <tbody>
        <?php foreach ($models as $course): ?>
            <tr>
                <td><?= Html::checkbox('Certificate[' . $course->id . ']', false, ['class' => 'form-control']) ?></td>
                <td><?= $course->title_ukr ?></td>
                <?php if (AuthHelper::canEditCertificateDateAdd()): ?>
                    <th>
                        <?= DatePicker::widget(
                            [
                                'name'          => 'date_add[' . $course->id . ']',
                                'options'       => ['placeholder' => 'ГГГГ-ММ-ДД', 'autocomplete' => 'off'],
                                'type'          => DatePicker::TYPE_INPUT,
                                'pluginOptions' => [
                                    'autoclose' => true,
                                    'format'    => 'yyyy-mm-dd',
                                ],
                            ]) ?>
                    </th>
                <?php endif; ?>
                <td><?= $course->price ?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>
    <?= Html::endForm() ?>

</div>
