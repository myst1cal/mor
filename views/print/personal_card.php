<?php
/**
 * @var \app\models\ars\Client $client
 * @var \app\models\ars\ClientCertificate $certificate
 */
?>

<html>

<head>
    <meta http-equiv=Content-Type content="text/html; charset=windows-1251">
    <meta name=Generator content="Microsoft Word 14 (filtered)">
    <style>
        <!--
        /* Font Definitions */
        @font-face
        {font-family:Calibri;
            panose-1:2 15 5 2 2 2 4 3 2 4;}
        /* Style Definitions */
        p.MsoNormal, li.MsoNormal, div.MsoNormal
        {margin-top:0cm;
            margin-right:0cm;
            margin-bottom:10.0pt;
            margin-left:0cm;
            line-height:115%;
            font-size:11.0pt;
            font-family:"Calibri","sans-serif";}
        p.MsoNoSpacing, li.MsoNoSpacing, div.MsoNoSpacing
        {margin:0cm;
            margin-bottom:.0001pt;
            font-size:11.0pt;
            font-family:"Calibri","sans-serif";}
        p.MsoListParagraph, li.MsoListParagraph, div.MsoListParagraph
        {margin-top:0cm;
            margin-right:0cm;
            margin-bottom:10.0pt;
            margin-left:36.0pt;
            line-height:115%;
            font-size:11.0pt;
            font-family:"Calibri","sans-serif";}
        p.MsoListParagraphCxSpFirst, li.MsoListParagraphCxSpFirst, div.MsoListParagraphCxSpFirst
        {margin-top:0cm;
            margin-right:0cm;
            margin-bottom:0cm;
            margin-left:36.0pt;
            margin-bottom:.0001pt;
            line-height:115%;
            font-size:11.0pt;
            font-family:"Calibri","sans-serif";}
        p.MsoListParagraphCxSpMiddle, li.MsoListParagraphCxSpMiddle, div.MsoListParagraphCxSpMiddle
        {margin-top:0cm;
            margin-right:0cm;
            margin-bottom:0cm;
            margin-left:36.0pt;
            margin-bottom:.0001pt;
            line-height:115%;
            font-size:11.0pt;
            font-family:"Calibri","sans-serif";}
        p.MsoListParagraphCxSpLast, li.MsoListParagraphCxSpLast, div.MsoListParagraphCxSpLast
        {margin-top:0cm;
            margin-right:0cm;
            margin-bottom:10.0pt;
            margin-left:36.0pt;
            line-height:115%;
            font-size:11.0pt;
            font-family:"Calibri","sans-serif";}
        .MsoChpDefault
        {font-family:"Calibri","sans-serif";}
        .MsoPapDefault
        {margin-bottom:10.0pt;
            line-height:115%;}
        @page WordSection1
        {size:595.3pt 841.9pt;
            margin:42.55pt 42.55pt 2.0cm 2.0cm;}
        div.WordSection1
        {page:WordSection1;}
        /* List Definitions */
        ol
        {margin-bottom:0cm;}
        ul
        {margin-bottom:0cm;}
        -->
    </style>

</head>

<body lang=RU>

<div class=WordSection1>

    <table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0 width="100%"
           style='width:100.0%;border-collapse:collapse;border:none'>
        <tr>
            <td width=376 valign=top style='width:281.85pt;border:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='font-family:"Times New Roman","serif"'>&nbsp;</span></p>
            </td>
            <td width=262 style='width:196.7pt;border:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  150%'><span lang=UK style='font-size:12.0pt;line-height:150%;font-family:
  "Times New Roman","serif"'>Додаток 3</span></p>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  150%'><span lang=UK style='font-size:12.0pt;line-height:150%;font-family:
  "Times New Roman","serif"'>До Положення про порядок видачі посвідчення
  судноводія малого/маломірного судна <br>
  (пункт 2.2) </span></p>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span lang=UK style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
            </td>
        </tr>
    </table>

    <p class=MsoNormal align=center style='text-align:center'><b><span lang=UK
                                                                       style='font-size:16.0pt;line-height:115%;font-family:"Times New Roman","serif"'>ОСОБОВА
КАРТКА</span></b></p>

    <p class=MsoNormal align=center style='text-align:center'><b><span lang=UK
                                                                       style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif"'>судноводія
малого/маломірного судна</span></b></p>

    <table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0 width="100%"
           style='width:100.0%;border-collapse:collapse;border:none'>
        <tr>
            <td width=319 valign=top style='width:239.25pt;border:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoListParagraph style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-indent:-18.0pt;line-height:normal'><span lang=UK style='font-size:12.0pt;
  font-family:"Times New Roman","serif"'>1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span></span><span lang=UK style='font-size:12.0pt;font-family:"Times New Roman","serif"'>Прізвище</span></p>
            </td>
            <td width=319 valign=top style='width:239.3pt;border:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span lang=UK style='font-size:12.0pt;font-family:"Times New Roman","serif"'><?= $client->surname_ukr ?></span></p>
            </td>
        </tr>
        <tr>
            <td width=319 valign=top style='width:239.25pt;border:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoListParagraph style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-indent:-18.0pt;line-height:normal'><span lang=UK style='font-size:12.0pt;
  font-family:"Times New Roman","serif"'>2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span></span><span lang=UK style='font-size:12.0pt;font-family:"Times New Roman","serif"'>Ім’я</span></p>
            </td>
            <td width=319 valign=top style='width:239.3pt;border:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span lang=UK style='font-size:12.0pt;font-family:"Times New Roman","serif"'><?= $client->name_ukr ?></span></p>
            </td>
        </tr>
        <tr>
            <td width=319 valign=top style='width:239.25pt;border:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoListParagraph style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-indent:-18.0pt;line-height:normal'><span lang=UK style='font-size:12.0pt;
  font-family:"Times New Roman","serif"'>3.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span></span><span lang=UK style='font-size:12.0pt;font-family:"Times New Roman","serif"'>По
  батькові</span></p>
            </td>
            <td width=319 valign=top style='width:239.3pt;border:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span lang=UK style='font-size:12.0pt;font-family:"Times New Roman","serif"'><?= $client->patronymic_ukr ?></span></p>
            </td>
        </tr>
        <tr>
            <td width=319 valign=top style='width:239.25pt;border:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoListParagraph style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-indent:-18.0pt;line-height:normal'><span lang=UK style='font-size:12.0pt;
  font-family:"Times New Roman","serif"'>4.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span></span><span lang=UK style='font-size:12.0pt;font-family:"Times New Roman","serif"'>Дата
  народження</span></p>
            </td>
            <td width=319 valign=top style='width:239.3pt;border:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span lang=UK style='font-size:12.0pt;font-family:"Times New Roman","serif"'><?= $client->getBirthday() ?></span></p>
            </td>
        </tr>
        <tr>
            <td width=319 valign=top style='width:239.25pt;border:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoListParagraph style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-indent:-18.0pt;line-height:normal'><span lang=UK style='font-size:12.0pt;
  font-family:"Times New Roman","serif"'>5.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span></span><span lang=UK style='font-size:12.0pt;font-family:"Times New Roman","serif"'>Громадянство,
  серія, номер паспорта або іншого документа, що посвідчує особу</span></p>
            </td>
            <td width=319 valign=top style='width:239.3pt;border:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal'>
                    <u>
                        <span lang=UK style='font-size:12.0pt;font-family:"Times New Roman","serif"'> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;, паспорт &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></u></p>
            </td>
        </tr>
        <tr>
            <td width=319 valign=top style='width:239.25pt;border:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoListParagraph style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-indent:-18.0pt;line-height:normal'><span lang=UK style='font-size:12.0pt;
  font-family:"Times New Roman","serif"'>6.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span></span>
                    <span lang=UK style='font-size:12.0pt;font-family:"Times New Roman","serif"'>Місце проживання</span>
                </p>
            </td>
            <td width=319 valign=top style='width:239.3pt;border:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal'>
                    <u>
                        <span lang=UK style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                    </u>
                </p>
            </td>
        </tr>
        <tr>
            <td width=638 colspan=2 valign=top style='width:478.55pt;border:none;
  padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoListParagraph style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-indent:-18.0pt;line-height:normal'><span lang=UK style='font-size:12.0pt;
  font-family:"Times New Roman","serif"'>7.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span></span><span lang=UK style='font-size:12.0pt;font-family:"Times New Roman","serif"'>Пройшов(ла)
  навчання за програмою підготовки судноводія малого/маломірного судна</span></p>
            </td>
        </tr>
        <tr>
            <td width=319 valign=top style='width:239.25pt;border:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoListParagraph style='margin-bottom:0cm;margin-bottom:.0001pt;
  line-height:normal'><span lang=UK style='font-size:12.0pt;font-family:"Times New Roman","serif"'>Тип</span></p>
            </td>
            <td width=319 valign=top style='width:239.3pt;border:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span lang=UK style='font-size:12.0pt;font-family:"Times New Roman","serif"'>Моторне
  судно, водний мотоцикл (гідроцикл), вітрильне судно</span></p>
            </td>
        </tr>
        <tr>
            <td width=319 valign=top style='width:239.25pt;border:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span lang=UK style='font-size:12.0pt;font-family:"Times New Roman","serif"'>           
  Довжина</span></p>
            </td>
            <td width=319 valign=top style='width:239.3pt;border:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span lang=UK style='font-size:12.0pt;font-family:"Times New Roman","serif"'>до
  6 м, від 6 м до 24 м, до 8,5 м, від 8,5 м – 15 м, до 15 м, від 15 м – 24 м,
  до 24 м.</span></p>
            </td>
        </tr>
        <tr>
            <td width=319 valign=top style='width:239.25pt;border:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span lang=UK style='font-size:12.0pt;font-family:"Times New Roman","serif"'>           
  Площа вітрил</span></p>
            </td>
            <td width=319 valign=top style='width:239.3pt;border:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span lang=UK style='font-size:12.0pt;font-family:"Times New Roman","serif"'>до
  12 кв.м, до 30 кв.м, до 80 кв.м, понад 30-80 кв.м, понад 80 кв.м. </span></p>
            </td>
        </tr>
        <tr>
            <td width=319 valign=top style='width:239.25pt;border:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span lang=UK style='font-size:12.0pt;font-family:"Times New Roman","serif"'>           
  Район плавання</span></p>
            </td>
            <td width=319 valign=top style='width:239.3pt;border:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span lang=UK style='font-size:12.0pt;font-family:"Times New Roman","serif"'>ВВШ,
  ПМШ, Відкрите море</span></p>
            </td>
        </tr>
        <tr>
            <td width=319 valign=top style='width:239.25pt;border:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoListParagraph style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-indent:-18.0pt;line-height:normal'><span lang=UK style='font-size:12.0pt;
  font-family:"Times New Roman","serif"'>8.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span></span><span lang=UK style='font-size:12.0pt;font-family:"Times New Roman","serif"'>Документ
  про закінчення навчально-тренажерного закладу (номер, дата видачі, ким
  виданий)</span></p>
            </td>
            <td width=319 valign=top style='width:239.3pt;border:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal'>
                    <b>
                        <span lang=UK style='font-size:12.0pt;font-family:"Times New Roman","serif"'>Свід.№<?= $certificate->number ?> від <?= $certificate->getDateStart() ?></span>
                    </b>
                    <span lang=UK style='font-size:12.0pt;
  font-family:"Times New Roman","serif"'> <br>
  ТОВ «Міжнародний Морський тренажерний центр»<br>
  <br>
  </span></p>
            </td>
        </tr>
        <tr>
            <td width=319 valign=top style='width:239.25pt;border:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span lang=UK style='font-size:12.0pt;font-family:"Times New Roman","serif"'>«__»
  ______ 20__ р.</span></p>
            </td>
            <td width=319 valign=top style='width:239.3pt;border:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span lang=UK style='font-size:12.0pt;font-family:"Times New Roman","serif"'><br>
  <br>
  </span></p>
            </td>
        </tr>
        <tr>
            <td width=319 valign=top style='width:239.25pt;border:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span lang=UK style='font-size:12.0pt;font-family:"Times New Roman","serif"'>Керівник</span></p>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span lang=UK style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span lang=UK style='font-size:12.0pt;font-family:"Times New Roman","serif"'>________________________
      О.Г. Габ</span></p>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span lang=UK style='font-size:12.0pt;font-family:"Times New Roman","serif"'>          
         (підпис)                       (П.І.Б.)</span></p>
            </td>
            <td width=319 style='width:239.3pt;border:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span lang=UK style='font-size:12.0pt;
  font-family:"Times New Roman","serif"'>Печатка НТЗ</span></p>
            </td>
        </tr>
    </table>

    <div style='border:none;border-bottom:solid windowtext 1.5pt;padding:0cm 0cm 1.0pt 0cm'>

        <p class=MsoNormal align=center style='text-align:center;border:none;
padding:0cm'><span lang=UK style='font-size:12.0pt;line-height:115%;font-family:
"Times New Roman","serif"'>&nbsp;</span></p>

    </div>

    <p class=MsoNoSpacing style='margin-left:36.0pt;text-indent:-18.0pt'><span
                lang=UK style='font-size:12.0pt;font-family:"Times New Roman","serif"'>9.<span
                    style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span
                lang=UK style='font-size:12.0pt;font-family:"Times New Roman","serif"'>Дата,
час та місце проведення засідання кваліфікаційної комісії _________________</span></p>

    <p class=MsoNoSpacing style='margin-left:18.0pt'><span lang=UK
                                                           style='font-size:12.0pt;font-family:"Times New Roman","serif"'>Результати
підтвердження кваліфікації:</span></p>

    <p class=MsoNoSpacing style='margin-left:18.0pt'><span lang=UK
                                                           style='font-size:12.0pt;font-family:"Times New Roman","serif"'>Теоретична
підготовка                   _____________________________________________</span></p>

    <p class=MsoNoSpacing style='margin-left:18.0pt'><span lang=UK
                                                           style='font-size:12.0pt;font-family:"Times New Roman","serif"'>Практична
підготовка                    _____________________________________________</span></p>

    <p class=MsoNoSpacing style='margin-left:36.0pt;text-indent:-18.0pt'><span
                lang=UK style='font-size:12.0pt;font-family:"Times New Roman","serif"'>10.<span
                    style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=UK
                                                                                    style='font-size:12.0pt;font-family:"Times New Roman","serif"'>Номер та дата
складання протоколу кваліфікаційної комісії ____________________</span></p>

    <div style='border:none;border-bottom:solid windowtext 1.5pt;padding:0cm 0cm 4.0pt 0cm;
margin-left:18.0pt;margin-right:0cm'>

        <p class=MsoNoSpacing style='margin-left:18.0pt;text-indent:-18.0pt;border:
none;padding:0cm'><span lang=UK style='font-size:12.0pt;font-family:"Times New Roman","serif"'>11.<span
                        style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=UK
                                                                                        style='font-size:12.0pt;font-family:"Times New Roman","serif"'>Особливі
відмітки</span></p>

        <p class=MsoNoSpacing style='border:none;padding:0cm'><span lang=UK
                                                                    style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

    </div>

    <p class=MsoNoSpacing><span lang=UK style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

    <p class=MsoNoSpacing><span lang=UK style='font-size:12.0pt;font-family:"Times New Roman","serif"'>Секретар
кваліфікаційної комісії   _______________    _________________________</span></p>

    <p class=MsoNoSpacing style='margin-left:36.0pt'><span lang=UK
                                                           style='font-size:12.0pt;font-family:"Times New Roman","serif"'>                                                        
(підпис)                           (П.І.Б.)</span></p>

    <p class=MsoNoSpacing><span lang=UK style='font-size:12.0pt;font-family:"Times New Roman","serif"'>«___»
_____________ 20___ р.</span></p>

</div>

</body>

</html>
