<?php

use yii\db\Migration;

/**
 * Class m190609_085421_addUserCertificateTable
 */
class m190609_085421_addUserCertificateTable extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%client_certificate}}', [
            'id'             => $this->primaryKey(),
            'group_id'       => $this->integer()->notNull(),
            'client_id'      => $this->integer()->notNull(),
            'number'         => $this->string()->null(),
            'status'         => $this->integer()->notNull(),
            'cost_price'     => $this->decimal(12, 2)->null(),
            'bank_sum'       => $this->decimal(12, 2)->null(),
            'additional_sum' => $this->decimal(12, 2)->null(),
            'total_sum'      => $this->decimal(12, 2)->null(),
            'paid'           => $this->boolean()->defaultValue(false),
            'issued'         => $this->boolean()->defaultValue(false),
            'middlemen_id'   => $this->integer(),
        ], $tableOptions);

        $this->createIndex('client_certificate_group_id_i', '{{%client_certificate}}', ['group_id']);
        $this->createIndex('client_certificate_client_id_i', '{{%client_certificate}}', ['client_id']);

        $this->addForeignKey('client_certificate_group_id_fk', '{{%client_certificate}}', 'group_id', '{{%group}}', 'id', 'restrict', 'cascade');
        $this->addForeignKey('client_certificate_client_id_fk', '{{%client_certificate}}', 'client_id', '{{%client}}', 'id', 'restrict', 'cascade');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('client_certificate_group_id_fk', '{{%client_certificate}}');
        $this->dropForeignKey('client_certificate_client_id_fk', '{{%client_certificate}}');

        $this->dropIndex('client_certificate_group_id_i', '{{%client_certificate}}');
        $this->dropIndex('client_certificate_client_id_i', '{{%client_certificate}}');

        $this->dropTable('{{%client_certificate}}');
    }
}
