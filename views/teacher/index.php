<?php

use app\helpers\AuthHelper;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\searches\TeacherSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Преподаватели';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="teacher-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить преподавателя', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],


            'full_name',
            'requirements:ntext',
            'diploma',
            'work_diploma',
            'certificate',
            'experience',
            //'rank',
            //'ship_type',
            //'phone',

            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => AuthHelper::canEditTeacher() ? '{view} {update}' : '{view}',
            ],
        ],
    ]); ?>


</div>
