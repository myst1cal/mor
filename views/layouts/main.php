<?php

/* @var $this View */

/* @var $content string */

use app\assets\AppAsset;
use app\helpers\AuthHelper;
use app\widgets\Alert;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\Breadcrumbs;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl'   => Yii::$app->homeUrl,
        'options'    => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options'      => ['class' => 'navbar-nav navbar-right'],
        'encodeLabels' => false,
        'items'        =>
            Yii::$app->user->isGuest ? (
            [['label' => 'Авторизоваться', 'url' => ['/auth/login']]]
            ) :
                ([
                    ['label' => '<span class="glyphicon glyphicon-user">', 'url' => ['/']],
                    AuthHelper::canSeeMenuReport() ? [
                        'label' => 'Отчеты',
                        'items' => [
                            ['label' => 'Отчет', 'url' => ['/report/by-dates'], 'class' => ''],
                            '<li class="divider"></li>',
                            ['label' => 'Отчет 2', 'url' => ['/report/by-day'], 'class' => ''],
                            '<li class="divider"></li>',
                            ['label' => 'Отчет 3', 'url' => ['/report/statistic'], 'class' => ''],
                            '<li class="divider"></li>',
                            ['label' => 'Отчет 4', 'url' => ['/report/by-middlemen'], 'class' => ''],
                        ],
                    ] : '',
                    AuthHelper::canPrintCertificate() ? ['label' => 'Печать', 'url' => ['/report/print-certificates'], 'class' => ''] : '',
                    AuthHelper::canSeeMenuClients() ? ['label' => 'Клиенты', 'url' => ['/client']] : '',
                    AuthHelper::canSeeMenuTeachers() ? ['label' => 'Преподаватели', 'url' => ['/teacher']] : '',
                    AuthHelper::canSeeMenuMiddlemens() ? ['label' => 'Посредники', 'url' => ['/middlemen']] : '',
                    AuthHelper::canSeeMenuCourses() ? ['label' => 'Курсы', 'url' => ['/course']] : '',
                    AuthHelper::canSeeMenuGroups() ? ['label' => 'Группы', 'url' => ['/group']] : '',
                    AuthHelper::canSeeMenuUsers() ? ['label' => 'Пользователи', 'url' => ['/user']] : '',
                    AuthHelper::canSeeMenuCertificates() && Yii::$app->user->identity->login == 'admin' ? ['label' => 'Сертификаты', 'url' => ['/certificate']] : '',
					AuthHelper::canSeeMenuCertificates() && Yii::$app->user->identity->login == 'Viktory' ? ['label' => 'Сертификаты', 'url' => ['/certificate']] : '',
					AuthHelper::canSeeMenuCertificates() && Yii::$app->user->identity->login == 'a.goncharenko' ? ['label' => 'Сертификаты', 'url' => ['/certificate']] : '',
                    Yii::$app->user->identity->login == 'admin' ? ['label' => 'Доп. поля', 'url' => ['/course-additional-field']] : '',
                    '<li>'
                    . Html::beginForm(['/auth/logout'], 'post')
                    . Html::submitButton(
                        'Выход (' . Yii::$app->user->identity->login . ')',
                        ['class' => 'btn btn-link logout']
                    )
                    . Html::endForm()
                    . '</li>',
                ]),
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; ТОВ "ЕМТЦ" <?= date('Y') ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
