<?php
/** @var $certificate \app\models\ars\ClientCertificate */
?>
<html xmlns:v="urn:schemas-microsoft-com:vml"
      xmlns:o="urn:schemas-microsoft-com:office:office"
      xmlns="http://www.w3.org/TR/REC-html40">

<head>
    <meta http-equiv=Content-Type content="text/html; charset=windows-1251">
    <meta name=ProgId content=Word.Document>
    <meta name=Generator content="Microsoft Word 14">
    <meta name=Originator content="Microsoft Word 14">
    <!--[if !mso]>
    <style>
        v\: * {
            behavior: url(#default#VML);
        }

        o\: * {
            behavior: url(#default#VML);
        }

        w\: * {
            behavior: url(#default#VML);
        }

        .shape {
            behavior: url(#default#VML);
        }
    </style>
    <![endif]--><!--[if gte mso 9]>
    <xml>
        <o:DocumentProperties>
            <o:Author>Falcon</o:Author>
            <o:LastAuthor>Falcon</o:LastAuthor>
            <o:Revision>3</o:Revision>
            <o:TotalTime>49</o:TotalTime>
            <o:Created>2019-04-02T14:15:00Z</o:Created>
            <o:LastSaved>2019-04-02T14:54:00Z</o:LastSaved>
            <o:Pages>1</o:Pages>
            <o:Words>552</o:Words>
            <o:Characters>3149</o:Characters>
            <o:Lines>26</o:Lines>
            <o:Paragraphs>7</o:Paragraphs>
            <o:CharactersWithSpaces>3694</o:CharactersWithSpaces>
            <o:Version>14.00</o:Version>
        </o:DocumentProperties>
    </xml><![endif]-->
    <!--[if gte mso 9]>
    <xml>
        <w:WordDocument>
            <w:Zoom>FullPage</w:Zoom>
            <w:SpellingState>Clean</w:SpellingState>
            <w:GrammarState>Clean</w:GrammarState>
            <w:TrackMoves>false</w:TrackMoves>
            <w:TrackFormatting/>
            <w:ValidateAgainstSchemas/>
            <w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid>
            <w:IgnoreMixedContent>false</w:IgnoreMixedContent>
            <w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText>
            <w:DoNotPromoteQF/>
            <w:LidThemeOther>RU</w:LidThemeOther>
            <w:LidThemeAsian>X-NONE</w:LidThemeAsian>
            <w:LidThemeComplexScript>X-NONE</w:LidThemeComplexScript>
            <w:Compatibility>
                <w:BreakWrappedTables/>
                <w:SplitPgBreakAndParaMark/>
            </w:Compatibility>
            <w:BrowserLevel>MicrosoftInternetExplorer4</w:BrowserLevel>
            <m:mathPr>
                <m:mathFont m:val="Cambria Math"/>
                <m:brkBin m:val="before"/>
                <m:brkBinSub m:val="&#45;-"/>
                <m:smallFrac m:val="off"/>
                <m:dispDef/>
                <m:lMargin m:val="0"/>
                <m:rMargin m:val="0"/>
                <m:defJc m:val="centerGroup"/>
                <m:wrapIndent m:val="1440"/>
                <m:intLim m:val="subSup"/>
                <m:naryLim m:val="undOvr"/>
            </m:mathPr>
        </w:WordDocument>
    </xml><![endif]--><!--[if gte mso 9]>
    <xml>
        <w:LatentStyles DefLockedState="false" DefUnhideWhenUsed="true"
                        DefSemiHidden="true" DefQFormat="false" DefPriority="99"
                        LatentStyleCount="267">
            <w:LsdException Locked="false" Priority="0" SemiHidden="false"
                            UnhideWhenUsed="false" QFormat="true" Name="Normal"/>
            <w:LsdException Locked="false" Priority="9" SemiHidden="false"
                            UnhideWhenUsed="false" QFormat="true" Name="heading 1"/>
            <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 2"/>
            <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 3"/>
            <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 4"/>
            <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 5"/>
            <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 6"/>
            <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 7"/>
            <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 8"/>
            <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 9"/>
            <w:LsdException Locked="false" Priority="39" Name="toc 1"/>
            <w:LsdException Locked="false" Priority="39" Name="toc 2"/>
            <w:LsdException Locked="false" Priority="39" Name="toc 3"/>
            <w:LsdException Locked="false" Priority="39" Name="toc 4"/>
            <w:LsdException Locked="false" Priority="39" Name="toc 5"/>
            <w:LsdException Locked="false" Priority="39" Name="toc 6"/>
            <w:LsdException Locked="false" Priority="39" Name="toc 7"/>
            <w:LsdException Locked="false" Priority="39" Name="toc 8"/>
            <w:LsdException Locked="false" Priority="39" Name="toc 9"/>
            <w:LsdException Locked="false" Priority="35" QFormat="true" Name="caption"/>
            <w:LsdException Locked="false" Priority="10" SemiHidden="false"
                            UnhideWhenUsed="false" QFormat="true" Name="Title"/>
            <w:LsdException Locked="false" Priority="1" Name="Default Paragraph Font"/>
            <w:LsdException Locked="false" Priority="11" SemiHidden="false"
                            UnhideWhenUsed="false" QFormat="true" Name="Subtitle"/>
            <w:LsdException Locked="false" Priority="22" SemiHidden="false"
                            UnhideWhenUsed="false" QFormat="true" Name="Strong"/>
            <w:LsdException Locked="false" Priority="20" SemiHidden="false"
                            UnhideWhenUsed="false" QFormat="true" Name="Emphasis"/>
            <w:LsdException Locked="false" Priority="59" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Table Grid"/>
            <w:LsdException Locked="false" UnhideWhenUsed="false" Name="Placeholder Text"/>
            <w:LsdException Locked="false" Priority="1" SemiHidden="false"
                            UnhideWhenUsed="false" QFormat="true" Name="No Spacing"/>
            <w:LsdException Locked="false" Priority="60" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Light Shading"/>
            <w:LsdException Locked="false" Priority="61" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Light List"/>
            <w:LsdException Locked="false" Priority="62" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Light Grid"/>
            <w:LsdException Locked="false" Priority="63" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium Shading 1"/>
            <w:LsdException Locked="false" Priority="64" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium Shading 2"/>
            <w:LsdException Locked="false" Priority="65" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium List 1"/>
            <w:LsdException Locked="false" Priority="66" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium List 2"/>
            <w:LsdException Locked="false" Priority="67" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium Grid 1"/>
            <w:LsdException Locked="false" Priority="68" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium Grid 2"/>
            <w:LsdException Locked="false" Priority="69" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium Grid 3"/>
            <w:LsdException Locked="false" Priority="70" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Dark List"/>
            <w:LsdException Locked="false" Priority="71" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Colorful Shading"/>
            <w:LsdException Locked="false" Priority="72" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Colorful List"/>
            <w:LsdException Locked="false" Priority="73" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Colorful Grid"/>
            <w:LsdException Locked="false" Priority="60" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Light Shading Accent 1"/>
            <w:LsdException Locked="false" Priority="61" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Light List Accent 1"/>
            <w:LsdException Locked="false" Priority="62" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Light Grid Accent 1"/>
            <w:LsdException Locked="false" Priority="63" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium Shading 1 Accent 1"/>
            <w:LsdException Locked="false" Priority="64" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium Shading 2 Accent 1"/>
            <w:LsdException Locked="false" Priority="65" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium List 1 Accent 1"/>
            <w:LsdException Locked="false" UnhideWhenUsed="false" Name="Revision"/>
            <w:LsdException Locked="false" Priority="34" SemiHidden="false"
                            UnhideWhenUsed="false" QFormat="true" Name="List Paragraph"/>
            <w:LsdException Locked="false" Priority="29" SemiHidden="false"
                            UnhideWhenUsed="false" QFormat="true" Name="Quote"/>
            <w:LsdException Locked="false" Priority="30" SemiHidden="false"
                            UnhideWhenUsed="false" QFormat="true" Name="Intense Quote"/>
            <w:LsdException Locked="false" Priority="66" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium List 2 Accent 1"/>
            <w:LsdException Locked="false" Priority="67" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium Grid 1 Accent 1"/>
            <w:LsdException Locked="false" Priority="68" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium Grid 2 Accent 1"/>
            <w:LsdException Locked="false" Priority="69" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium Grid 3 Accent 1"/>
            <w:LsdException Locked="false" Priority="70" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Dark List Accent 1"/>
            <w:LsdException Locked="false" Priority="71" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Colorful Shading Accent 1"/>
            <w:LsdException Locked="false" Priority="72" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Colorful List Accent 1"/>
            <w:LsdException Locked="false" Priority="73" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Colorful Grid Accent 1"/>
            <w:LsdException Locked="false" Priority="60" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Light Shading Accent 2"/>
            <w:LsdException Locked="false" Priority="61" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Light List Accent 2"/>
            <w:LsdException Locked="false" Priority="62" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Light Grid Accent 2"/>
            <w:LsdException Locked="false" Priority="63" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium Shading 1 Accent 2"/>
            <w:LsdException Locked="false" Priority="64" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium Shading 2 Accent 2"/>
            <w:LsdException Locked="false" Priority="65" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium List 1 Accent 2"/>
            <w:LsdException Locked="false" Priority="66" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium List 2 Accent 2"/>
            <w:LsdException Locked="false" Priority="67" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium Grid 1 Accent 2"/>
            <w:LsdException Locked="false" Priority="68" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium Grid 2 Accent 2"/>
            <w:LsdException Locked="false" Priority="69" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium Grid 3 Accent 2"/>
            <w:LsdException Locked="false" Priority="70" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Dark List Accent 2"/>
            <w:LsdException Locked="false" Priority="71" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Colorful Shading Accent 2"/>
            <w:LsdException Locked="false" Priority="72" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Colorful List Accent 2"/>
            <w:LsdException Locked="false" Priority="73" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Colorful Grid Accent 2"/>
            <w:LsdException Locked="false" Priority="60" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Light Shading Accent 3"/>
            <w:LsdException Locked="false" Priority="61" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Light List Accent 3"/>
            <w:LsdException Locked="false" Priority="62" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Light Grid Accent 3"/>
            <w:LsdException Locked="false" Priority="63" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium Shading 1 Accent 3"/>
            <w:LsdException Locked="false" Priority="64" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium Shading 2 Accent 3"/>
            <w:LsdException Locked="false" Priority="65" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium List 1 Accent 3"/>
            <w:LsdException Locked="false" Priority="66" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium List 2 Accent 3"/>
            <w:LsdException Locked="false" Priority="67" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium Grid 1 Accent 3"/>
            <w:LsdException Locked="false" Priority="68" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium Grid 2 Accent 3"/>
            <w:LsdException Locked="false" Priority="69" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium Grid 3 Accent 3"/>
            <w:LsdException Locked="false" Priority="70" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Dark List Accent 3"/>
            <w:LsdException Locked="false" Priority="71" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Colorful Shading Accent 3"/>
            <w:LsdException Locked="false" Priority="72" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Colorful List Accent 3"/>
            <w:LsdException Locked="false" Priority="73" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Colorful Grid Accent 3"/>
            <w:LsdException Locked="false" Priority="60" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Light Shading Accent 4"/>
            <w:LsdException Locked="false" Priority="61" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Light List Accent 4"/>
            <w:LsdException Locked="false" Priority="62" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Light Grid Accent 4"/>
            <w:LsdException Locked="false" Priority="63" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium Shading 1 Accent 4"/>
            <w:LsdException Locked="false" Priority="64" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium Shading 2 Accent 4"/>
            <w:LsdException Locked="false" Priority="65" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium List 1 Accent 4"/>
            <w:LsdException Locked="false" Priority="66" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium List 2 Accent 4"/>
            <w:LsdException Locked="false" Priority="67" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium Grid 1 Accent 4"/>
            <w:LsdException Locked="false" Priority="68" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium Grid 2 Accent 4"/>
            <w:LsdException Locked="false" Priority="69" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium Grid 3 Accent 4"/>
            <w:LsdException Locked="false" Priority="70" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Dark List Accent 4"/>
            <w:LsdException Locked="false" Priority="71" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Colorful Shading Accent 4"/>
            <w:LsdException Locked="false" Priority="72" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Colorful List Accent 4"/>
            <w:LsdException Locked="false" Priority="73" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Colorful Grid Accent 4"/>
            <w:LsdException Locked="false" Priority="60" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Light Shading Accent 5"/>
            <w:LsdException Locked="false" Priority="61" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Light List Accent 5"/>
            <w:LsdException Locked="false" Priority="62" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Light Grid Accent 5"/>
            <w:LsdException Locked="false" Priority="63" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium Shading 1 Accent 5"/>
            <w:LsdException Locked="false" Priority="64" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium Shading 2 Accent 5"/>
            <w:LsdException Locked="false" Priority="65" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium List 1 Accent 5"/>
            <w:LsdException Locked="false" Priority="66" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium List 2 Accent 5"/>
            <w:LsdException Locked="false" Priority="67" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium Grid 1 Accent 5"/>
            <w:LsdException Locked="false" Priority="68" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium Grid 2 Accent 5"/>
            <w:LsdException Locked="false" Priority="69" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium Grid 3 Accent 5"/>
            <w:LsdException Locked="false" Priority="70" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Dark List Accent 5"/>
            <w:LsdException Locked="false" Priority="71" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Colorful Shading Accent 5"/>
            <w:LsdException Locked="false" Priority="72" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Colorful List Accent 5"/>
            <w:LsdException Locked="false" Priority="73" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Colorful Grid Accent 5"/>
            <w:LsdException Locked="false" Priority="60" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Light Shading Accent 6"/>
            <w:LsdException Locked="false" Priority="61" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Light List Accent 6"/>
            <w:LsdException Locked="false" Priority="62" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Light Grid Accent 6"/>
            <w:LsdException Locked="false" Priority="63" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium Shading 1 Accent 6"/>
            <w:LsdException Locked="false" Priority="64" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium Shading 2 Accent 6"/>
            <w:LsdException Locked="false" Priority="65" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium List 1 Accent 6"/>
            <w:LsdException Locked="false" Priority="66" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium List 2 Accent 6"/>
            <w:LsdException Locked="false" Priority="67" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium Grid 1 Accent 6"/>
            <w:LsdException Locked="false" Priority="68" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium Grid 2 Accent 6"/>
            <w:LsdException Locked="false" Priority="69" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium Grid 3 Accent 6"/>
            <w:LsdException Locked="false" Priority="70" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Dark List Accent 6"/>
            <w:LsdException Locked="false" Priority="71" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Colorful Shading Accent 6"/>
            <w:LsdException Locked="false" Priority="72" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Colorful List Accent 6"/>
            <w:LsdException Locked="false" Priority="73" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Colorful Grid Accent 6"/>
            <w:LsdException Locked="false" Priority="19" SemiHidden="false"
                            UnhideWhenUsed="false" QFormat="true" Name="Subtle Emphasis"/>
            <w:LsdException Locked="false" Priority="21" SemiHidden="false"
                            UnhideWhenUsed="false" QFormat="true" Name="Intense Emphasis"/>
            <w:LsdException Locked="false" Priority="31" SemiHidden="false"
                            UnhideWhenUsed="false" QFormat="true" Name="Subtle Reference"/>
            <w:LsdException Locked="false" Priority="32" SemiHidden="false"
                            UnhideWhenUsed="false" QFormat="true" Name="Intense Reference"/>
            <w:LsdException Locked="false" Priority="33" SemiHidden="false"
                            UnhideWhenUsed="false" QFormat="true" Name="Book Title"/>
            <w:LsdException Locked="false" Priority="37" Name="Bibliography"/>
            <w:LsdException Locked="false" Priority="39" QFormat="true" Name="TOC Heading"/>
        </w:LatentStyles>
    </xml><![endif]-->
    <style>
        <!--
        /* Font Definitions */
        @font-face {
            font-family: Calibri;
            panose-1: 2 15 5 2 2 2 4 3 2 4;
            mso-font-charset: 204;
            mso-generic-font-family: swiss;
            mso-font-pitch: variable;
            mso-font-signature: -536870145 1073786111 1 0 415 0;
        }

        @font-face {
            font-family: Tahoma;
            panose-1: 2 11 6 4 3 5 4 4 2 4;
            mso-font-charset: 204;
            mso-generic-font-family: swiss;
            mso-font-pitch: variable;
            mso-font-signature: -520081665 -1073717157 41 0 66047 0;
        }

        /* Style Definitions */
        p.MsoNormal, li.MsoNormal, div.MsoNormal {
            mso-style-unhide: no;
            mso-style-qformat: yes;
            mso-style-parent: "";
            margin-top: 0cm;
            margin-right: 0cm;
            margin-bottom: 10.0pt;
            margin-left: 0cm;
            line-height: 115%;
            mso-pagination: widow-orphan;
            font-size: 11.0pt;
            font-family: "Calibri", "sans-serif";
            mso-fareast-font-family: "Times New Roman";
            mso-fareast-theme-font: minor-fareast;
            mso-bidi-font-family: "Times New Roman";
            text-decoration: none !important;
        }

        a:-webkit-any-link {
            color: #000;
            cursor: pointer;
            text-decoration: none !important;
        }

        p.MsoAcetate, li.MsoAcetate, div.MsoAcetate {
            mso-style-noshow: yes;
            mso-style-priority: 99;
            mso-style-link: "Текст выноски Знак";
            margin: 0cm;
            margin-bottom: .0001pt;
            mso-pagination: widow-orphan;
            font-size: 8.0pt;
            font-family: "Tahoma", "sans-serif";
            mso-fareast-font-family: "Times New Roman";
            mso-fareast-theme-font: minor-fareast;
        }

        span.a {
            mso-style-name: "Текст выноски Знак";
            text-decoration: none !important;
            mso-style-noshow: yes;
            mso-style-priority: 99;
            mso-style-unhide: no;
            mso-style-locked: yes;
            mso-style-link: "Текст выноски";
            font-family: "Tahoma", "sans-serif";
            mso-ascii-font-family: Tahoma;
            mso-hansi-font-family: Tahoma;
            mso-bidi-font-family: Tahoma;
        }

        p.msopapdefault, li.msopapdefault, div.msopapdefault {
            mso-style-name: msopapdefault;
            mso-style-unhide: no;
            mso-margin-top-alt: auto;
            margin-right: 0cm;
            margin-bottom: 10.0pt;
            margin-left: 0cm;
            line-height: 115%;
            mso-pagination: widow-orphan;
            font-size: 12.0pt;
            font-family: "Times New Roman", "serif";
            mso-fareast-font-family: "Times New Roman";
            mso-fareast-theme-font: minor-fareast;
        }

        span.SpellE {
            mso-style-name: "";
            mso-spl-e: yes;
        }

        span.GramE {
            mso-style-name: "";
            mso-gram-e: yes;
        }

        .MsoChpDefault {
            mso-style-type: export-only;
            mso-default-props: yes;
            font-size: 10.0pt;
            mso-ansi-font-size: 10.0pt;
            mso-bidi-font-size: 10.0pt;
        }

        @page WordSection1 {
            size: 595.3pt 841.9pt;
            margin: 0cm 0cm 0cm 0cm;
            mso-header-margin: 35.4pt;
            mso-footer-margin: 35.4pt;
            mso-paper-source: 0;
        }

        div.WordSection1 {
            page: WordSection1;
        }

        -->
    </style>
    <!--[if gte mso 10]>
    <style>
        /* Style Definitions */
        table.MsoNormalTable {
            mso-style-name: "Обычная таблица";
            mso-tstyle-rowband-size: 0;
            mso-tstyle-colband-size: 0;
            mso-style-noshow: yes;
            mso-style-priority: 99;
            mso-style-parent: "";
            mso-padding-alt: 0cm 5.4pt 0cm 5.4pt;
            mso-para-margin: 0cm;
            mso-para-margin-bottom: .0001pt;
            mso-pagination: widow-orphan;
            font-size: 10.0pt;
            font-family: "Times New Roman", "serif";
        }
    </style>
    <![endif]--><!--[if gte mso 9]>
    <xml>
        <o:shapedefaults v:ext="edit" spidmax="1028"/>
    </xml><![endif]--><!--[if gte mso 9]>
    <xml>
        <o:shapelayout v:ext="edit">
            <o:idmap v:ext="edit" data="1"/>
        </o:shapelayout>
    </xml><![endif]-->
</head>

<body lang=RU style='tab-interval:35.4pt'>

<div class=WordSection1>

    <div align=center>

        <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=794 style='width:595.5pt;border-collapse:collapse;mso-yfti-tbllook:1184;mso-padding-alt:0cm 0cm 0cm 0cm'>
            <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes;page-break-inside:avoid;height:419.6pt;mso-height-rule:exactly'>
                <td width="50%" valign=top style='width:50.0%;padding:2.85pt 5.4pt 0cm 5.4pt;height:419.6pt;mso-height-rule:exactly'>
                    <div align=center>
                        <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=381 style='width:285.95pt;border-collapse:collapse;mso-yfti-tbllook:1184;
   mso-padding-alt:0cm 0cm 0cm 0cm'>
                            <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;page-break-inside:avoid;
    height:36.85pt;mso-height-rule:exactly'>
                                <td width=114 colspan=3 rowspan=2 style='width:85.8pt;padding:0cm 5.4pt 0cm 5.4pt;
    height:36.85pt;mso-height-rule:exactly'></td>
                                <td width=267 colspan=5 valign=top style='width:200.15pt;padding:0cm 5.4pt 0cm 5.4pt;
    height:36.85pt;mso-height-rule:exactly'></td>
                            </tr>
                            <tr style='mso-yfti-irow:1;height:36.85pt;mso-height-rule:exactly'>
                                <td width=133 colspan=3 style='width:100.0pt;padding:0cm 5.4pt 0cm 5.4pt;
    height:36.85pt;mso-height-rule:exactly'></td>
                                <td width=134 colspan=2 style='width:100.15pt;padding:0cm 5.4pt 0cm 5.4pt;
    height:36.85pt;mso-height-rule:exactly'></td>
                            </tr>
                            <tr style='mso-yfti-irow:2;height:19.85pt'>
                                <td width=381 colspan=8 style='width:285.95pt;padding:0cm 5.4pt 0cm 5.4pt;
    height:19.85pt'>
                                    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:
    .0001pt;text-align:center;line-height:normal'>
                                        <b>
                                            <span style='font-size:11.0pt;font-family:"Times New Roman","serif"'>СВІДОЦТВО ФАХІВЦЯ</span>&nbsp;
                                            <span style='font-size:12.0pt;font-family:"Times New Roman","serif"'>№ <?= $certificate->number ?></span>
                                        </b>
                                    </p>
                                </td>
                            </tr>
                            <tr style='mso-yfti-irow:3;height:22.7pt'>
                                <td width=378 colspan=8 valign=top style='width:10.0cm;padding:0cm 5.4pt 0cm 5.4pt;
    height:22.7pt'>
         <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt; line-height:normal'></p>
                                </td>
                            </tr>
                            <tr style='mso-yfti-irow:4;height:11.35pt'>
                                <td width=381 colspan=8 style='width:285.95pt;padding:0cm 5.4pt 0cm 5.4pt;
    height:11.35pt'>
                                    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;
    line-height:normal'><span class=SpellE><span style='font-size:7.0pt;
    font-family:"Times New Roman","serif"'>Цим</span></span><span
                                                style='font-size:7.0pt;font-family:"Times New Roman","serif"'> <span
                                                    class=SpellE>засвідчується</span>, <span
                                                    class=SpellE>що</span>:</span><span
                                                style='font-size:7.0pt'><o:p></o:p></span></p>
                                </td>
                            </tr>
                            <tr style='mso-yfti-irow:5;height:19.85pt'>
                                <td width=381 colspan=8 style='width:285.95pt;padding:0cm 5.4pt 0cm 5.4pt;
    height:19.85pt'>
                                    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:
    .0001pt;text-align:center;line-height:normal'><b><i><span
                                                        style='font-size:14.0pt;font-family:"Times New Roman","serif";'><?= $certificate->client->surname_ukr ?></span></i></b>
                                    </p>
                                </td>
                            </tr>
                            <tr style='mso-yfti-irow:6;height:19.85pt'>
                                <td width=381 colspan=8 style='width:285.95pt;padding:0cm 5.4pt 0cm 5.4pt;
    height:19.85pt'>
                                    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:
    .0001pt;text-align:center;line-height:normal'><b><i><span
                                                        style='font-size:14.0pt;font-family:"Times New Roman","serif";'> <?= $certificate->client->name_ukr . ' ' . $certificate->client->patronymic_ukr ?></span></i></b>
                                    </p>
                                </td>
                            </tr>
                            <tr style='mso-yfti-irow:7;height:17.0pt;mso-height-rule:exactly'>
                                <td width=114 colspan=3 style='width:85.8pt;padding:0cm 5.4pt 0cm 5.4pt;
    height:17.0pt;mso-height-rule:exactly'>
                                    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;
    line-height:normal'>
                                        <b><span style='font-size:8.0pt;font-family:"Times New Roman","serif"'>Дата&nbsp;народження:</span></b>
                                    </p>
                                </td>
                                <td width=152 colspan=4 style='width:113.9pt;padding:0cm 5.4pt 0cm 5.4pt;
    height:17.0pt;mso-height-rule:exactly'>
                                    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:
    .0001pt;text-align:center;line-height:normal'><b><span lang=EN-US
                                                           style='font-size:12.0pt;font-family:"Times New Roman","serif";mso-ansi-language:
    EN-US'><?= Yii::$app->formatter->asDate($certificate->client->birthday) ?></span></b></p>
                                </td>
                                <td width=115 valign=top style='width:86.25pt;padding:0cm 5.4pt 0cm 5.4pt;
    height:17.0pt;mso-height-rule:exactly'>
                                    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;
    line-height:normal'><span lang=EN-US style='font-size:8.0pt;font-family:
    "Times New Roman","serif";mso-ansi-language:EN-US'>&nbsp;</span></p>
                                </td>
                            </tr>
                            <tr style='mso-yfti-irow:8;height:19.85pt'>
                                <td width=381 colspan=8 style='width:285.95pt;padding:0cm 5.4pt 0cm 5.4pt;
    height:19.85pt'>
                                   <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;
    line-height:normal'><span style='font-size:7.0pt;
    font-family:"Times New Roman","serif"'>успішно закінчив курс навчання та оцінювання компетентності відповідно до сучасних вимог роботодавців та отримав відповідну кваліфікацію, щоб бути прийнятим на посаду</span></p>
                                </td>
                            </tr>
                            <tr style='mso-yfti-irow:9;page-break-inside:avoid;height:51.05pt'>
                                <td width=381 colspan=8 style='width:285.95pt;padding:0cm 5.4pt 0cm 5.4pt;
    height:51.05pt'>
                                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:
    .0001pt;text-align:center;line-height:normal'><b><i><span style='font-size:
    10.0pt;font-family:"Times New Roman","serif";color:black'>ПОМІЧНИК СТЮАРДА</span></i></b></p>
                                </td>
                            </tr>
                            <tr style='mso-yfti-irow:10;height:17.0pt'>
                                <td width=381 colspan=8 valign=top style='width:285.95pt;padding:0cm 5.4pt 0cm 5.4pt;
    height:17.0pt'>
                                 <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;
    line-height:normal'><span style='font-size:7.0pt;
    font-family:"Times New Roman","serif"'>і має право проходити службу на судні в будь-якій якості, що вимагає такого сертифіката.</span></p>
                                </td>
                            </tr>
                            <tr style='mso-yfti-irow:11;height:22.7pt'>
                                <td width=92 colspan=2 rowspan=4 style='width:69.35pt;padding:0cm 5.4pt 0cm 5.4pt;
    height:22.7pt'>&nbsp;</td>
                                <td width=174 colspan=5 valign=bottom style='width:130.35pt;padding:0cm 5.4pt 0cm 5.4pt;
    height:22.7pt'>
                                   <p class=MsoNormal align=right style='margin-bottom:0cm;margin-bottom:.0001pt;
    text-align:right;line-height:normal'><span class=SpellE><span
                                                    style='font-size:8.0pt;font-family:"Times New Roman","serif"'>Керівник</span></span><span
                                                    style='font-size:8.0pt;font-family:"Times New Roman","serif"'>&nbsp;&nbsp;&nbsp;&nbsp;
    .</span><span style='font-size:8.0pt;font-family:"Times New Roman","serif"'>.……………......…</span><span
                                                lang=EN-US style='font-size:8.0pt;font-family:"Times New Roman","serif";
    mso-ansi-language:EN-US'>...</span></p>
                                </td>
                                <td width=115 rowspan=2 valign=top style='width:86.25pt;padding:0cm 5.4pt 0cm 5.4pt;
    height:22.7pt'>
                                    <p class=MsoNormal align=right style='margin-bottom:0cm;margin-bottom:.0001pt;
    text-align:right;line-height:normal'><!--[if gte vml 1]>
                                        <v:shapetype id="_x0000_t75"
                                                     coordsize="21600,21600" o:spt="75" o:preferrelative="t"
                                                     path="m@4@5l@4@11@9@11@9@5xe"
                                                     filled="f" stroked="f">
                                            <v:stroke joinstyle="miter"/>
                                            <v:formulas>
                                                <v:f eqn="if lineDrawn pixelLineWidth 0"/>
                                                <v:f eqn="sum @0 1 0"/>
                                                <v:f eqn="sum 0 0 @1"/>
                                                <v:f eqn="prod @2 1 2"/>
                                                <v:f eqn="prod @3 21600 pixelWidth"/>
                                                <v:f eqn="prod @3 21600 pixelHeight"/>
                                                <v:f eqn="sum @0 0 1"/>
                                                <v:f eqn="prod @6 1 2"/>
                                                <v:f eqn="prod @7 21600 pixelWidth"/>
                                                <v:f eqn="sum @8 21600 0"/>
                                                <v:f eqn="prod @7 21600 pixelHeight"/>
                                                <v:f eqn="sum @10 21600 0"/>
                                            </v:formulas>
                                            <v:path o:extrusionok="f" gradientshapeok="t" o:connecttype="rect"/>
                                            <o:lock v:ext="edit" aspectratio="t"/>
                                        </v:shapetype>
                                        <v:shape id="Рисунок_x0020_8" o:spid="_x0000_s1027" type="#_x0000_t75"
                                                 style='width:39.75pt;height:39.75pt;visibility:visible;mso-wrap-style:square;
     mso-left-percent:-10001;mso-top-percent:-10001;mso-position-horizontal:absolute;
     mso-position-horizontal-relative:char;mso-position-vertical:absolute;
     mso-position-vertical-relative:line;mso-left-percent:-10001;
     mso-top-percent:-10001'>
                                            <w:wrap type="none"/>
                                            <w:anchorlock/>
                                        </v:shape><![endif]--><![if !vml]><img width=53 height=53
                                                                               src="/images/certificate/image001.jpg"
                                                                               v:shapes="Рисунок_x0020_8"><![endif]></p>
                                </td>
                            </tr>
                            <tr style='mso-yfti-irow:12;height:22.7pt'>
                                <td width=174 colspan=5 valign=bottom style='width:130.35pt;padding:0cm 5.4pt 0cm 5.4pt;
    height:22.7pt'>
                                    <p class=MsoNormal align=right style='margin-bottom:0cm;margin-bottom:.0001pt;
    text-align:right;line-height:normal'><span class=SpellE><span
                                                    style='font-size:8.0pt;font-family:"Times New Roman","serif"'>Реєстратор</span></span><span
                                                class=GramE><span
                                                    style='font-size:8.0pt;font-family:"Times New Roman","serif"'>&nbsp;&nbsp;
    .</span></span><span style='font-size:8.0pt;font-family:"Times New Roman","serif"'>.……………......</span><span
                                                lang=EN-US style='font-size:8.0pt;font-family:"Times New Roman","serif";
    mso-ansi-language:EN-US'>......</span><span lang=EN-US style='mso-ansi-language:
    EN-US'><o:p></o:p></span></p>
                                </td>
                            </tr>
                            <tr style='mso-yfti-irow:13;height:22.7pt'>
                                <td width=77 colspan=3 valign=bottom style='width:57.9pt;padding:0cm 5.4pt 0cm 5.4pt;
    height:22.7pt'>
                                    <p class=MsoNormal align=right style='margin-bottom:0cm;margin-bottom:.0001pt;
    text-align:right;line-height:normal'><span style='font-size:8.0pt;
    font-family:"Times New Roman","serif"'>Дата <span class=SpellE>видачі</span></span></p>
                                </td>
                                <td width=97 nowrap colspan=2 valign=bottom style='width:72.45pt;
    padding:0cm 2.85pt 0cm 2.85pt;height:22.7pt'>
                                    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;
    line-height:normal'><b><span lang=EN-US style='font-size:12.0pt;font-family:
    "Times New Roman","serif";mso-ansi-language:EN-US'><?= $certificate->getDateStart() ?></span></b><span
                                                lang=EN-US style='font-size:12.0pt;mso-ansi-language:EN-US'><o:p></o:p></span>
                                    </p>
                                </td>
                                <td width=115 rowspan=2 valign=bottom style='width:86.25pt;padding:0cm 0cm 0cm 0cm;
    height:22.7pt'>
                           </td>
                            </tr>
                            <tr style='mso-yfti-irow:14;height:22.7pt;mso-height-rule:exactly'>
                                <td width=77 colspan=3 valign=bottom style='width:57.9pt;padding:0cm 5.4pt 0cm 5.4pt;
    height:22.7pt;mso-height-rule:exactly'>
                                    <p class=MsoNormal align=right style='margin-bottom:0cm;margin-bottom:.0001pt;
    text-align:right;line-height:normal'><span class=SpellE><span
                                                    style='font-size:8.0pt;font-family:"Times New Roman","serif"'>Дійсно</span></span><span
                                                style='font-size:8.0pt;font-family:"Times New Roman","serif"'> до</span>
                                    </p>
                                </td>
                                <td width=97 colspan=2 valign=bottom style='width:72.45pt;padding:0cm 2.85pt 0cm 2.85pt;
    height:22.7pt;mso-height-rule:exactly'>
                                    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;
    line-height:normal'><b><span lang=EN-US style='font-size:12.0pt;font-family:
    "Times New Roman","serif";mso-ansi-language:EN-US'><?= $certificate->getDateEnd() ?></span></b></p>
                                </td>
                            </tr>
                            <tr style='mso-yfti-irow:15;height:34.0pt'>
                                <td width=381 colspan=8 style='width:285.95pt;padding:0cm 0cm 0cm 0cm;
    height:34.0pt'>
                                    
                                </td>
                            </tr>
                            <tr style='mso-yfti-irow:16;mso-yfti-lastrow:yes'>
                                <td width=76 style='width:57.2pt;padding:0cm 0cm 0cm 0cm'></td>
                                <td width=38 colspan=2 style='width:28.6pt;padding:0cm 0cm 0cm 0cm'></td>
                                <td width=38 style='width:28.6pt;padding:0cm 0cm 0cm 0cm'></td>
                                <td width=95 colspan=2 style='width:71.4pt;padding:0cm 0cm 0cm 0cm'></td>
                                <td width=19 style='width:13.9pt;padding:0cm 0cm 0cm 0cm'></td>
                                <td width=115 style='width:86.25pt;padding:0cm 0cm 0cm 0cm'></td>
                            </tr>
                            <![if !supportMisalignedColumns]>
                            <tr height=0>
                                <td width=76 style='border:none'></td>
                                <td width=16 style='border:none'></td>
                                <td width=22 style='border:none'></td>
                                <td width=39 style='border:none'></td>
                                <td width=17 style='border:none'></td>
                                <td width=79 style='border:none'></td>
                                <td width=19 style='border:none'></td>
                                <td width=113 style='border:none'></td>
                            </tr>
                            <![endif]>
                        </table>
                    </div>
                </td>
                <?php if (!$ukrOnly): ?>
                <td width="50%" valign=top style='width:50.0%;padding:2.85pt 5.4pt 0cm 5.4pt;height:419.6pt;mso-height-rule:exactly'>
                    <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 style='border-collapse:collapse;mso-yfti-tbllook:1184;mso-padding-alt:0cm 0cm 0cm 0cm'>
                        <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;height:36.85pt;mso-height-rule:
    exactly'>
                            <td width=114 rowspan=2 style='width:85.8pt;padding:0cm 5.4pt 0cm 5.4pt;
    height:36.85pt;mso-height-rule:exactly'></td>
                            <td width=281 colspan=7 valign=top style='width:211.05pt;padding:0cm 5.4pt 0cm 5.4pt;
    height:36.85pt;mso-height-rule:exactly'></td>
                        </tr>
                        <tr style='mso-yfti-irow:1;height:36.85pt;mso-height-rule:exactly'>
                            <td width=159 colspan=3 style='width:119.6pt;padding:0cm 5.4pt 0cm 5.4pt;
    height:36.85pt;mso-height-rule:exactly'></td>
                            <td width=122 colspan=4 style='width:91.45pt;padding:0cm 5.4pt 0cm 5.4pt;
    height:36.85pt;mso-height-rule:exactly'></td>
                        </tr>
                        <tr style='mso-yfti-irow:2;height:19.85pt'>
                            <td width=396 colspan=8 style='width:296.85pt;padding:0cm 5.4pt 0cm 5.4pt;
    height:19.85pt'>
        <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom: .0001pt;text-align:center;line-height:normal'><b><span style='font-size:12.0pt;font-family:"Times New Roman","serif";'>CERTIFICATE OF PROFICIENCY</span>&nbsp;<span
                                                style='font-size:12.0pt;font-family:"Times New Roman","serif";'>№ <?= $certificate->number ?></span></b>
                                </p>
                            </td>
                        </tr>
                        <tr style='mso-yfti-irow:3;height:22.7pt'>
                            <td width=396 colspan=8 valign=top style='width:296.85pt;padding:0cm 5.4pt 0cm 5.4pt;
    height:22.7pt'>
                             <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;
    line-height:normal'></span>
                                </p>
                            </td>
                        </tr>
                        <tr style='mso-yfti-irow:4;height:11.35pt'>
                            <td width=396 colspan=8 style='width:296.85pt;padding:0cm 5.4pt 0cm 5.4pt;
    height:11.35pt'>
                                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;
    line-height:normal'><span lang=EN-US style='font-size:8.0pt;font-family:
    "Times New Roman","serif";mso-ansi-language:EN-US'>This is to certify&nbsp;
    that:</span><span lang=EN-US style='mso-ansi-language:EN-US'><o:p></o:p></span></p>
                            </td>
                        </tr>
                        <tr style='mso-yfti-irow:5;height:19.85pt'>
                            <td width=396 colspan=8 style='width:296.85pt;padding:0cm 5.4pt 0cm 5.4pt;
    height:19.85pt'>
                                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:
    .0001pt;text-align:center;line-height:normal'><b><i><span lang=EN-US
                                                              style='font-size:14.0pt;font-family:"Times New Roman","serif";mso-ansi-language:
    EN-US'><?= $certificate->client->name_transliteration ?></span></i></b></p>
                            </td>
                        </tr>
                        <tr style='mso-yfti-irow:6;height:19.85pt'>
                            <td width=396 colspan=8 style='width:296.85pt;padding:0cm 5.4pt 0cm 5.4pt;
    height:19.85pt'>
                                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:
    .0001pt;text-align:center;line-height:normal'><b><i><span lang=EN-US
                                                              style='font-size:14.0pt;font-family:"Times New Roman","serif";mso-ansi-language:
    EN-US'><?= $certificate->client->surname_transliteration ?></i></b><span
                                            lang=EN-US style='mso-ansi-language:EN-US'><o:p></o:p></span></p>
                            </td>
                        </tr>
                        <tr style='mso-yfti-irow:7;height:17.0pt;mso-height-rule:exactly'>
                            <td width=114 style='width:85.8pt;padding:0cm 5.4pt 0cm 5.4pt;height:17.0pt;
    mso-height-rule:exactly'>
                                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;
    line-height:normal'><b><span lang=EN-US style='font-size:8.0pt;font-family:
    "Times New Roman","serif";mso-ansi-language:EN-US'>Date of&nbsp; birth:</span></b></p>
                            </td>
                            <td width=175 colspan=5 style='width:131.05pt;padding:0cm 5.4pt 0cm 5.4pt;
    height:17.0pt;mso-height-rule:exactly'>
                                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:
    .0001pt;text-align:center;line-height:normal'><b><span lang=EN-US
                                                           style='font-size:12.0pt;font-family:"Times New Roman","serif";mso-ansi-language:
    EN-US'><?= Yii::$app->formatter->asDate($certificate->client->birthday) ?></span></b><span lang=EN-US
                                                                                               style='font-size:12.0pt;mso-ansi-language:EN-US'><o:p></o:p></span>
                                </p>
                            </td>
                            <td width=107 colspan=2 valign=top style='width:80.0pt;padding:0cm 5.4pt 0cm 5.4pt;
    height:17.0pt;mso-height-rule:exactly'>
                                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;
    line-height:normal'><span lang=EN-US style='font-size:8.0pt;font-family:
    "Times New Roman","serif";mso-ansi-language:EN-US'>&nbsp;</span><span
                                            lang=EN-US style='mso-ansi-language:EN-US'><o:p></o:p></span></p>
                            </td>
                        </tr>
                        <tr style='mso-yfti-irow:8;height:19.85pt'>
                            <td width=396 colspan=8 style='width:296.85pt;padding:0cm 5.4pt 0cm 5.4pt;
    height:19.85pt'>
                                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;
    line-height:normal'><span lang=EN-US style='font-size:7.0pt;font-family:
    "Times New Roman","serif";mso-ansi-language:EN-US'>has successfully
    completed the course of training and assessment of competence according to the current requirements of employers and appropriately qualified to be emploed as</span><span lang=EN-US style='font-size:7.0pt;
    mso-ansi-language:EN-US'><o:p></o:p></span></p>
                            </td>
                        </tr>
                        <tr style='mso-yfti-irow:9;height:51.05pt'>
                            <td width=396 colspan=8 style='width:296.85pt;padding:0cm 5.4pt 0cm 5.4pt;
    height:51.05pt'>
                              <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:
    .0001pt;text-align:center;line-height:normal'><b><i><span lang=EN-US style='font-family:"Times New Roman","serif";mso-ansi-language:EN-US'>STEWARD ASSISTANT</span></i></b></p>
                            </td>
                        </tr>
                        <tr style='mso-yfti-irow:10;height:17.0pt'>
                            <td width=396 colspan=8 valign=top style='width:296.85pt;padding:0cm 5.4pt 0cm 5.4pt;
    height:17.0pt'>
                             <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;
    line-height:normal'><span lang=EN-US style='font-size:7.0pt;font-family:
    "Times New Roman","serif";mso-ansi-language:EN-US'>and are entitled to serve in a ship in any capacity requiring a sertificate of this kind.</span></p>
                            </td>
                        </tr>
                        <tr style='mso-yfti-irow:11;height:22.7pt'>
                            <td width=114 rowspan=5 style='width:85.8pt;padding:0cm 5.4pt 0cm 5.4pt;
    height:22.7pt'>&nbsp;</td>
                            <td width=164 colspan=4 valign=bottom style='width:123.25pt;padding:0cm 5.4pt 0cm 5.4pt;
    height:22.7pt'>
                                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;
    line-height:normal'><span lang=EN-US style='font-size:8.0pt;font-family:
    "Times New Roman","serif";mso-ansi-language:EN-US'>Manager</span><span
                                           style='font-size:8.0pt;font-family:"Times New Roman","serif"'>&nbsp;&nbsp; </span><span
                                            lang=EN-US style='font-size:8.0pt;font-family:"Times New Roman","serif";
    mso-ansi-language:EN-US'>.…………….</span></p>
                            </td>
                            <td width=117 colspan=3 rowspan=2 style='width:87.8pt;padding:0cm 0cm 0cm 0cm;
    height:22.7pt'>
                                <p class=MsoNormal align=right style='margin-bottom:0cm;margin-bottom:.0001pt;
    text-align:right;line-height:normal'><!--[if gte vml 1]>
                                    <v:shape id="Рисунок_x0020_9"
                                             o:spid="_x0000_s1026" type="#_x0000_t75" style='width:39.75pt;height:39.75pt;
     visibility:visible;mso-wrap-style:square;mso-left-percent:-10001;
     mso-top-percent:-10001;mso-position-horizontal:absolute;
     mso-position-horizontal-relative:char;mso-position-vertical:absolute;
     mso-position-vertical-relative:line;mso-left-percent:-10001;
     mso-top-percent:-10001'>
                                        <w:wrap type="none"/>
                                        <w:anchorlock/>
                                    </v:shape><![endif]--><![if !vml]><img width=53 height=53
                                                                           src="/images/certificate/image001.jpg"
                                                                           v:shapes="Рисунок_x0020_9"><![endif]></p>
                            </td>
                        </tr>
                        <tr style='mso-yfti-irow:12;height:22.7pt'>
                            <td width=164 colspan=4 valign=bottom style='width:123.25pt;padding:0cm 5.4pt 0cm 5.4pt;
    height:22.7pt'>
                                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;
    line-height:normal'><span lang=EN-US style='font-size:8.0pt;font-family:
    "Times New Roman","serif";mso-ansi-language:EN-US'>Registrar</span><span
                                            style='font-size:8.0pt;font-family:"Times New Roman","serif"'>&nbsp;&nbsp; </span><span
                                            lang=EN-US style='font-size:8.0pt;font-family:"Times New Roman","serif";
    mso-ansi-language:EN-US'>.…………….</span></p>
                            </td>
                        </tr>
                        <tr style='mso-yfti-irow:13;height:22.7pt'>
                            <td width=78 valign=bottom style='width:58.45pt;padding:0cm 5.4pt 0cm 5.4pt;
    height:22.7pt'>
                                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;
    line-height:normal'><span lang=EN-US style='font-size:8.0pt;font-family:
    "Times New Roman","serif";mso-ansi-language:EN-US'>Date of issue</span></p>
                            </td>
                            <td width=86 colspan=3 style='width:64.8pt;padding:0cm 5.4pt 0cm 5.4pt;
    height:22.7pt'>
                                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:
    .0001pt;text-align:center;line-height:normal'><b><span lang=EN-US
                                                           style='font-size:12.0pt;font-family:"Times New Roman","serif";mso-ansi-language:
    EN-US'><?= $certificate->getDateStart() ?></span></b><span lang=EN-US
                                                               style='font-size:12.0pt;mso-ansi-language:EN-US'><o:p></o:p></span>
                                </p>
                            </td>
                            <td width=117 colspan=3 rowspan=2 valign=top style='width:87.8pt;
    padding:0cm 5.4pt 0cm 5.4pt;height:22.7pt'>
                              
                            </td>
                        </tr>
                        <tr style='mso-yfti-irow:14;height:22.7pt;mso-height-rule:exactly'>
                            <td width=78 valign=bottom style='width:58.45pt;padding:0cm 5.4pt 0cm 5.4pt;
    height:22.7pt;mso-height-rule:exactly'>
                                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;
    line-height:normal'><span lang=EN-US style='font-size:8.0pt;font-family:
    "Times New Roman","serif";mso-ansi-language:EN-US'>Date of expire</span></p>
                            </td>
                            <td width=86 colspan=3 valign=bottom style='width:64.8pt;padding:0cm 5.4pt 0cm 5.4pt;
    height:22.7pt;mso-height-rule:exactly'>
                                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:
    .0001pt;text-align:center;line-height:normal'><b><span lang=EN-US
                                                           style='font-size:12.0pt;font-family:"Times New Roman","serif";mso-ansi-language:
    EN-US'><?= $certificate->getDateEnd() ?></span></b><span lang=EN-US
                                                             style='font-size:12.0pt;mso-ansi-language:EN-US'><o:p></o:p></span>
                                </p>
                            </td>
                        </tr>
                        <tr style='mso-yfti-irow:15;height:36.85pt'>
                            <td width=281 colspan=7 style='width:211.05pt;padding:0cm 0cm 0cm 0cm;
    height:36.85pt'>
                             
                                
                            </td>
                        </tr>
                        <tr style='mso-yfti-irow:16;mso-yfti-lastrow:yes'>
                            <td width=114 style='width:85.8pt;padding:0cm 0cm 0cm 0cm'></td>
                            <td width=134 colspan=2 style='width:100.2pt;padding:0cm 0cm 0cm 0cm'></td>
                            <td width=26 style='width:19.4pt;padding:0cm 0cm 0cm 0cm'></td>
                            <td width=5 style='width:3.65pt;padding:0cm 0cm 0cm 0cm'></td>
                            <td width=54 colspan=2 style='width:40.4pt;padding:0cm 0cm 0cm 0cm'></td>
                            <td width=63 style='width:47.4pt;padding:0cm 0cm 0cm 0cm'></td>
                        </tr>
                        <![if !supportMisalignedColumns]>
                        <tr height=0>
                            <td width=114 style='border:none'></td>
                            <td width=78 style='border:none'></td>
                            <td width=67 style='border:none'></td>
                            <td width=31 style='border:none'></td>
                            <td width=6 style='border:none'></td>
                            <td width=14 style='border:none'></td>
                            <td width=44 style='border:none'></td>
                            <td width=59 style='border:none'></td>
                        </tr>
                        <![endif]>
                    </table>
                </td>
                <?php endif; ?>
            </tr>
        </table>
    </div>
</div>
</body>
</html>