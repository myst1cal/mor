<?php

namespace app\models\ars;

use yii\db\Expression;

/**
 * This is the model class for table "client_certificate".
 *
 * @property int $id
 * @property int $course_id
 * @property int $group_id
 * @property int $client_id
 * @property string $date_add
 * @property string $number
 * @property string $cost_price
 * @property string $cost_price_2
 * @property int $price_type
 * @property string $additional_sum
 * @property double $overpayment [decimal(12,2)]
 * @property string $total_sum
 * @property int $paid
 * @property int $issued
 * @property int $middlemen_id
 * @property bool $additional_sum_percent [tinyint(3)]
 * @property bool $printed [tinyint(3)]
 * @property int $pay_type [tinyint(3)]
 *
 * @property CourseAdditionalFieldCourse[] $courseAdditionalFieldCourses
 * @property CourseAdditionalField[] $courseAdditionalFields
 * @property CourseAdditionalFieldClientCertificate[] $courseAdditionalFieldClientCertificates
 * @property Client $client
 * @property Group $group
 * @property Course $course
 * @property mixed $dateEnd
 * @property mixed $dateStart
 * @property Middlemen $middlemen
 * @property string $date_of_issue [date]
 */
class ClientCertificate extends \yii\db\ActiveRecord
{
    const STATUS_PAID = 1;

    const PAY_TYPE_CASH = 1;
    const PAY_TYPE_BANK_TRANSFER = 2;

    const PRICE_TYPE_FULL_COURSE = 1;
    const PRICE_TYPE_SHORT_COURSE = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'client_certificate';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['course_id', 'client_id'], 'required'],
            [['group_id', 'course_id', 'client_id', 'additional_sum_percent', 'paid', 'printed', 'issued', 'middlemen_id', 'pay_type'], 'integer'],
            [['cost_price', 'cost_price_2', 'additional_sum', 'overpayment', 'total_sum'], 'number'],
            [['number'], 'default', 'value' => null],
            [['number'], 'string', 'max' => 255],
            [['client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Client::className(), 'targetAttribute' => ['client_id' => 'id']],
            [['group_id'], 'exist', 'skipOnError' => true, 'targetClass' => Group::className(), 'targetAttribute' => ['group_id' => 'id']],
            [['date_of_issue'], 'date', 'format' => 'php:Y-m-d'],
            [['date_add'], 'date', 'format' => 'php:Y-m-d'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'course_id' => 'Курс',
            'group_id' => 'Группа',
            'client_id' => 'Клиент',
            'date_add' => 'Дата создания',
            'number' => 'Номер',
            'cost_price' => 'Цена P1',
            'cost_price_2' => 'Цена P2',
            'additional_sum' => 'Добавочная сумма, грн',
            'additional_sum_percent' => 'Скидка, %',
            'overpayment' => 'Переплата, грн',
            'total_sum' => 'Всего, грн.',
            'paid' => 'Оплачен',
            'printed' => 'Распечатан',
            'issued' => 'Выдан',
            'date_of_issue' => 'Дата получения',
            'middlemen_id' => 'Посредник',
            'pay_type' => 'Тип оплаты',
        ];
    }

    public static function payTypeList()
    {
        return [
            self::PAY_TYPE_CASH => 'Наличный',
            self::PAY_TYPE_BANK_TRANSFER => 'Безналичный',
        ];
    }

    public function beforeValidate()
    {
        if (parent::beforeValidate()) {
            if (is_numeric($this->cost_price) || is_numeric($this->cost_price_2)) {
                $this->total_sum = $this->priceCost() + ($this->additional_sum ?: 0) + $this->overpayment;
            }

            return true;
        }

        return false;
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert && empty($this->date_add)) {
                $this->date_add = new Expression('DATE(NOW())');
            }

            return true;
        }

        return false;
    }

    public function status()
    {
        if ($this->issued) {
            return 'Выдан';
        }

        if ($this->printed) {
            return 'Распечатан';
        }

        if ($this->paid) {
            return 'Оплачен';
        }

        return 'Нет оплаты';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(Group::className(), ['id' => 'group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourse()
    {
        return $this->hasOne(Course::className(), ['id' => 'course_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMiddlemen()
    {
        return $this->hasOne(Middlemen::className(), ['id' => 'middlemen_id']);
    }

    public function getDateStart()
    {
        return \Yii::$app->formatter->asDate($this->date_of_issue);
    }

    public function getDateEnd()
    {
        $date = strtotime($this->date_of_issue . ' +5 years');
        return \Yii::$app->formatter->asDate($date);
    }

    public function getCourseAdditionalFields()
    {
        return $this->hasMany(CourseAdditionalField::className(), ['id' => 'course_additional_field_id'])->viaTable('course_additional_field_client_certificate', ['client_certificate_id' => 'id']);
    }

    public function getCourseAdditionalFieldCourses()
    {
        return $this->hasMany(CourseAdditionalFieldCourse::className(), ['course_id' => 'id']);
    }

    public function getCourseAdditionalFieldClientCertificates()
    {
        return $this->hasMany(CourseAdditionalFieldClientCertificate::className(), ['client_certificate_id' => 'id']);
    }

    public function priceCost()
    {
        return $this->price_type === ClientCertificate::PRICE_TYPE_FULL_COURSE ? $this->cost_price : $this->cost_price_2;
    }

    public function priceTypeText()
    {
        return self::priceTypeList()[$this->price_type];
    }

    public static function priceTypeList()
    {
        return [self::PRICE_TYPE_FULL_COURSE => 'Полный, P1', self::PRICE_TYPE_SHORT_COURSE => 'Укороченный, P2'];
    }
}
