<?php


namespace app\controllers;


use app\helpers\AuthHelper;
use app\models\ars\Client;
use app\models\ars\ClientCertificate;
use app\models\ars\Course;
use app\models\ars\Group;
use app\services\ReportService;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;

class ReportController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['by-dates', 'by-day', 'by-middlemen', 'statistic', 'statistic-by-course'],
                        'allow' => true,
                        'roles' => AuthHelper::CAN_SEE_MENU_REPORT,
                    ],
                    [
                        'actions' => ['print-certificates'],
                        'allow' => true,
                        'roles' => AuthHelper::CAN_PRINT_CERTIFICATE,
                    ],
                ],
            ],
        ];
    }

    public function actionByDates()
    {
        $startDate = Yii::$app->request->post('start_date');
        $endDate = Yii::$app->request->post('end_date');
        $toXls = Yii::$app->request->post('to_xls');

        $data = [];
        $statistics = [];

        if (!empty($startDate)) {
            $data = ClientCertificate::find()
                ->leftJoin(Group::tableName(), 'group_id=group.id')
                ->where(['>=', 'group.end_date', $startDate])
                ->andWhere(['<=', 'group.end_date', $endDate])
                ->all();

            $statistics['totalSum'] = ClientCertificate::find()
                ->leftJoin(Group::tableName(), 'group_id=group.id')
                ->where(['>=', 'group.end_date', $startDate])
                ->andWhere(['<=', 'group.end_date', $endDate])
                ->sum('total_sum');
            $statistics['clientCertificates'] = ClientCertificate::find()
                ->leftJoin(Group::tableName(), 'group_id=group.id')
                ->where(['>=', 'group.end_date', $startDate])
                ->andWhere(['<=', 'group.end_date', $endDate])
                ->count('client_certificate.id');
            $statistics['clients'] = ClientCertificate::find()
                ->leftJoin(Group::tableName(), 'group_id=group.id')
                ->where(['>=', 'group.end_date', $startDate])
                ->andWhere(['<=', 'group.end_date', $endDate])
                ->select('client_id')
                ->distinct()
                ->count('client_id');
            $statistics['certificateProcessing'] = ClientCertificate::find()
                ->leftJoin(Group::tableName(), 'group_id=group.id')
                ->where(['>=', 'group.end_date', $startDate])
                ->andWhere(['<=', 'group.end_date', $endDate])
                ->andWhere(['number' => null])
                ->count('*');
            $statistics['certificateDone'] = ClientCertificate::find()
                ->leftJoin(Group::tableName(), 'group_id=group.id')
                ->where(['>=', 'group.end_date', $startDate])
                ->andWhere(['<=', 'group.end_date', $endDate])
                ->andWhere(['is not', 'number', null])
                ->count('*');

            if ($toXls) {
                $report = ReportService::byDatesToXls($data);

                $filename = 'report-dates-' . $startDate . '-' . $endDate . '.xls';
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header('Content-Disposition: attachment;filename="' . $filename . '"');
                header('Cache-Control: max-age=0');

                $writer = new Xls($report);
                $writer->save('php://output');
                exit;
            }
        }

        return $this->render('by_dates', [
            'data' => $data,
            'startDate' => $startDate,
            'endDate' => $endDate,
            'statistics' => $statistics
        ]);
    }

    public function actionByDay()
    {
        $day = Yii::$app->request->post('day');
        $toXls = Yii::$app->request->post('to_xls');

        $data = [];
        $sum = 0;
        if (!empty($day)) {
            $data = ClientCertificate::find()
                ->leftJoin(Client::tableName(), 'client_id=client.id')
                ->where(['date_add' => $day])
                ->andWhere(['paid' => 1])
                ->groupBy('client_id')
                ->select('SUM(total_sum) as total_sum, client.name_ukr, client.surname_ukr, client.patronymic_ukr')
                ->asArray()
                ->all();

            $sum = array_sum(ArrayHelper::getColumn($data, 'total_sum'));

            if ($toXls) {
                $report = ReportService::byDayToXls($data);

                $filename = 'report-day-' . $day . '.xls';
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header('Content-Disposition: attachment;filename="' . $filename . '"');
                header('Cache-Control: max-age=0');

                $writer = new Xls($report);
                $writer->save('php://output');
                exit;
            }
        }

        return $this->render('by_day', [
            'day' => $day,
            'data' => $data,
            'sum' => $sum,
        ]);
    }

    public function actionStatisticByCourse()
    {
        $startDate = Yii::$app->request->get('start_date');
        $endDate = Yii::$app->request->get('end_date');
        $courseId = Yii::$app->request->get('course_id');

        $query = (new Query())
            ->select("CONCAT_WS(' ', `surname_ukr`, `name_ukr`, `patronymic_ukr`) as name, date_add, date_of_issue, number")
            ->from(ClientCertificate::tableName())
            ->leftJoin(Client::tableName(), 'client.id = client_certificate.client_id');

        $query->where(['paid' => ClientCertificate::STATUS_PAID]);

        if ($startDate) {
            $query->andWhere(['>=', 'date_add', $startDate]);
        }

        if ($endDate) {
            $query->andWhere(['<=', 'date_add', $endDate]);
        }

        if ($courseId) {
            $query->andWhere(['course_id' => $courseId]);
        }

        $certificateData = $query->all();

        return $this->render('statistic_by_course', [
            'certificateData' => $certificateData,
            'startDate' => $startDate,
            'endDate' => $endDate,
        ]);
    }

    public function actionStatistic()
    {
        $startDate = Yii::$app->request->post('start_date');
        $endDate = Yii::$app->request->post('end_date');

        $query = (new Query())
            ->select('
                SUM(client_certificate.total_sum) as total_sum, 
                SUM(case when client_certificate.pay_type = ' . ClientCertificate::PAY_TYPE_CASH . ' then client_certificate.total_sum else 0 end) as total_sum_cash, 
                SUM(case when client_certificate.pay_type = ' . ClientCertificate::PAY_TYPE_BANK_TRANSFER . ' then client_certificate.total_sum else 0 end) as total_sum_bank_transfer,
                COUNT(client_certificate.id) as total_certificates, 
                course.title_ukr as title, course.id as course_id
            ')
            ->from('client_certificate')
            ->leftJoin(Course::tableName(), 'course.id=client_certificate.course_id')
            ->groupBy('course_id');

        $query->where(['paid' => ClientCertificate::STATUS_PAID]);

        $queryConventional = (new Query())
            ->select('
                conventional,
                COUNT(client_certificate.id) AS counter,
                SUM(client_certificate.cost_price) AS total_sum
            ')
            ->from('client_certificate')
            ->leftJoin(Course::tableName(), 'course.id=client_certificate.course_id')
            ->groupBy('conventional');;

        if ($startDate) {
            $query->andWhere(['>=', 'date_add', $startDate]);
            $queryConventional->andWhere(['>=', 'date_add', $startDate]);
        }

        if ($endDate) {
            $query->andWhere(['<=', 'date_add', $endDate]);
            $queryConventional->andWhere(['<=', 'date_add', $endDate]);
        }

        $certificateData = $query->all();
        $conventionalData = $queryConventional->all();

        return $this->render('statistic', [
            'certificateData' => $certificateData,
            'conventionalData' => $conventionalData,
            'startDate' => $startDate,
            'endDate' => $endDate,
        ]);
    }

    public function actionByMiddlemen()
    {
        $query = (new Query())
            ->select('name, sum(total_sum) as total_sum, count(client_certificate.id) as count, middlemen.id as id')
            ->from('middlemen')
            ->leftJoin(ClientCertificate::tableName(), 'middlemen.id=client_certificate.middlemen_id')
            ->groupBy('name, middlemen.id');

        $data = $query->all();

        return $this->render('by_middlemen', [
            'data' => $data,
        ]);
    }

    public function actionPrintCertificates()
    {
        $day = Yii::$app->request->get('day');

        $query = ClientCertificate::find()
            ->with('client', 'course')
            ->orderBy('id DESC');

        if (!empty($day)) {
            $query = $query->where(['date_of_issue' => $day]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $this->render('print_certificates', [
            'day' => $day,
            'dataProvider' => $dataProvider,
        ]);
    }
}