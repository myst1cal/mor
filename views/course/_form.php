<?php

use app\models\ars\Course;
use app\models\ars\Teacher;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ars\Course */
/* @var $form yii\widgets\ActiveForm */
/* @var $teachers Teacher[] */
/* @var $additionalFields \app\models\ars\CourseAdditionalField[] */
/* @var $currentTeachers integer[] */
?>

<div class="course-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'title_ukr')->textarea(['rows' => 6]) ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'title_eng')->textarea(['rows' => 6]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3">
            <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($model, 'template')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($model, 'has_ukr_only')->checkbox() ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($model, 'conventional')->dropDownList(Course::getConventionalList(), ['prompt' => '...']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6">
            <div class="form-group">
                <h3>Преподаватели</h3>
                <?php foreach ($teachers as $teacher): ?>
                    <br><?= Html::checkbox('teacher[' . $teacher->id . ']', in_array($teacher->id, $currentTeachers), ['value' => $teacher->id]) . ' ' . $teacher->full_name ?>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                <h3>Доп. поля</h3>
                <?php foreach ($additionalFields as $additionalField): ?>
                    <br>
                    <?= Html::checkbox('additionalField[' . $additionalField->id . ']', in_array($additionalField->id, $currentAdditionalFields), ['value' => $additionalField->id]) . ' ' . $additionalField->name ?>
                    <?= Html::textInput('additionalFieldSort[' . $additionalField->id . ']', $currentAdditionalFieldSort[$additionalField->id] ?? 0) ?>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
