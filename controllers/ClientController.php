<?php

namespace app\controllers;

use app\helpers\AuthHelper;
use app\models\ars\Client;
use app\models\ars\ClientCertificate;
use app\models\ars\Middlemen;
use app\models\forms\UploadForm;
use app\models\searches\ClientSearch;
use app\services\LogService;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * ClientController implements the CRUD actions for Client model.
 */
class ClientController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view'],
                        'allow'   => true,
                        'roles'   => AuthHelper::CAN_SEE_MENU_CLIENTS,
                    ],
                    [
                        'actions' => ['create', 'update'],
                        'allow'   => true,
                        'roles'   => AuthHelper::CAN_EDIT_CLIENT,
                    ],
                    [
                        'actions' => ['set-comment'],
                        'allow'   => true,
                        'roles'   => AuthHelper::CAN_EDIT_CLIENT_COMMENT,
                    ],
                ],
            ],
        ];
    }
    /**
     * Lists all Client models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ClientSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Client model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $query = ClientCertificate::find()->where(['client_id' => $id])->orderBy(['id' => SORT_DESC]);

        return $this->render('view', [
            'model'                   => $this->findModel($id),
            'certificates'            => $query->all(),
            'middlemens'              => ArrayHelper::map(Middlemen::find()->all(), 'id', 'name'),
        ]);
    }

    /**
     * Finds the Client model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Client the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Client::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Creates a new Client model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Client();

        $file = new UploadForm();
        $file->photo = UploadedFile::getInstance($file, 'photo');

        $fileExtension = $file->getExtension();
        if ($fileExtension) {
            $model->photo_dir = 'images/client-photos';
            $model->photo_name = time() . '.' . $file->getExtension();

            $file->upload($model->photo_name, $model->photo_dir);
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if (empty($model->errors) && empty($file->errors)) {
                LogService::add($model, Yii::$app->user->getIdentity());
                $model->save();
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'file' => $file
        ]);
    }

    /**
     * Updates an existing Client model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $file = new UploadForm();
        $file->photo = UploadedFile::getInstance($file, 'photo');

        $fileExtension = $file->getExtension();
        if ($fileExtension) {
            $model->photo_dir = 'images/client-photos';
            $model->photo_name = time() . '.' . $file->getExtension();

            $file->upload($model->photo_name, $model->photo_dir);
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if (empty($model->errors) && empty($file->errors)) {
                LogService::add($model, Yii::$app->user->getIdentity());
                $model->save();

                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'file' => $file
        ]);
    }

    public function actionSetComment()
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $id = Yii::$app->request->get('id');
            $value = Yii::$app->request->get('value');

            $certificate = Client::findOne($id);

            $certificate->comment = $value;

            if ($certificate->save()) {
                return ['status' => 'OK'];
            }

            return ['status' => 'Error', 'messages' => $certificate->getErrors()];
        }
    }
}
