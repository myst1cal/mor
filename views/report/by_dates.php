<?php

use app\models\ars\ClientCertificate;
use kartik\date\DatePicker;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\searches\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $certificate ClientCertificate */

$this->title = 'Отчет: по датам';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="report-by_dates">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= Html::beginForm('', 'post', ['id' => 'report-by-dates']) ?>
    <div class="form-group">
        <div class="row">
            <div class="col-lg-3">
                <?= DatePicker::widget(
                    [
                        'name'          => 'start_date',
                        'options'       => ['placeholder' => 'ГГГГ-ММ-ДД', 'autocomplete' => 'off'],
                        'type'          => DatePicker::TYPE_COMPONENT_PREPEND,
                        'value'         => $startDate,
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format'    => 'yyyy-mm-dd',
                        ],
                    ]) ?>
            </div>
            <div class="col-lg-3">
                <?= DatePicker::widget(
                    [
                        'name'          => 'end_date',
                        'options'       => ['placeholder' => 'ГГГГ-ММ-ДД', 'autocomplete' => 'off'],
                        'type'          => DatePicker::TYPE_COMPONENT_PREPEND,
                        'value'         => $endDate,
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format'    => 'yyyy-mm-dd',
                        ],
                    ]) ?>
            </div>
            <?= Html::hiddenInput('to_xls', 0, ['class' => 'toXls'])?>
            <div class="col-lg-1">
                <?= Html::button('Поиск', ['class' => 'report-by-dates btn btn-success']) ?>
            </div>
            <div class="col-lg-1">
                <?= Html::button('В excel', ['class' => 'report-by-dates-to-xls btn btn-info']) ?>
            </div>
        </div>
    </div>
    <?= Html::endForm() ?>

    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <th>ФИО</th>
            <th>Дата рождения</th>
            <th>Курс</th>
            <th>Прайс</th>
            <th>Доп. оплата</th>
            <th>Сумма</th>
            <th>Оплечен</th>
            <th>Дата получения</th>
            <th>Посредник</th>
        </tr>
        </thead>

        <tbody>
        <?php foreach ($data as $certificate): ?>
            <tr>
                <td><?= $certificate->client->surname_ukr . ' ' . $certificate->client->name_ukr . ' ' . $certificate->client->patronymic_ukr ?></td>
                <td><?= $certificate->client->birthday ?></td>
                <td><?= $certificate->course->title_ukr ?></td>
                <td><?= $certificate->cost_price ?></td>
                <td><?= $certificate->additional_sum ?></td>
                <td><?= $certificate->total_sum ?></td>
                <td><?= $certificate->paid ? 'Да' : 'Нет' ?></td>
                <td><?= $certificate->group->end_date ?></td>
                <td><?= $certificate->middlemen_id ? $certificate->middlemen->name : 'Нет' ?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>

    <?php if (!empty($statistics)): ?>
        <table class="table table-striped table-bordered">
            <tbody>
            <tr>
                <td>Людей</td>
                <td><?= $statistics['clients'] ?></td>
            </tr>
            <tr>
                <td>Сертификатов</td>
                <td><?= $statistics['clientCertificates'] ?></td>
            </tr>
            <tr>
                <td>Сумма</td>
                <td><?= $statistics['totalSum'] ?></td>
            </tr>
            <tr>
                <td>Без номера</td>
                <td><?= $statistics['certificateProcessing'] ?></td>
            </tr>
            <tr>
                <td>С номером</td>
                <td><?= $statistics['certificateDone'] ?></td>
            </tr>
            </tbody>
        </table>
    <?php endif; ?>

    <?php $this->registerJsFile('js/report-by-dates.js', ['depends' => ['yii\web\JqueryAsset']]); ?>
</div>
