<?php

namespace app\models\ars;

/**
 * This is the model class for table "group".
 *
 * @property int $id
 * @property int $course_id
 * @property int $teacher_id
 * @property string $name
 * @property int $max_students
 * @property string $start_date
 * @property string $end_date
 *
 * @property ClientCertificate[] $clientCertificates
 * @property Course $course
 * @property Teacher $teacher
 */
class Group extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'group';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['course_id', 'teacher_id', 'name', 'max_students', 'start_date', 'end_date'], 'required'],
            [['course_id', 'teacher_id', 'max_students'], 'integer'],
            [['start_date', 'end_date'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [
                ['course_id'], 'exist', 'skipOnError' => true, 'targetClass' => Course::className(),
                'targetAttribute' => ['course_id' => 'id']
            ],
            [
                ['teacher_id'], 'exist', 'skipOnError' => true, 'targetClass' => Teacher::className(),
                'targetAttribute' => ['teacher_id' => 'id']
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'course_id' => 'Курс',
            'teacher_id' => 'Преподаватель',
            'name' => 'Название',
            'max_students' => 'Максимум студентов',
            'start_date' => 'Начало обучения',
            'end_date' => 'Окончание обучения',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientCertificates()
    {
        return $this->hasMany(ClientCertificate::className(), ['group_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourse()
    {
        return $this->hasOne(Course::className(), ['id' => 'course_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeacher()
    {
        return $this->hasOne(Teacher::className(), ['id' => 'teacher_id']);
    }

    public function countStudents()
    {
        return ClientCertificate::find()->where(['group_id' => $this->id])->count();
    }
}
