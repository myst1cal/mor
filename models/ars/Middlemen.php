<?php

namespace app\models\ars;

use Yii;

/**
 * This is the model class for table "middlemen".
 *
 * @property int $id
 * @property string $name
 *
 * @property ClientCertificate[] $clientCertificates
 */
class Middlemen extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'middlemen';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Посредник',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientCertificates()
    {
        return $this->hasMany(ClientCertificate::className(), ['middlemen_id' => 'id']);
    }
}
