<?php


use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'Отчет: посредники';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="report-by-middlemen">

    <h1><?= Html::encode($this->title) ?></h1>

    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <th>Посредние</th>
            <th>Сертификатов, шт.</th>
            <th>Сумма, грн.</th>
            <th></th>
        </tr>
        </thead>

        <tbody>
        <?php
        $totalCertificates = 0;
        $totalSum = 0;
        ?>
        <?php foreach ($data as $middlemenData): ?>
            <tr>
                <td><?= $middlemenData['name'] ?></td>
                <td><?= $middlemenData['count'] ?></td>
                <td><?= $middlemenData['total_sum'] ?></td>
                <td><?= Html::a('Список', Url::to(['/certificate/list-by-middlemen', 'middlemen_id' => $middlemenData['id']])) ?></td>
            </tr>
        <?php $totalCertificates +=  $middlemenData['count'];?>
        <?php $totalSum +=  $middlemenData['total_sum'];?>
        <?php endforeach; ?>
        <tr>
            <td><b>Всего</b></td>
            <td><?= $totalCertificates ?></td>
            <td><?= $totalSum ?></td>
            <td></td>
        </tr>
        </tbody>
    </table>
</div>
