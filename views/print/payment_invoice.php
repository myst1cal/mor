<?php
/** @var \app\models\ars\ClientCertificate[] $certificates */
/** @var \app\models\ars\Client $client */

use app\helpers\FinanceHelper; ?>
<html>

<head>
    <meta http-equiv=Content-Type content="text/html; charset=utf-8">
    <meta name=Generator content="Microsoft Word 14 (filtered)">
    <style>
        <!--
        /* Font Definitions */
        @font-face {
            font-family: Arial;
            panose-1: 2 15 5 2 2 2 4 3 2 4;
        }

        /* Style Definitions */
        p.MsoNormal, li.MsoNormal, div.MsoNormal {
            margin-top: 0cm;
            margin-right: 0cm;
            margin-bottom: 10.0pt;
            margin-left: 0cm;
            line-height: 110%;
            font-size: 10.0pt;
            font-family: "Arial", "sans-serif";
        }

        .MsoPapDefault {
            margin-bottom: 10.0pt;
            line-height: 115%;
        }

        @page WordSection1 {
            size: 595.3pt 841.9pt;
            margin: 36.0pt 36.0pt 36.0pt 36.0pt;
        }

        div.WordSection1 {
            page: WordSection1;
        }


        .page-break {page-break-after: always;}
        -->
    </style>

</head>

<body lang=RU>
<div class=WordSection1>
    <p class=MsoNormal align=center style='text-align:center'><b><span style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif"'>Рахунок на оплату № </span></b><span style='font-size:9.0pt;line-height:115%;font-family:"Arial","sans-serif"'>____________</span><b><span style='font-size:10.0pt; line-height:115%;font-family:"Arial","sans-serif"'> від<span style='font-size:9.0pt;line-height:115%;font-family:"Arial","sans-serif"'>__ __________</span> <?= date('Y') ?> р. </span></b></p>
    <table class=MsoTableGrid border=0 cellspacing=0 width="98%" cellpadding=0 style='border-collapse:collapse;border:none'>
        <tr style='height:13.75pt'>
            <td width=102 rowspan=2 style='width:76.3pt;padding:0cm 5.4pt 0cm 5.4pt;'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height: normal'><span style='font-size:8.0pt;font-family:"Arial","sans-serif"'>Постачальник:</span></p>
            </td>
            <td width=610 valign=top style='padding:0cm 5.4pt 0cm 5.4pt; height:13.75pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height: normal'><b><span style='font-size:8.0pt;font-family:"Arial", "sans-serif"'>ТОВАРИСТВО З ОБМЕЖЕНОЮ ВІДПОВІДАЛЬНІСТЮ "ЄВРОПЕЙСЬКИЙ МОРСЬКИЙ ТРЕНАЖЕРНИЙ ЦЕНТР"</span></b></p>
            </td>
        </tr>
        <tr>
            <td valign=bottom style='padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height: normal'><span style='font-size:8.0pt;font-family:"Arial", "sans-serif"'>п/р UA363806340000026009141514001 у банку ПуАТ "КБ "АКОРДБАНК", 65003, Одеса, Атамана Головатого, будинок № 24, тел.: 0667882424, код за ЄДРПОУ 43116969, ІПН 431169615534</span></p>
            </td>
        </tr>
        <tr>
            <td style='width:76.3pt;padding:0cm 5.4pt 0cm 5.4pt;height:17.2pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height: normal'><span style='font-size:8.0pt;font-family:"Arial", "sans-serif"'>Покупець:</span></p>
            </td>
            <td width=610 style='width:457.8pt;padding:0cm 5.4pt 0cm 5.4pt;height:17.2pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height: normal'><b><span style='font-size:8.0pt;font-family:"Arial", "sans-serif"'><?= $client->getFullNameUkr() ?></span></b></p>
            </td>
        </tr>
        <tr style='height:21.0pt'>
            <td valign=bottom style='width:76.3pt;padding:0cm 5.4pt 0cm 5.4pt; height:21.0pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height: normal'><span style='font-size:8.0pt;font-family:"Arial", "sans-serif"'>Договір:</span></p>
            </td>
            <td>&nbsp;</td>
        </tr>
    </table>
    <p class=MsoNormal><span style='font-size:4.0pt;line-height:100%;font-family:"Arial","sans-serif"'>&nbsp;</span></p>
    <table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0 width="98%" style='border-collapse:collapse;border:none'>
        <tr>
            <td width=28 style='width:20.85pt;border:solid windowtext 1.0pt;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt; text-align:center;line-height:normal'><b><span style='font-size:9.0pt; font-family:"Arial","sans-serif"'>№</span></b></p>
            </td>
            <td width=414 style='width:310.6pt;border:solid windowtext 1.0pt;border-left:
  none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span lang=UK style='font-size:9.0pt;
  font-family:"Arial","sans-serif"'>Товари (роботи, послуги)</span></b></p>
            </td>
            <td width=57 style='width:42.5pt;border:solid windowtext 1.0pt;border-left:
  none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span lang=UK style='font-size:9.0pt;
  font-family:"Arial","sans-serif"'>Кіл-сть</span></b></p>
            </td>
            <td width=57 style='width:42.55pt;border:solid windowtext 1.0pt;border-left:
  none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span lang=UK style='font-size:9.0pt;
  font-family:"Arial","sans-serif"'>Од.</span></b></p>
            </td>
            <td width=76 style='width:2.0cm;border:solid windowtext 1.0pt;border-left:
  none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span lang=UK style='font-size:9.0pt;
  font-family:"Arial","sans-serif"'>Ціна <br>без<br> ПДВ</span></b></p>
            </td>
            <td width=81 style='width:60.9pt;border:solid windowtext 1.0pt;border-left:
  none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span lang=UK style='font-size:9.0pt;
  font-family:"Arial","sans-serif"'>Сума <br>без<br>ПДВ</span></b></p>
            </td>
        </tr>
        <?php $i = 1; ?>
        <?php $totalSum = 0; ?>
        <?php foreach ($certificates as $certificate): ?>
            <tr>
                <td width=28 style='width:20.85pt;border:solid windowtext 1.0pt;border-top: none;padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt; text-align:center;line-height:normal'><span lang=UK style='font-size:8.0pt; font-family:"Arial","sans-serif"'><?= $i++ ?></span></p>
                </td>
                <td width=414 valign=top style='width:310.6pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:8.0pt;font-family:"Arial","sans-serif"'><?= $certificate->course->title_ukr ?></span></p>
                </td>
                <td width=57 style='width:42.5pt;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal align=right style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:right;line-height:normal'><span lang=UK style='font-size:8.0pt;
  font-family:"Arial","sans-serif"'>1</span></p>
                </td>
                <td width=57 style='width:42.55pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span lang=UK style='font-size:8.0pt;
  font-family:"Arial","sans-serif"'>послуга</span></p>
                </td>
                <td width=76 style='width:2.0cm;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal align=right style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:right;line-height:normal'><span lang=UK style='font-size:8.0pt;
  font-family:"Arial","sans-serif"'><?= $certificate->total_sum ?></span></p>
                </td>
                <td width=81 style='width:60.9pt;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal align=right style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:right;line-height:normal'><span lang=UK style='font-size:8.0pt;
  font-family:"Arial","sans-serif"'><?= $certificate->total_sum ?></span></p>
                </td>
            </tr>
            <?php $totalSum += $certificate->total_sum; ?>
        <?php endforeach; ?>
        <tr>
            <td width=28 valign=top style='width:20.85pt;border:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>&nbsp;</span></p>
            </td>
            <td width=414 valign=top style='width:310.6pt;border:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>&nbsp;</span></p>
            </td>
            <td width=57 valign=top style='width:42.5pt;border:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>&nbsp;</span></p>
            </td>
            <td width=57 valign=top style='width:42.55pt;border:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>&nbsp;</span></p>
            </td>
            <td width=76 valign=top style='width:2.0cm;border:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>&nbsp;</span></p>
            </td>
            <td width=81 valign=top style='width:60.9pt;border:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:9.0pt;font-family:"Arial","sans-serif"'>&nbsp;</span></p>
            </td>
        </tr>
        <tr>
            <td width=631 colspan=5 style='width:473.2pt;border:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal align=right style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:right;line-height:normal'><b><span style='font-size:9.0pt;
  font-family:"Arial","sans-serif"'>Всього:</span></b></p>
            </td>
            <td width=81 style='width:60.9pt;border:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal align=right style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:right;line-height:normal'><b><span lang=UK style='font-size:9.0pt;
  font-family:"Arial","sans-serif"'><?= number_format($totalSum, 2) ?></span></b></p>
            </td>
        </tr>
        <tr>
            <td width=631 colspan=5 style='width:473.2pt;border:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal align=right style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:right;line-height:normal'><b><span style='font-size:9.0pt;
  font-family:"Arial","sans-serif"'>Сума ПДВ:</span></b></p>
            </td>
            <td width=81 style='width:60.9pt;border:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <?php $nds = $totalSum / 5; ?>
                <p class=MsoNormal align=right style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:right;line-height:normal'><b><span lang=UK style='font-size:9.0pt;
  font-family:"Arial","sans-serif"'><?= number_format($nds, 2) ?></span></b></p>
            </td>
        </tr>
        <tr>
            <td width=631 colspan=5 style='width:473.2pt;border:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal align=right style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:right;line-height:normal'><b><span style='font-size:9.0pt;
  font-family:"Arial","sans-serif"'>Всього із ПДВ:</span></b></p>
            </td>
            <td width=81 style='width:60.9pt;border:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal align=right style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:right;line-height:normal'><b><span lang=UK style='font-size:9.0pt;
  font-family:"Arial","sans-serif"'><?= number_format($nds + $totalSum, 2) ?></span></b></p>
            </td>
        </tr>
    </table>
    <p class=MsoNormal>
        <span lang=UK style='font-size:9.0pt;line-height:115%;
font-family:"Arial","sans-serif"'>Всього найменувань <?= count($certificates) ?>, на суму <?= $nds + $totalSum ?> грн.<br>
            <b><?= FinanceHelper::priceToUkrText($nds + $totalSum) ?><br>У т.ч. ПДВ: <?= FinanceHelper::priceToUkrText($nds) ?></b>
        </span>
    </p>
    <p class=MsoNormal><span lang=UK style='font-size:9.0pt;line-height:115%;
font-family:"Arial","sans-serif"'>_______________________________________________________________________________________________________</span></p>

    <p class=MsoNormal><span lang=UK style='font-size:9.0pt;line-height:115%;
font-family:"Arial","sans-serif"'>&nbsp;</span></p>

    <p class=MsoNormal align=right style='text-align:right'><b>Виписав(ла):</b><b> </b> <span lang=UK style='font-family:"Times New Roman","serif"'>_______________ </span>Костенюк Р. Б.</p>
    <p class=MsoNormal><span lang=UK style='font-size:9.0pt;line-height:115%; font-family:"Arial","sans-serif"'>&nbsp;</span></p>
    <p class=MsoNormal align=center style='text-align:center'><b><span style='font-size:10.0pt;line-height:115%;font-family:"Arial","sans-serif"'>АКТ <br />приймання-передачі виконаних послуг<br />до договору аферти №___ від “__” ________ 20___року</span></b></p>
    <p class=MsoNormal style='text-align:center'><span lang=UK style='font-size: 10.0pt; font-family:"Arial","sans-serif"'>ТОВАРИСТВО З ОБМЕЖЕНОЮ ВІДПОВІДАЛЬНІСТЮ <br />«ЄВРОПЕЙСЬКИЙ МОРСЬКИЙ ТРЕНАЖЕРНИЙ ЦЕНТР»,</span><br /> <span lang=UK style='font-size: 9.0pt; font-family:"Arial","sans-serif"'>в особі директора Костенюка Р.Б., який в подальшому іменується Виконавець та </span></p>
    <p class=MsoNormal><span lang=UK style='font-size:9.0pt; font-family:"Arial","sans-serif"'><?= $client->getFullNameUkr() ?></span></p>
    <p class=MsoNormal><span lang=UK style='font-size:9.0pt; font-family:"Arial","sans-serif"'>Паспорт: серія <u><?= $client->passport_series ?></u>&nbsp;№ <u><?= $client->passport_number ?></u>, виданий <u><?= Yii::$app->formatter->asDate($client->passport_date_issued) ?> <?= $client->passport_issued_by ?></u> року</span></p>
    <p class=MsoNormal><span lang=UK style='font-size:9.0pt; font-family:"Arial","sans-serif"'>Адреса реєстрації: <u><?= $client->registration_address ?></u></span></p>
    <p class=MsoNormal><span lang=UK style='font-size:9.0pt; font-family:"Arial","sans-serif"'>який надалі іменується Замовник, склали цей Акт, про наступне:</span></p>
    <ol style='margin-top:0cm' start=1 type=1>
        <li class=MsoNormal style='text-align:justify'><span lang=UK style='font-size: 9.0pt; font-family:"Arial","sans-serif"'>Виконавець надав, а Замовник прийняв послуги з навчання (підготовки, перепідготовки, підвищення кваліфікації кадрів або надання додаткової освітньої послуги), далі - освітні послуги, а саме: Підготовка відповідно до Міжнародної конвенції про підготовку і дипломування моряків та несення вахти 1978 року (з Манільським поправками).</span></li>
        <li class=MsoNormal style='text-align:justify'><span lang=UK style='font-size: 9.0pt; font-family:"Arial","sans-serif"'>Вартість послуг склала:  <b><u><?= $nds + $totalSum ?>  грн. 00 коп.</u></b></span></li>
        <li class=MsoNormal style='text-align:justify'><span lang=UK style='font-size:9.0pt; font-family:"Arial","sans-serif"'>Сторони/Замовник не мають/не має жодних претензій одна до одної.</span></li>
        <li class=MsoNormal style='text-align:justify'><span lang=UK style='font-size: 9.0pt; font-family:"Arial","sans-serif"'>Оплата послуг здійснена/не здійснена Замовником у повному обсязі. Оплата послуг повинна бути проведена до “____” _________ 20__ року включно.</span></li>
    </ol>
    <p class=MsoNormal align=center style='text-align:center'><b><span lang=UK style='font-size:9.0pt; font-family:"Arial","sans-serif"'>ПІДПИСИ СТОРІН:</span></b></p>
    <table class=MsoNormalTable align=center border=0 cellspacing=0 cellpadding=0 width="80%" style='border-collapse:collapse'>
        <tr>
            <td width='50%' valign=top align=center style='padding:0cm 0cm 0cm 0cm'>
                <p class=MsoNormal><span lang=UK style='font-size:9.0pt;text-align:center; font-family:"Arial","sans-serif"'>ТОВ “ЕМТЦ”</span></p>
                <p class=MsoNormal><span lang=UK style='font-size:9.0pt;text-align:center'>_______________________</span></p>
            </td>
            <td width='50%' valign=top align=center style='padding:0cm 0cm 0cm 0cm'>
                <p class=MsoNormal><span lang=UK style='font-size:9.0pt;text-align:center; font-family:"Arial","sans-serif"'><?= $client->surname_ukr ?></span></p>
                <p class=MsoNormal><span lang=UK style='font-size:9.0pt;text-align:center; font-family:"Arial","sans-serif"'><?= $client->name_ukr . ' ' . $client->patronymic_ukr ?></span></p>
            </td>
        </tr>
    </table>
</div>
</body>
</html>
