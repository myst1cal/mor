<?php

use app\helpers\AuthHelper;
use yii\helpers\Html;
use yii\web\YiiAsset;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ars\Group */
/* @var $certificates app\models\ars\ClientCertificate[] */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Группы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
YiiAsset::register($this);
?>
<div class="group-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Список', ['index', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
        <?= AuthHelper::canEditGroup() ? Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) : '' ?>
    </p>

    <?= DetailView::widget([
        'model'      => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'course_id',
                'value'     => $model->course->title_ukr
            ],
            [
                'attribute' => 'teacher_id',
                'value'     => $model->teacher->full_name
            ],
            'name',
            'max_students',
            'start_date',
            'end_date',
        ],
    ]) ?>

    <?php if (!empty($certificates)): ?>
        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>Преподаватель</th>
                <th>№ группы</th>
                <th></th>
                <th>ФИО</th>
                <th>даты обучения</th>
                <th>№ сертификата</th>
                <th>дата получения сертификата</th>
                <th>дата сдачи теста</th>
                <th>готовность</th>
            </tr>
            </thead>
            <tbody>
            <?php $i = 1; ?>
            <?php foreach ($certificates as $certificate): ?>
                <tr>
                    <td><?= $model->teacher->full_name ?></td>
                    <td><?= $model->name ?></td>
                    <td><?= $i++; ?></td>
                    <td><?= $certificate->client->getFullNameUkr(true) ?></td>
                    <td><?= $certificate->group->start_date . ' - ' . $certificate->group->end_date ?></td>
                    <td><?= AuthHelper::canEditCertificateNumber() && empty($certificate->number) ? Html::input('text', 'certificateSerialNumber', null, ['class' => 'form-control certificateSerialNumber', 'data-id' => $certificate->id, 'index' => $i]) : $certificate->number ?></td>
                    <td><?= $certificate->getDateEnd() ?></td>
                    <td><?= $certificate->group->end_date ?></td>
                    <td><?= $certificate->status() ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    <?php endif; ?>

    <?= Html::a('Список', ['index', 'id' => $model->id], ['class' => 'btn btn-success']) ?>

    <?php $this->registerJsFile('js/certificate.js', ['depends' => ['yii\web\JqueryAsset']]); ?>
</div>
