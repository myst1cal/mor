<?php

use app\helpers\AuthHelper;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\searches\ClientSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Клиенты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="client-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= AuthHelper::canEditClient() ? Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) : '' ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],

            'surname_ukr',
            'name_ukr',
            'patronymic_ukr',
            'birthday',
            'phone',
            'email:email',
            'comment:ntext',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{add_certificate} {view} {update} {view_log}',
                'buttons' => [
                        'add_certificate' => function ($url, $model) {
                            return AuthHelper::canEditCertificate() ? Html::a(
                                '<span class="glyphicon glyphicon-file"></span>',
                                '/certificate/create?client_id='. $model->id) : '';
                        },
                        'update' => function ($url, $model) {
                            return AuthHelper::canEditClient() ? Html::a(
                                '<span class="glyphicon glyphicon-pencil"></span>',
                                '/client/update?id='. $model->id) : '';
                        },
                        'view_log' => function ($url, $model) {
                            return AuthHelper::canViewLogs() ? Html::a(
                                '<span class="glyphicon glyphicon-list"></span>',
                                '/log/view?table='. $model::tableName() . '&id=' . $model->id, ['target' => '_blank']) : '';
                        },
                ],
            ],
        ],
    ]); ?>


</div>
