<?php
/* @var $client \app\models\ars\Client */
/* @var \app\models\ars\ClientCertificate[] $certificates */
/* @var \app\models\ars\Client $client */
?>

<html xmlns:v="urn:schemas-microsoft-com:vml"
xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:w="urn:schemas-microsoft-com:office:word"
xmlns:m="http://schemas.microsoft.com/office/2004/12/omml"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1251">
<meta name=ProgId content=Word.Document>
<meta name=Generator content="Microsoft Word 15">
<meta name=Originator content="Microsoft Word 15">
<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:AGLettericaCondensedC;
	panose-1:0 0 5 6 0 0 0 0 0 0;
	mso-font-charset:204;
	mso-generic-font-family:modern;
	mso-font-format:other;
	mso-font-pitch:variable;
	mso-font-signature:-2147483005 74 0 0 5 0;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-parent:"";
	margin-top:0cm;
	margin-right:0cm;
	margin-bottom:8.0pt;
	margin-left:0cm;
	line-height:100%;
.MsoChpDefault
	{font-family:"AGLettericaCondensedC",sans-serif;}
.MsoPapDefault
	{margin-bottom:0pt;
	line-height:107%;}
@page WordSection1
	{size:762.156939pt 478.963331pt;
	margin:0cm 0cm 0cm 0cm;
	border:solid windowtext 1.0pt;
	padding:0pt 0pt 0pt 0pt;}
div.WordSection1
	{page:WordSection1;}
.page-break {
    page-break-before: always;
        }
-->
</style>
</head>

<body style="margin-top: 0px;margin-bottom: 0px;margin-right: 0px;margin-left: 0px;">
	<div class=WordSection1>
		<div align=center>
			<table class=MsoTableGrid border=0 cellspacing=0 cellpadding=0 width="1020px" height="641px" style='width:1020px;height:641px;border-collapse:collapse;border:none;mso-yfti-tbllook:1184;mso-padding-alt:0pt;background: url(/images/bg-sert/etmu_ua.png) no-repeat; background-size: contain;'>
				<tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes; page-break-inside:avoid;height:16.9pt'>
					<td width=162 valign=top style='width:495.4pt;border:none;padding:0cm 0.4pt 0cm 0.4pt;'>
						<table class=MsoTableGrid border=0 cellspacing=0 cellpadding=0 width="100%" style='width:100.0%;border-collapse:collapse;border:none;mso-yfti-tbllook:1184;mso-padding-alt:0cm 0.4pt 0cm 0.4pt'>
							<tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;height:181.55pt'>
								<td width=185 valign=top style='width:512.05pt;border:0pt;padding:0cm 0.4pt 0cm 0.4pt'>
									<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt; text-align:justify;line-height:normal'><span style='font-size:12.0pt;font-family:AGLettericaCondensedC'><o:p>&nbsp;</o:p></span></p>
								</td>
								<td width=12 valign=bottom style='width:35.05pt;border:none;padding:0cm 5.4pt 4.4pt 5.4pt'>
									<p><b><span style='font-size:16.0pt;font-family:"AGLettericaCondensedC";padding-left:4pt;'><?= Yii::$app->formatter->asDate($certificate->client->birthday) ?></span></b></p> 
								</td>
							</tr>
							<tr style='mso-yfti-irow:1;height:25.55pt'>
								<td width=105 valign=bottom style='width:512.05pt;border:none;padding:0cm 5.4pt 0cm 5.4pt'>
									<p class=MsoNormal style='margin-left:19pt;'><span style='font-size:24.0pt;font-family:"AGLettericaCondensedC";text-transform: uppercase;'><?= $certificate->client->surname_ukr ?>&nbsp;<?= $certificate->client->name_ukr . ' ' . $certificate->client->patronymic_ukr ?></span></p>
								</td>
								<td width=42 valign=top style='width:73.05pt;border:none;padding:0cm 5.4pt 0cm 5.4pt'>
									<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt; text-align:justify;line-height:normal'><span style='font-size:12.0pt; font-family:AGLettericaCondensedC'><o:p>&nbsp;</o:p></span></p>
								</td>
							</tr>
   <tr style='mso-yfti-irow:2'>
    <td width=105 valign=top style='width:221.05pt;border:0pt;padding:0cm 5.4pt 0cm 5.4pt'>
    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;
    text-align:justify;line-height:normal'><span style='font-size:12.0pt;
    font-family:AGLettericaCondensedC'><o:p>&nbsp;</o:p></span></p>
    </td>
    <td width=42 valign=top style='width:73.05pt;border:none;padding:0cm 5.4pt 0cm 5.4pt'>
    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;
    text-align:justify;line-height:normal'><span style='font-size:12.0pt;
    font-family:AGLettericaCondensedC'><o:p>&nbsp;</o:p></span></p>
    </td>
   </tr>
   <tr style='mso-yfti-irow:3'>
    <td width=105 valign=top style='width:221.05pt;border:none;padding:0cm 5.4pt 0cm 5.4pt'>
    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;
    text-align:justify;line-height:normal'><span style='font-size:12.0pt;
    font-family:AGLettericaCondensedC'><o:p>&nbsp;</o:p></span></p>
    </td>
    <td width=42 valign=top style='width:73.05pt;border:none;padding:0cm 5.4pt 0cm 5.4pt'>
    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;
    text-align:justify;line-height:normal'><span style='font-size:12.0pt;
    font-family:AGLettericaCondensedC'><o:p>&nbsp;</o:p></span></p>
    </td>
   </tr>
   <tr style='mso-yfti-irow:4;mso-yfti-lastrow:yes'>
    <td width=105 valign=top style='width:221.05pt;border:none;padding:0cm 5.4pt 0cm 5.4pt'>
    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;
    text-align:justify;line-height:normal'><span style='font-size:12.0pt;
    font-family:AGLettericaCondensedC'><o:p>&nbsp;</o:p></span></p>
    </td>
    <td width=42 valign=top style='width:73.05pt;border:none;padding:0cm 5.4pt 0cm 5.4pt'>
    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;
    text-align:justify;line-height:normal'><span style='font-size:12.0pt;
    font-family:AGLettericaCondensedC'><o:p>&nbsp;</o:p></span></p>
    </td>
   </tr>
  </table>
  </td>
  <td width=152 valign=top style='width:115.55pt;border:none;padding:0cm 5.4pt 0cm 5.4pt;height:16.9pt'>
  <table class=MsoTableGrid border=0 cellspacing=0 cellpadding=0 width="100%"
   style='width:100.0%;border-collapse:collapse;border:none;mso-border-alt:
   solid windowtext .5pt;mso-yfti-tbllook:1184;mso-padding-alt:0cm 5.4pt 0cm 5.4pt'>
   <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
    <td width=137 valign=bottom style='width:294.25pt;height:110.75pt;padding:0cm 5.4pt 0cm 5.4pt'>
    <p class=MsoNormal style='padding-left:17pt;padding-top:92pt;'><b><span style='font-size:22.0pt;font-family:"AGLettericaCondensedC";'><?= $certificate->number ?></span></b></p>
    </td>
   </tr>
   <tr style='mso-yfti-irow:1'>
    <td width=137 valign=bottom style='width:294.25pt;height:47.75pt;padding:0cm 0.4pt 0cm 0.4pt'>
    <p class=MsoNormal style='padding-left:1pt;padding-top:23pt;'><b><span style='font-size:18.0pt;font-family:"AGLettericaCondensedC";'><?= $certificate->getDateStart() ?></span></b></p>
    </td>
   </tr>
   <tr style='mso-yfti-irow:2;mso-yfti-lastrow:yes'>
    <td width=137 valign=bottom style='width:294.25pt;height:32.75pt;padding:0cm 0.4pt 0cm 0.4pt'>
    <p><b><span style='font-size:18.0pt;font-family:"AGLettericaCondensedC";'><?= $certificate->getDateEnd() ?></span></b></p>
    </td>
   </tr>
  </table>
  </td>
 </tr>
</table>

</div>

</div>

	<div class=page-break>
	<div class=WordSection1>
		<div align=center>
			<table class=MsoTableGrid border=0 cellspacing=0 cellpadding=0 width="1020px" height="641px" style='width:1020px;height:641px;border-collapse:collapse;border:none; mso-padding-alt:0cm 0.4pt 0cm 0.4pt;background: url(/images/bg-sert/etmu_en.png) no-repeat; background-size: contain;'>
				<tr style='page-break-inside:avoid;'>
					<td valign=top style='width:300px;border:none;padding:0cm 0.4pt 0cm 0.4pt;'>
						<table class=MsoTableGrid border=0 cellspacing=0 cellpadding=0 width="300px" style='width:300px; border-collapse:collapse; border:none;mso-padding-alt:0cm 0.4pt 0cm 0.4pt'>
							<tr style='mso-yfti-irow:1'>
								<td valign=top style='width:290.0px;border:none;padding:0cm 5.4pt 0cm 5.4pt'><img width=250 height=365 src="<?= $certificate->client->photoUrl() ?>" style="padding-top:185px; padding-left: 28px;">
								</td>
							</tr>
						</table>
					</td>
					<td valign=top>
						<table valign=top border=0 cellspacing=0 cellpadding=0 width="518px" style='width:518px; border-collapse:collapse; border:none;'>
							<tr>
								<td valign=top align=right style="padding-top: 184px;padding-right:15px;">
									<b><span style='font-size:16.0pt;font-family:"AGLettericaCondensedC";'><?= Yii::$app->formatter->asDate($certificate->client->birthday) ?></span></b>
								</td>	
							</tr>
							<tr>
								<td valign=top style='width:518px;padding:0px'>
									<span style='font-size:24.0pt;font-family:"AGLettericaCondensedC";text-transform: uppercase;'><?= $certificate->client->name_transliteration ?>&nbsp;<?= $certificate->client->surname_transliteration ?></span>
								</td>
							</tr>
						</table>
					</td>
					<td valign=top align=left style='width:197px;padding:0pt;'>
							<table class=MsoTableGrid border=0  style='width:197px;border:none;'>
								<tr>
									<td valign=bottom style='width:197px;height:150px;padding:0cm 0.4pt 3.5pt 5.4pt'>
										<b><span style='padding-left:23px;font-size:22.0pt;font-family:"AGLettericaCondensedC";'><?= $certificate->number ?></span></b>
									</td>
								</tr>
								<tr>
									<td valign=bottom style='width:197px;height:197px;padding:0pt'>
										<b><span style='font-size:18.0pt;font-family:"AGLettericaCondensedC";'><?= $certificate->getDateStart() ?></span></b>
									</td>
								</tr>
								<tr>
									<td valign=bottom style='width:197px;height:45px;padding:0pt'>
									<b><span style='font-size:18.0pt;font-family:"AGLettericaCondensedC";'><?= $certificate->getDateEnd() ?></span></b>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</div>
		</div>
	</body>
</html>