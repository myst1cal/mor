<?php

use yii\db\Migration;

/**
 * Class m190612_165833_addMiddlemenTable
 */
class m190612_165833_addMiddlemenTable extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%middlemen}}', [
            'id'   => $this->primaryKey(),
            'name' => $this->string()->notNull(),
        ], $tableOptions);

        $this->createIndex('client_certificate_middlemen_id_i', '{{%client_certificate}}', ['middlemen_id']);

        $this->addForeignKey('client_certificate_middlemen_id_fk', '{{%client_certificate}}', 'middlemen_id', '{{%middlemen}}', 'id', 'restrict', 'cascade');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('client_certificate_middlemen_id_fk', '{{%client_certificate}}');
        $this->dropIndex('client_certificate_middlemen_id_i', '{{%client_certificate}}');
        $this->dropTable('{{%middlemen}}');
    }
}
