<?php

namespace app\models\searches;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ars\ClientCertificate;

/**
 * CertificateSearch represents the model behind the search form of `app\models\ars\ClientCertificate`.
 */
class CertificateSearch extends ClientCertificate
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'group_id', 'additional_sum_percent', 'paid', 'printed', 'issued', 'middlemen_id'], 'integer'],
            [['client_id',], 'string'],
            [['number'], 'safe'],
            [['cost_price', 'additional_sum', 'total_sum'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ClientCertificate::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'group_id' => $this->group_id,
            'cost_price' => $this->cost_price,
            'additional_sum' => $this->additional_sum,
            'additional_sum_percent' => $this->additional_sum_percent,
            'total_sum' => $this->total_sum,
            'paid' => $this->paid,
            'printed' => $this->printed,
            'issued' => $this->issued,
            'middlemen_id' => $this->middlemen_id,
        ]);

        $query->joinWith('client');
        $query->andFilterWhere(['like', 'client.surname_ukr', $this->client_id]);
        $query->andFilterWhere(['like', 'number', $this->number]);

        return $dataProvider;
    }
}
