<?php

namespace app\models\ars;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "client".
 *
 * @property int $id
 * @property string $name_ukr
 * @property string $patronymic_ukr
 * @property string $surname_ukr
 * @property string $name_transliteration
 * @property string $patronymic_transliteration
 * @property string $surname_transliteration
 * @property string $birthday
 * @property int $created_at
 * @property int $updated_at
 * @property string $phone
 * @property string $email
 * @property string $comment
 * @property string $passport_series [varchar(25)]
 * @property string $passport_number [varchar(25)]
 * @property string $passport_date_issued [date]
 * @property string $passport_issued_by
 * @property string $registration_address
 * @property string $inn [varchar(40)]
 * @property string $photo_dir [varchar(7)]
 * @property integer $photo_name
 */
class Client extends ActiveRecord
{
    public static function tableName()
    {
        return 'client';
    }

    public function rules()
    {
        return [
            [['name_ukr', 'surname_ukr', 'name_transliteration', 'surname_transliteration'], 'required'],
            [['birthday', 'passport_date_issued'], 'safe'],
            [['created_at', 'updated_at',], 'integer'],
            [['name_ukr', 'patronymic_ukr', 'surname_ukr', 'name_transliteration', 'patronymic_transliteration', 'surname_transliteration'], 'string', 'max' => 200],
            [['phone', 'email'], 'string', 'max' => 255],
            [['passport_series', 'passport_number', 'inn'], 'string', 'max' => 25],
            [['photo_dir',], 'string', 'max' => 28],
            [['photo_name',], 'string', 'max' => 15],
            [['comment', 'passport_issued_by', 'registration_address'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name_ukr' => 'Имя',
            'patronymic_ukr' => 'Отчество',
            'surname_ukr' => 'Фамилия',
            'name_transliteration' => 'Имя(транслитерация)',
            'patronymic_transliteration' => 'Отчество(транслитерация)',
            'surname_transliteration' => 'Фамилия(транслитерация)',
            'birthday' => 'Дата рождения',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'phone' => 'Телефон',
            'email' => 'Почта',
            'comment' => 'Комментарий',
            'passport_series' => 'Серия паспорта',
            'passport_number' => 'Номер паспорта',
            'passport_date_issued' => 'Дата выдачи',
            'passport_issued_by' => 'Кем выдан',
            'registration_address' => 'Адрес регистрации',
            'inn' => 'ИНН',
            'photo_dir' => 'Директория фото',
            'photo_name' => 'Фото',
        ];
    }

    public function getFullNameUkr($short = false)
    {
        if ($short) {
            return $this->surname_ukr . ' ' . mb_substr($this->name_ukr, 0, 1) . '. ' . mb_substr($this->patronymic_ukr, 0, 1) . '.';
        } else {
            return $this->surname_ukr . ' ' . $this->name_ukr . ' ' . $this->patronymic_ukr;
        }
    }

    public function getFullNameTransliteration($short = false)
    {
        if ($short) {
            return $this->surname_transliteration . ' ' . mb_substr($this->name_transliteration, 0, 1) . '.';
        } else {
            return $this->surname_transliteration . ' ' . $this->name_transliteration;
        }
    }

    public function getBirthday()
    {
        $months = [
            1 => 'січня',
            2 => 'лютого',
            3 => 'березеня',
            4 => 'квітня',
            5 => 'травня',
            6 => 'червня',
            7 => 'липня',
            8 => 'серпня',
            9 => 'вересня',
            10 => 'жовтня',
            11 => 'листопада',
            12 => 'грудня',
        ];

        if ($this->birthday) {
            $day = date('d', strtotime($this->birthday));
            $month = $months[date('n', strtotime($this->birthday))];
            $year = date('Y', strtotime($this->birthday));

            return "$day $month $year";
        }

        return '';
    }

    public function photoUrl() {
        if (empty($this->photo_name)) {
            return '/images/client-photos/no_photo.jpg';
        }

        return '/' . $this->photo_dir . '/' . $this->photo_name;
    }
}
