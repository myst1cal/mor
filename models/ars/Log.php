<?php

namespace app\models\ars;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "log".
 *
 * @property int $id
 * @property string $table
 * @property string $foreign_id
 * @property string $old_data
 * @property string $new_data
 * @property int $user_id
 * @property int $created_at
 */
class Log extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['table', 'foreign_id', 'created_at'], 'required'],
            [['old_data', 'new_data'], 'string'],
            [['user_id', 'created_at'], 'integer'],
            [['table', 'foreign_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'table' => 'Таблица',
            'foreign_id' => 'Внешний id',
            'old_data' => 'Старые данные',
            'new_data' => 'Новые данные',
            'user_id' => 'Пользователь',
            'created_at' => 'Дата',
        ];
    }
}
