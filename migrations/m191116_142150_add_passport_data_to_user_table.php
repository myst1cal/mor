<?php

use app\models\ars\Client;
use app\models\ars\User;
use yii\db\Migration;

/**
 * Class m191116_142150_add_passport_data_to_user_table
 */
class m191116_142150_add_passport_data_to_user_table extends Migration
{
    public function safeUp()
    {
        /**
         * 1. Серия паспорта
         * 2. Номер паспорта
         * 3. Дата выдачи паспорта
         * 4. Кем выдан паспорт
         * 5. Адрес регистрации
         * 6. ИНН
         */
        $this->addColumn(Client::tableName(), 'passport_series', $this->string(25)->null());
        $this->addColumn(Client::tableName(), 'passport_number', $this->string(25)->null());
        $this->addColumn(Client::tableName(), 'passport_date_issued', $this->date()->null());
        $this->addColumn(Client::tableName(), 'passport_issued_by', $this->text()->null());
        $this->addColumn(Client::tableName(), 'registration_address', $this->text()->null());
        $this->addColumn(Client::tableName(), 'inn', $this->string(40)->null());
    }

    public function safeDown()
    {
        $this->dropColumn(Client::tableName(), 'passport_series');
        $this->dropColumn(Client::tableName(), 'passport_number');
        $this->dropColumn(Client::tableName(), 'passport_date_issued');
        $this->dropColumn(Client::tableName(), 'passport_issued_by');
        $this->dropColumn(Client::tableName(), 'registration_address');
        $this->dropColumn(Client::tableName(), 'inn');
    }
}
