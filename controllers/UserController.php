<?php

namespace app\controllers;

use app\helpers\AuthHelper;
use app\models\ars\AuthAssignment;
use app\models\ars\User;
use app\models\forms\UserForm;
use app\models\searches\UserSearch;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view'],
                        'allow'   => true,
                        'roles'   => AuthHelper::CAN_SEE_MENU_GROUPS,
                    ],
                    [
                        'actions' => ['info'],
                        'allow'   => true,
                        'roles'   => ['@'],
                    ],
                    [
                        'actions' => ['create', 'update'],
                        'allow'   => true,
                        'roles'   => AuthHelper::CAN_EDIT_GROUP,
                    ],
                ],
            ],
        ];
    }

    public function actionInfo()
    {
        /** @var User $model */
        $user = \Yii::$app->user->identity;
        return $this->render('info', ['user' => $user]);
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $form = new UserForm();
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            $user = new User();

            $user->login = $form->login;
            $user->email = $form->email;
            if ($form->password) {
                $user->setPassword($form->password);
            }

            $user->generateAuthKey();

            if ($user->save()) {
                AuthAssignment::deleteAll(['user_id' => $user->id]);

                $auth = \Yii::$app->authManager;
                $role = $auth->getRole($form->role);
                $auth->assign($role, $user->id);

                return $this->redirect(['view', 'id' => $user->id]);
            }
        }

        return $this->render('create', [
            'model' => $form,
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $user = User::findOne($id);

        $form = new UserForm();
        $form->scenario = UserForm::SCENARIO_UPDATE;

        $form->id = $user->id;
        $form->login = $user->login;
        $form->email = $user->email;
        $form->role = $user->getRole();

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {

            $user->login = $form->login;
            $user->email = $form->email;
            if ($form->password) {
                $user->setPassword($form->password);
            }

            $user->generateAuthKey();

            if ($user->save()) {
                AuthAssignment::deleteAll(['user_id' => $user->id]);

                $auth = \Yii::$app->authManager;
                $role = $auth->getRole($form->role);
                $auth->assign($role, $user->id);

                return $this->redirect(['view', 'id' => $user->id]);
            }
        }

        return $this->render('update', [
            'model' => $form,
        ]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public
    function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
}
