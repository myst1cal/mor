$(function () {
    $(document).on('change', '.certificateSerialNumber', function () {
        let id = $(this).attr('data-id');
        let number = $(this).val();
        let index = parseInt($(this).attr('index'));
        if (index) {
            $('[index=' + (index + 1) + ']').focus();
        }

        $.ajax({
            url: '/certificate/change-number',
            method: 'get',
            data: {id: id, number: number},
            dataType: 'json',
            success: function (response) {
                if (response.status == 'OK') {
                    alert('Номер сохранен');
                } else {
                    alert('Не удалось сохранить номер. Попробуйте снова')
                }
            }
        })
    });

    $(document).on('change', '.additional-sum-percent, .cost_price', function () {
        let id = $(this).attr('data-id');
        let attribute = $(this).attr('data-attr');
        let value = $(this).val();

        $.ajax({
            url: '/certificate/set-additional-data',
            method: 'get',
            data: {id: id, attribute: attribute, value: value},
            dataType: 'json',
            success: function (response) {
                if (response.status == 'OK') {
                    $('[data-certificate-id="' + id + '"] .cost_price').text(response.costPrice);
                    $('[data-certificate-id="' + id + '"] .total_sum').text(response.totalSum);
                } else {
                    alert('Не удалось сохранить данные. Попробуйте снова')
                }
            }
        })
    });

    $(document).on('click', '.ajax-status', function () {
        let id = $(this).attr('data-id');
        let attribute = $(this).attr('data-attribute');
        let value = $(this).prop('checked') ? 1 : 0;

        if (attribute == 'paid') {
            let button = $('.print-button[data-id=' + id + ']');
            if (value) {
                button.removeClass('hide')
            } else {
                button.addClass('hide')
            }
        }

        $.ajax({
            url: '/certificate/set-status',
            method: 'get',
            data: {id: id, attribute: attribute, value: value},
            dataType: 'json',
            success: function (response) {
                if (response.status != 'OK') {
                    alert('Не удалось сохранить данные. Попробуйте снова');
                }
            }
        })
    });

    $(document).on('click', '.pay-type', function () {
        let id = $(this).attr('data-id');
        let attribute = $(this).attr('data-attribute');
        let value = $(this).val();

        $.ajax({
            url: '/certificate/set-status',
            method: 'get',
            data: {id: id, attribute: attribute, value: value},
            dataType: 'json',
            success: function (response) {
                if (response.status != 'OK') {
                    alert('Не удалось сохранить данные. Попробуйте снова');
                }
            }
        })
    });

    $(document).on('change', '.change-group', function () {
        let certificateId = $(this).attr('data-id');
        let groupId = $(this).val();

        $.ajax({
            url: '/certificate/set-group',
            method: 'get',
            data: {certificate_id: certificateId, group_id: groupId},
            dataType: 'json',
            success: function (response) {
                if (response.status != 'OK') {
                    alert('Не удалось сохранить данные. Обновите страницу и попробуйте снова.');
                } else {
                    $.pjax.reload({container: '#certificates-table'});
                    alert('Группа сохранена.');
                }
            }
        })
    });

    $(document).on('change', '.date-of-issued', function () {
        let id = $(this).attr('data-certificate_id');
        let value = $(this).val();

        $.ajax({
            url: '/certificate/set-date-of-issue',
            method: 'get',
            data: {id: id, value: value},
            dataType: 'json',
            success: function (response) {
                if (response.status != 'OK') {
                    alert('Не удалось сохранить данные. Попробуйте снова');
                }
            }
        })
    });

    $(document).on('change', '.middlemen', function () {
        let id = $(this).attr('data-id');
        let value = $(this).val();

        $.ajax({
            url: '/certificate/set-middlemen',
            method: 'get',
            data: {id: id, value: value},
            dataType: 'json',
            success: function (response) {
                if (response.status != 'OK') {
                    alert('Не удалось сохранить данные. Попробуйте снова');
                }
            }
        })
    });

    var commentRequest = undefined;
    $(document).on('keyup', '.comment', function () {
        let id = $(this).attr('data-id');
        let value = $(this).val();

        try {
            commentRequest.abort();
        } catch (e) {
            console.log(e.message);
        }

        commentRequest = $.ajax({
            url: '/client/set-comment',
            method: 'get',
            data: {id: id, value: value},
            dataType: 'json',
            success: function (response) {
                if (response.status != 'OK') {
                    alert('Не удалось сохранить данные. Попробуйте снова');
                }
            }
        })
    });


    $(document).on('change', '.additionalField', function () {
        let certificate_id = $(this).attr('data-certificate_id');
        let additional_field_id = $(this).attr('data-additional_field_id');
        let value = $(this).is(':checked');

        $.ajax({
            url: '/certificate/set-additional-field',
            method: 'post',
            data: {certificate_id: certificate_id, additional_field_id: additional_field_id, value: value},
            dataType: 'json',
            success: function (response) {
                if (response.status != 'OK') {
                    alert('Не удалось сохранить данные. Попробуйте снова');
                }
            }
        })
    });
});