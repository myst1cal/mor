<?php

use yii\db\Migration;

/**
 * Class m191023_125213_addLogTable
 */
class m191023_125213_addLogTable extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%log}}', [
            'id' => $this->primaryKey(),
            'table' => $this->string()->notNull(),
            'foreign_id' => $this->string()->notNull(),
            'old_data' => $this->text(),
            'new_data' => $this->text(),
            'user_id' => $this->integer()->null(),
            'created_at' => $this->integer()->notNull(),
        ], $tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable('{{%log}}');
    }
}
