<?php

namespace app\models\ars;

/**
 * This is the model class for table "course_additional_field_course".
 *
 * @property int $course_additional_field_id
 * @property int $course_id
 * @property int $sort
 *
 * @property CourseAdditionalField $courseAdditionalField
 * @property Course $course
 */
class CourseAdditionalFieldCourse extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'course_additional_field_course';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['course_additional_field_id', 'course_id'], 'required'],
            [['course_additional_field_id', 'course_id', 'sort'], 'integer'],
            [['course_additional_field_id', 'course_id'], 'unique', 'targetAttribute' => ['course_additional_field_id', 'course_id']],
            [['course_additional_field_id'], 'exist', 'skipOnError' => true, 'targetClass' => CourseAdditionalField::className(), 'targetAttribute' => ['course_additional_field_id' => 'id']],
            [['course_id'], 'exist', 'skipOnError' => true, 'targetClass' => Course::className(), 'targetAttribute' => ['course_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'course_additional_field_id' => 'Course Additional Field ID',
            'course_id' => 'Course ID',
            'sort' => 'Sort',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourseAdditionalField()
    {
        return $this->hasOne(CourseAdditionalField::className(), ['id' => 'course_additional_field_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourse()
    {
        return $this->hasOne(Course::className(), ['id' => 'course_id']);
    }
}
