<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ars\CourseAdditionalField */

$this->title = 'Добавление доп. поля';
$this->params['breadcrumbs'][] = ['label' => 'Доп. поля курсов', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="course-additional-field-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
