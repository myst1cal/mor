<?php


use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $startDate string*/
/* @var $endDate string*/
/* @var $certificateData array*/

$this->title = 'Отчет: статистика по курсу ' . $startDate . ' ' . $endDate;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="report-statistic">

    <h1><?= Html::encode($this->title) ?></h1>

    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <th>ФИО</th>
            <th>Дата добавления</th>
            <th>Дата выдачи</th>
            <th>Номер сертификата</th>
        </tr>
        </thead>

        <tbody>
        <?php foreach ($certificateData as $certificate): ?>
            <tr>
                <td><?= $certificate['name'] ?></td>
                <td><?= $certificate['date_add'] ?></td>
                <td><?= $certificate['date_of_issue'] ?></td>
                <td><?= $certificate['number'] ?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>
