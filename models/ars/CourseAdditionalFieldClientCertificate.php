<?php

namespace app\models\ars;

use Yii;

/**
 * This is the model class for table "course_additional_field_client_certificate".
 *
 * @property int $course_additional_field_id
 * @property int $client_certificate_id
 * @property string $value
 *
 * @property ClientCertificate $clientCertificate
 * @property CourseAdditionalField $courseAdditionalField
 */
class CourseAdditionalFieldClientCertificate extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'course_additional_field_client_certificate';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['course_additional_field_id', 'client_certificate_id', 'value'], 'required'],
            [['course_additional_field_id', 'client_certificate_id'], 'integer'],
            [['value'], 'string', 'max' => 255],
            [['course_additional_field_id', 'client_certificate_id'], 'unique', 'targetAttribute' => ['course_additional_field_id', 'client_certificate_id']],
            [['client_certificate_id'], 'exist', 'skipOnError' => true, 'targetClass' => ClientCertificate::className(), 'targetAttribute' => ['client_certificate_id' => 'id']],
            [['course_additional_field_id'], 'exist', 'skipOnError' => true, 'targetClass' => CourseAdditionalField::className(), 'targetAttribute' => ['course_additional_field_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'course_additional_field_id' => 'Course Additional Field ID',
            'client_certificate_id' => 'Client Certificate ID',
            'value' => 'Value',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientCertificate()
    {
        return $this->hasOne(ClientCertificate::className(), ['id' => 'client_certificate_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourseAdditionalField()
    {
        return $this->hasOne(CourseAdditionalField::className(), ['id' => 'course_additional_field_id']);
    }
}
