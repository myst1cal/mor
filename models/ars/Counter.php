<?php

namespace app\models\ars;

use Yii;

/**
 * This is the model class for table "counter".
 *
 * @property int $id
 * @property string $name
 * @property int $value
 */
class Counter extends \yii\db\ActiveRecord
{
    const CERTIFICATE_NUMBER = 'certificate';

    public static function tableName()
    {
        return 'counter';
    }

    public function rules()
    {
        return [
            [['name'], 'required'],
            [['value'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'value' => 'Value',
        ];
    }
}
