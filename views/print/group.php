<?php
/** @var \app\models\ars\Client[] $clients */
?>
<html>

<head>
    <meta http-equiv=Content-Type content="text/html; charset=windows-1251">
    <meta name=Generator content="Microsoft Word 14 (filtered)">
    <style>
        <!--
        /* Font Definitions */
        @font-face {
            font-family: Calibri;
            panose-1: 2 15 5 2 2 2 4 3 2 4;
        }

        /* Style Definitions */
        p.MsoNormal, li.MsoNormal, div.MsoNormal {
            margin-top: 0cm;
            margin-right: 0cm;
            margin-bottom: 10.0pt;
            margin-left: 0cm;
            line-height: 115%;
            font-size: 11.0pt;
            font-family: "Calibri", "sans-serif";
        }

        .MsoChpDefault {
            font-family: "Calibri", "sans-serif";
        }

        .MsoPapDefault {
            margin-bottom: 10.0pt;
            line-height: 115%;
        }

        @page WordSection1 {
            size: 595.3pt 841.9pt;
            margin: 36.0pt 36.0pt 36.0pt 36.0pt;
        }

        div.WordSection1 {
            page: WordSection1;
        }

        -->
    </style>

</head>

<body lang=RU>

<div class=WordSection1>

    <table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0 style='border-collapse:collapse;border:none'>
        <tr style='height:21.15pt'>
            <td width=45 rowspan=2 style='width:33.8pt;border:solid windowtext 1.0pt;padding:0cm 5.4pt 0cm 5.4pt;height:21.15pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:center;line-height:normal'>
                    <b>
                        <span style='font-size:12.0pt'>№</span>
                    </b>
                </p>
            </td>
            <td width=189 rowspan=2 style='width:141.8pt;border:solid windowtext 1.0pt;border-left:none;padding:0cm 5.4pt 0cm 5.4pt;height:21.15pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:center;line-height:normal'>
                    <b>
                        <span style='font-size:12.0pt'>П.І.Б.<br />слухача</span>
                    </b>
                </p>
            </td>
            <td width=478 colspan="<?= count($dates) ?>" style='width:358.5pt;border:solid windowtext 1.0pt;border-left:none;padding:0cm 5.4pt 0cm 5.4pt;height:21.15pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:center;line-height:normal'>
                    <b>
                        <span lang=UK style='font-size:12.0pt'>Дата проведення занять</span>
                    </b>
                </p>
            </td>
            <td width=478 rowspan=2 style='border:solid windowtext 1.0pt;border-left:none;padding:0cm 5.4pt 0cm 5.4pt;height:21.15pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:center;line-height:normal'>
                    <b>
                        <span lang=UK style='font-size:12.0pt'>Іспит</span>
                    </b>
                </p>
            </td>
        </tr>
                <tr style='page-break-inside:avoid;height:2.0cm'>
            <?php foreach ($dates as $date): ?>

            <td width=30 style='width:22.45pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 2.4pt 0cm 2.4pt;height:2.0cm'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:2.65pt;margin-bottom:0cm;margin-left:2.65pt;margin-bottom:.0001pt;line-height:normal'>
                    <span class="MsoNormal" align="center" style="margin-bottom:0cm;margin-bottom:.0001pt; writing-mode: vertical-lr; transform: rotate(-180deg);text-align:center;line-height:normal"><?= $date ?></span>
                </p>
            </td>

            <?php endforeach; ?>
            
        </tr>

        <?php $i = 1; ?>
        <?php foreach ($clients as $client): ?>
        <tr>
            <td width=45 style='width:33.8pt;border:solid windowtext 1.0pt;border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:center;line-height:normal'>
                    <span lang=UK><?= $i++ ?></span>
                </p>
            </td>
            <td width=189 style='width:141.8pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal align=left style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:left;line-height:normal'><?= $client->getFullNameUkr() ?></p>
            </td>
            <?php foreach ($dates as $date): ?>
            <td width=30 style='width:22.45pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 2.4pt 0cm 2.4pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:center;line-height:normal'>&nbsp;</p>
            </td>
            <?php endforeach; ?>
            <td width=62 style='width:46.75pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:center;line-height:normal'>&nbsp;</p>
            </td>
        </tr>
        <?php endforeach; ?>

    </table>

    <p class=MsoNormal>&nbsp;</p>

</div>

</body>

</html>