<?php

use yii\db\Migration;

/**
 * Class m190608_172623_addGroupTable
 */
class m190608_172623_addGroupTable extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%group}}', [
            'id'           => $this->primaryKey(),
            'course_id'    => $this->integer()->notNull(),
            'teacher_id'   => $this->integer()->notNull(),
            'name'         => $this->string()->notNull(),
            'max_students' => $this->tinyInteger()->notNull(),
            'start_date'   => $this->date()->notNull(),
            'end_date'     => $this->date()->notNull(),
        ], $tableOptions);

        $this->createIndex('group_teacher_id_i', '{{%group}}', ['teacher_id']);
        $this->createIndex('group_course_id_i', '{{%group}}', ['course_id']);

        $this->addForeignKey('group_teacher_id_fk', '{{%group}}', 'teacher_id', '{{%teacher}}', 'id', 'restrict', 'cascade');
        $this->addForeignKey('group_course_id_fk', '{{%group}}', 'course_id', '{{%course}}', 'id', 'restrict', 'cascade');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('group_teacher_id_fk', '{{%group}}');
        $this->dropForeignKey('group_course_id_fk', '{{%group}}');

        $this->dropIndex('group_teacher_id_i', '{{%group}}');
        $this->dropIndex('group_course_id_i', '{{%group}}');

        $this->dropTable('{{%group}}');
    }
}
