<?php

namespace app\models\ars;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "teacher".
 *
 * @property int $id
 * @property string $full_name
 * @property string $requirements
 * @property string $diploma
 * @property string $work_diploma
 * @property string $certificate
 * @property string $experience
 * @property string $rank
 * @property string $ship_type
 * @property string $phone
 *
 * @property TeacherCourse[] $teacherCourses
 * @property Course[] $courses
 */
class Teacher extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'teacher';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['full_name'], 'required'],
            [['requirements'], 'string'],
            [['full_name', 'diploma', 'work_diploma', 'certificate', 'experience', 'rank', 'ship_type', 'phone'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'           => 'ID',
            'full_name'    => 'ФИО',
            'requirements' => 'Требования',
            'diploma'      => 'Диплом',
            'work_diploma' => 'Рабочий диплом',
            'certificate'  => 'Сертификаты',
            'experience'   => 'Стаж',
            'rank'         => 'Звание на судне',
            'ship_type'    => 'Типы судов',
            'phone'        => 'Телефон',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getTeacherCourses()
    {
        return $this->hasMany(TeacherCourse::className(), ['teacher_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getCourses()
    {
        return $this->hasMany(Course::className(), ['id' => 'course_id'])->viaTable('teacher_course', ['teacher_id' => 'id']);
    }
}
