<html xmlns:v="urn:schemas-microsoft-com:vml"
      xmlns:o="urn:schemas-microsoft-com:office:office"
      xmlns="http://www.w3.org/TR/REC-html40">

<head>
    <meta http-equiv=Content-Type content="text/html; charset=windows-1251">
    <meta name=ProgId content=Word.Document>
    <meta name=Generator content="Microsoft Word 14">
    <meta name=Originator content="Microsoft Word 14">
    <style>
        <!--
        /* Font Definitions */
        @font-face {
            font-family: Calibri;
            panose-1: 2 15 5 2 2 2 4 3 2 4;
            mso-font-charset: 204;
            mso-generic-font-family: swiss;
            mso-font-pitch: variable;
            mso-font-signature: -536870145 1073786111 1 0 415 0;
        }

        @font-face {
            font-family: Tahoma;
            panose-1: 2 11 6 4 3 5 4 4 2 4;
            mso-font-charset: 204;
            mso-generic-font-family: swiss;
            mso-font-pitch: variable;
            mso-font-signature: -520081665 -1073717157 41 0 66047 0;
        }

        /* Style Definitions */
        p.MsoNormal, li.MsoNormal, div.MsoNormal {
            mso-style-unhide: no;
            mso-style-qformat: yes;
            mso-style-parent: "";
            margin-top: 0cm;
            margin-right: 0cm;
            margin-bottom: 10.0pt;
            margin-left: 0cm;
            line-height: 115%;
            mso-pagination: widow-orphan;
            font-size: 11.0pt;
            font-family: "Calibri", "sans-serif";
            mso-fareast-font-family: "Times New Roman";
            mso-fareast-theme-font: minor-fareast;
            mso-bidi-font-family: "Times New Roman";
        }

        a:-webkit-any-link {
            color: #000;
            cursor: pointer;
            text-decoration: none !important;
        }

        p.MsoAcetate, li.MsoAcetate, div.MsoAcetate {
            mso-style-noshow: yes;
            mso-style-priority: 99;
            mso-style-link: "Текст выноски Знак";
            margin: 0cm;
            margin-bottom: .0001pt;
            mso-pagination: widow-orphan;
            font-size: 8.0pt;
            font-family: "Tahoma", "sans-serif";
            mso-fareast-font-family: "Times New Roman";
            mso-fareast-theme-font: minor-fareast;
        }

        span.a {
            mso-style-name: "Текст выноски Знак";
            mso-style-noshow: yes;
            mso-style-priority: 99;
            mso-style-unhide: no;
            mso-style-locked: yes;
            mso-style-link: "Текст выноски";
            font-family: "Tahoma", "sans-serif";
            mso-ascii-font-family: Tahoma;
            mso-hansi-font-family: Tahoma;
            mso-bidi-font-family: Tahoma;
        }

        p.msopapdefault, li.msopapdefault, div.msopapdefault {
            mso-style-name: msopapdefault;
            mso-style-unhide: no;
            mso-margin-top-alt: auto;
            margin-right: 0cm;
            margin-bottom: 10.0pt;
            margin-left: 0cm;
            line-height: 115%;
            mso-pagination: widow-orphan;
            font-size: 12.0pt;
            font-family: "Times New Roman", "serif";
            mso-fareast-font-family: "Times New Roman";
            mso-fareast-theme-font: minor-fareast;
        }

        span.SpellE {
            mso-style-name: "";
            mso-spl-e: yes;
        }

        span.GramE {
            mso-style-name: "";
            mso-gram-e: yes;
        }

        .MsoChpDefault {
            mso-style-type: export-only;
            mso-default-props: yes;
            font-size: 10.0pt;
            mso-ansi-font-size: 10.0pt;
            mso-bidi-font-size: 10.0pt;
        }

        @page WordSection1 {
            size: 595.3pt 841.9pt;
            margin: 0cm 0cm 0cm 0cm;
            mso-header-margin: 35.4pt;
            mso-footer-margin: 35.4pt;
            mso-paper-source: 0;
        }

        div.WordSection1 {
            page: WordSection1;
        }

        -->
    </style>
</head>

<body lang=RU style='tab-interval:35.4pt'>

<div class=WordSection1>

    <div align=center>

        <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=794
               style='width:595.5pt;border-collapse:collapse;mso-yfti-tbllook:1184;
 mso-padding-alt:0cm 0cm 0cm 0cm'>
            <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes;
  page-break-inside:avoid;height:419.6pt;mso-height-rule:exactly'>
                <td width="50%" valign=top style='width:50.0%;padding:2.85pt 5.4pt 0cm 5.4pt;
  height:419.6pt;mso-height-rule:exactly'>
                    <div align=center>
                        <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=381
                               style='width:285.95pt;border-collapse:collapse;mso-yfti-tbllook:1184;
   mso-padding-alt:0cm 0cm 0cm 0cm'>
                            <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;page-break-inside:avoid;
    height:20.85pt;mso-height-rule:exactly'>
                                <td width=114 colspan=3 rowspan=2 style='width:85.8pt;padding:0cm 5.4pt 0cm 5.4pt;
    height:20.85pt;mso-height-rule:exactly'>
                                
                                </td>
                                <td width=267 colspan=5 valign=top style='width:200.15pt;padding:0cm 5.4pt 0cm 5.4pt;
    height:20.85pt;mso-height-rule:exactly'>
                                    
                                </td>
                            </tr>
                            <tr style='mso-yfti-irow:1;height:20.05pt;mso-height-rule:exactly'>
                                <td width=133 colspan=3 style='width:100.0pt;padding:0cm 5.4pt 0cm 5.4pt;
    height:20.05pt;mso-height-rule:exactly'>
                                  
                                </td>
                                <td width=134 colspan=2 style='width:100.15pt;padding:0cm 5.4pt 0cm 5.4pt;
    height:20.05pt;mso-height-rule:exactly'>
                                    
                                </td>
                            </tr>
                            <tr style='mso-yfti-irow:2;height:19.85pt'>
                                <td width=381 colspan=8 style='width:285.95pt;padding:0cm 5.4pt 0cm 5.4pt;
    height:15.85pt'>
                                   <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:
    .0001pt;text-align:center;line-height:normal'><b><span
                                                    style='font-size:12.0pt;font-family:"Times New Roman","serif"'>СВІДОЦТВО&nbsp;№ <?= $certificate->number ?></span></b>
                                    </p>
                                </td>
                            </tr>
                            <tr style='mso-yfti-irow:4;height:10.35pt'>
                                <td width=381 colspan=8 style='width:285.95pt;padding:0cm 5.4pt 0cm 5.4pt;
    height:10.35pt'>
                                  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;
    line-height:normal'><span style='font-size:7.0pt;
    font-family:"Times New Roman","serif"'>Цим засвідчується, що:</span></p>
                                </td>
                            </tr>
                            <tr style='mso-yfti-irow:5;height:14.85pt'>
                                <td width=381 colspan=8 style='width:285.95pt;padding:0cm 5.4pt 0cm 5.4pt;
    height:14.85pt'>
                                   <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:
    .0001pt;text-align:center;line-height:normal'><b><span lang=EN-US
                                                              style='font-size:14.0pt;font-family:"Times New Roman","serif";mso-ansi-language:
    EN-US'><?= $certificate->client->surname_ukr ?></span></b></p>
                                </td>
                            </tr>
                            <tr style='mso-yfti-irow:6;height:14.85pt'>
                                <td width=381 colspan=8 style='width:285.95pt;padding:0cm 5.4pt 0cm 5.4pt;
    height:14.85pt'>
                                   <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:
    .0001pt;text-align:center;line-height:normal'><b><span lang=EN-US
                                                              style='font-size:14.0pt;font-family:"Times New Roman","serif";mso-ansi-language:
    EN-US'> <?= $certificate->client->name_ukr . ' ' . $certificate->client->patronymic_ukr ?></span></b></p>
                                </td>
                            </tr>
                            <tr style='mso-yfti-irow:7;height:17.0pt;mso-height-rule:exactly'>
                                <td width=114 colspan=3 style='width:85.8pt;padding:0cm 5.4pt 0cm 5.4pt;
    height:17.0pt;mso-height-rule:exactly'>
                                    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;
    line-height:normal'><b><span style='font-size:8.0pt;font-family:"Times New Roman","serif"'>Дата&nbsp;
    народження:</span></b></p>
                                </td>
                                <td width=152 colspan=4 style='width:113.9pt;padding:0cm 5.4pt 0cm 5.4pt;
    height:17.0pt;mso-height-rule:exactly'>
                                   <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:
    .0001pt;text-align:center;line-height:normal'><b><span lang=EN-US
                                                           style='font-size:11.0pt;font-family:"Times New Roman","serif";mso-ansi-language:
    EN-US'><?= Yii::$app->formatter->asDate($certificate->client->birthday) ?></span></b></p>
                                </td>
        <td width=115 valign=top style='width:86.25pt;padding:0cm 5.4pt 0cm 5.4pt;height:17.0pt;mso-height-rule:exactly'>&nbsp;</td>
                            </tr>
                            <tr style='mso-yfti-irow:8;height:19.85pt'>
                                <td width=381 colspan=8 style='width:285.95pt;padding:0cm 5.4pt 0cm 5.4pt;
    height:19.85pt'>
                                   <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;
    line-height:normal'><span style='font-size:7.0pt;
    font-family:"Times New Roman","serif"'>успішно закінчив схвалений курс підготовки </span></p>
                                </td>
                            </tr>
							                            <tr style='mso-yfti-irow:9;height:19.85pt'>
                                <td width=381 colspan=8 style='width:285.95pt;padding:0cm 5.4pt 0cm 5.4pt;
    height:19.85pt'>
                                   <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt; line-height:normal;text-align:center;'><span style='font-size:11.0pt;
    font-family:"Times New Roman","serif";text-align:center;'><b>СУДНОВА СИСТЕМА ХАРЧОВОЇ САНІТАРІЇ</b></span></p>
                                </td>
                            </tr>
                            <tr style='mso-yfti-irow:10;page-break-inside:avoid;height:21.05pt'>
                                <td width=381 colspan=8 style='width:285.95pt;padding:0cm 5.4pt 0cm 5.4pt;'>
<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=353
 style='width:265.0pt;margin-left:4.65pt;border-collapse:collapse'>
 <tr style='height:16.5pt'>
  <td width=295 style='width:221.0pt;padding:0cm 5.4pt 0cm 5.4pt;height:16.5pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:9.0pt;font-family:"Times New Roman","serif";
  color:black'>на основі вимог MLC 2006 р. А 3.2 та керівних зазначень департаменту США з охорони здоров'я та сервісного центру з контролю та профілактиці людських захварювань, а також включаючи:</span></p>
  </td>
 </tr>
 <tr style='height:16.5pt'>
  <td width=295 style='width:221.0pt;padding:0cm 5.4pt 0cm 5.4pt;height:16.5pt'>
   <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:9.0pt;font-family:"Times New Roman","serif";
  color:black'> - основи, значення і загальні частини аналізу ризиків та критичних контрольних пунктів (ХАССП); 				
</span></p>
  </td>
 </tr>
 <tr style='height:10.5pt'>
  <td width=295 style='width:221.0pt;padding:0cm 5.4pt 0cm 5.4pt;height:10.5pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:9.0pt;font-family:"Times New Roman","serif";
  color:black'> - зберігання та використання харчових продуктів і страв;				
</span></p>
  </td>
 </tr>
 <tr style='height:9.75pt'>
  <td width=295 style='width:221.0pt;padding:0cm 5.4pt 0cm 5.4pt;height:9.75pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:9.0pt;font-family:"Times New Roman","serif";
  color:black'> - використання обладнання, миючих засобів та санітарних розчинів;</span></p>
  </td>
 </tr>
 <tr style='height:14.5pt'>
  <td width=295 style='width:221.0pt;padding:0cm 5.4pt 0cm 5.4pt;height:10.5pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:9.0pt;font-family:"Times New Roman","serif";
  color:black'> - практика прибирання та гігієна приміщення камбузу.</span></p>
  </td>
 </tr>
</table>

                                </td>
                            </tr>
                            <tr style='mso-yfti-irow:11;height:22.7pt'>
                                <td width=92 colspan=2 rowspan=4 style='width:69.35pt;padding:0cm 5.4pt 0cm 5.4pt; height:22.7pt'>&nbsp;</td>
                                <td width=174 colspan=5 valign=bottom style='width:130.35pt;padding:0cm 5.4pt 0cm 5.4pt; height:22.7pt'>
                                    <p class=MsoNormal align=right style='margin-bottom:0cm;margin-bottom:.0001pt; text-align:right;line-height:normal'><span style='font-size:8.0pt;font-family:"Times New Roman","serif"'>Керівник&nbsp;&nbsp;&nbsp;&nbsp;..……………......…...</span></p>
                                </td>
                               <td width=115 rowspan=2 valign=center style='width:86.25pt;padding:0cm 5.4pt 0cm 5.4pt; height:22.7pt'></td>
                            </tr>
                            <tr style='mso-yfti-irow:12;height:22.7pt'>
                                <td width=174 colspan=5 valign=bottom style='width:130.35pt;padding:0cm 5.4pt 0cm 5.4pt;height:22.7pt'>
                                    <p class=MsoNormal align=right style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:right;line-height:normal'><span style='font-size:8.0pt;font-family:"Times New Roman","serif"'>Реєстратор&nbsp;&nbsp;..……………............</span></p>
                                </td>
                            </tr>
                            <tr style='mso-yfti-irow:13;height:22.7pt'>
                                <td width=77 colspan=3 valign=bottom style='width:57.9pt;padding:0cm 5.4pt 0cm 5.4pt; height:22.7pt'>
                                   <p class=MsoNormal align=right style='margin-bottom:0cm;margin-bottom:1pt; text-align:right;line-height:normal'><span style='font-size:8.0pt; font-family:"Times New Roman","serif"'>Дата видачі:</span></p>
                                </td>
                                <td width=97 nowrap colspan=2 valign=bottom style='width:72.45pt; padding:0cm 2.85pt 0cm 2.85pt;height:22.7pt'>
                                    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal'><b><span style='font-size:12.0pt;font-family: "Times New Roman","serif";mso-ansi-language:EN-US'><?= $certificate->getDateStart() ?></span></b>
                                    </p>
                                </td>
                                <td width=115 rowspan=2 valign=center align=center style='width:86.25pt;padding:0cm 0cm 0cm 0cm; height:22.7pt'><img width=53 height=53 src="/assets/img/qr-code.gif" style="padding-top: 15px;"></td>
                            </tr>
                            <tr style='mso-yfti-irow:14;height:22.7pt;mso-height-rule:exactly'>
                                <td width=77 colspan=3 valign=bottom style='width:57.9pt;padding:0cm 5.4pt 0cm 5.4pt; height:22.7pt;mso-height-rule:exactly'>
                                    <p class=MsoNormal align=right style='margin-bottom:0cm;margin-bottom:1pt;text-align:right;line-height:normal'><span style='font-size:8.0pt;font-family:"Times New Roman","serif"'>Дійсно до</span></p>
                                </td>
                                <td width=97 colspan=2 valign=bottom style='width:72.45pt;padding:0cm 2.85pt 0cm 2.85pt; height:22.7pt; mso-height-rule:exactly'>
                                    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal'><b><span style='font-size:12.0pt; font-family: "Times New Roman","serif";'><?= $certificate->getDateEnd() ?></span></b></p>
                                </td>
                            </tr>
                            <tr style='mso-yfti-irow:15;height:34.0pt'>
        <td width=381 colspan=8 style='width:285.95pt;padding:0cm 0cm 0cm 0cm; height:34.0pt'>&nbsp;
                                </td>
                            </tr>
                            <tr style='mso-yfti-irow:16;mso-yfti-lastrow:yes'>
                                <td width=76 style='width:57.2pt;padding:0cm 0cm 0cm 0cm'></td>
                                <td width=38 colspan=2 style='width:28.6pt;padding:0cm 0cm 0cm 0cm'></td>
                                <td width=38 style='width:28.6pt;padding:0cm 0cm 0cm 0cm'></td>
                                <td width=95 colspan=2 style='width:71.4pt;padding:0cm 0cm 0cm 0cm'></td>
                                <td width=19 style='width:13.9pt;padding:0cm 0cm 0cm 0cm'></td>
                                <td width=115 style='width:86.25pt;padding:0cm 0cm 0cm 0cm'></td>
                            </tr>
                            <![if !supportMisalignedColumns]>
                            <tr height=0>
                                <td width=76 style='border:none'></td>
                                <td width=16 style='border:none'></td>
                                <td width=22 style='border:none'></td>
                                <td width=39 style='border:none'></td>
                                <td width=17 style='border:none'></td>
                                <td width=79 style='border:none'></td>
                                <td width=19 style='border:none'></td>
                                <td width=113 style='border:none'></td>
                            </tr>
                            <![endif]>
                        </table>
                    </div>
                </td>
    <td width="50%" valign=top style='width:50.0%;padding:2.85pt 5.4pt 0cm 5.4pt; height:419.6pt;mso-height-rule:exactly'>
        <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
                           style='border-collapse:collapse;mso-yfti-tbllook:1184;mso-padding-alt:0cm 0cm 0cm 0cm'>
                        <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;height:20.85pt;mso-height-rule:
    exactly'>
                            <td width=114 rowspan=2 style='width:85.8pt;padding:0cm 5.4pt 0cm 5.4pt;
    height:20.85pt;mso-height-rule:exactly'>
                                
                            </td>
                            <td width=281 colspan=7 valign=top style='width:211.05pt;padding:0cm 5.4pt 0cm 5.4pt;
    height:20.85pt;mso-height-rule:exactly'>
                              
                            </td>
                        </tr>
                        <tr style='mso-yfti-irow:1;height:20.05pt;mso-height-rule:exactly'>
                            <td width=159 colspan=3 style='width:119.6pt;padding:0cm 5.4pt 0cm 5.4pt;
    height:20.05pt;mso-height-rule:exactly'>
                             
                            </td>
                            <td width=122 colspan=4 style='width:91.45pt;padding:0cm 5.4pt 0cm 5.4pt;
    height:20.05pt;mso-height-rule:exactly'>
                                
                            </td>
                        </tr>
                        <tr style='mso-yfti-irow:2;height:19.85pt'>
                            <td width=396 colspan=8 style='width:296.85pt;padding:0cm 5.4pt 0cm 5.4pt;
    height:15.85pt'>
                              <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:
    .0001pt;text-align:center;line-height:normal'><b><span
                                                style='font-size:12.0pt;font-family:"Times New Roman","serif";'>CERTIFICATE&nbsp;№ <?= $certificate->number ?></span></b>
                                </p>
                            </td>
                        </tr>
                        <tr style='mso-yfti-irow:4;height:10.35pt'>
    <td width=396 colspan=8 style='width:296.85pt;padding:0cm 5.4pt 0cm 5.4pt; height:10.35pt'>
        <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt; line-height:normal'><span lang=EN-US style='font-size:7.0pt;font-family: "Times New Roman","serif";mso-ansi-language:EN-US'>This is to certify&nbsp;that:</span></p>
    </td>
                        </tr>
                        <tr style='mso-yfti-irow:5;height:14.85pt'>
                            <td width=396 colspan=8 style='width:296.85pt;padding:0cm 5.4pt 0cm 5.4pt;
    height:14.85pt'>
                               <p class=MsoNormal align=center
                                   style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:center;line-height:normal'>
                                    <b><span lang=EN-US
                                                style='font-size:14.0pt;font-family:"Times New Roman","serif";mso-ansi-language: EN-US'><?= $certificate->client->name_transliteration ?></span></b>
                                </p>
                            </td>
                        </tr>
                        <tr style='mso-yfti-irow:6;height:14.85pt'>
                            <td width=396 colspan=8 style='width:296.85pt;padding:0cm 5.4pt 0cm 5.4pt;
    height:14.85pt'>
                              <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:
    .0001pt;text-align:center;line-height:normal'><b><span lang=EN-US
                                                              style='font-size:14.0pt;font-family:"Times New Roman","serif";mso-ansi-language:
    EN-US'><?= $certificate->client->surname_transliteration ?></span></b></p>
                            </td>
                        </tr>
                        <tr style='mso-yfti-irow:7;height:17.0pt;mso-height-rule:exactly'>
                            <td width=114 style='width:85.8pt;padding:0cm 5.4pt 0cm 5.4pt;height:17.0pt;
    mso-height-rule:exactly'>
                                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;
    line-height:normal'><b><span lang=EN-US style='font-size:8.0pt;font-family:
    "Times New Roman","serif";mso-ansi-language:EN-US'>Date of birth:</span></b></p>
                            </td>
                            <td width=175 colspan=5 style='width:131.05pt;padding:0cm 5.4pt 0cm 5.4pt;
    height:17.0pt;mso-height-rule:exactly'>
                               <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:
    .0001pt;text-align:center;line-height:normal'><b><span lang=EN-US
                                                           style='font-size:11.0pt;font-family:"Times New Roman","serif";mso-ansi-language:
    EN-US'><?= Yii::$app->formatter->asDate($certificate->client->birthday) ?></span></b></p>
                            </td>
                            <td width=107 colspan=2 valign=top style='width:80.0pt;padding:0cm 5.4pt 0cm 5.4pt;
    height:17.0pt;mso-height-rule:exactly'>
                                
                            </td>
                        </tr>
                        <tr style='mso-yfti-irow:8;height:19.85pt'>
                            <td width=396 colspan=8 style='width:296.85pt;padding:0cm 5.4pt 0cm 5.4pt;
    height:19.85pt'>
                              <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt; line-height:normal'><span lang=EN-US style='font-size:7.0pt;font-family:"Times New Roman","serif";mso-ansi-language:EN-US'>Has successfully completed approved course of training on the syllabus</span></p>
                            </td>
                        </tr>
						<tr style='mso-yfti-irow:9;height:19.85pt'>
                            <td width=396 colspan=8 style='width:296.85pt;padding:0cm 5.4pt 0cm 5.4pt;
    height:19.85pt'>
                              <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:center;line-height:normal'><span lang=EN-US style='font-size:11.0pt;font-family:"Times New Roman","serif";text-align:center;mso-ansi-language:EN-US'><b>VESSEL FOOD`S SANITATION SYSTEM</b></span></p>
                            </td>
                        </tr>
                        <tr style='mso-yfti-irow:10;height:21.05pt'>
                            <td width=396 colspan=8 style='width:296.85pt;padding:0cm 5.4pt 0cm 5.4pt;'>
 <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=353
         style='width:265.0pt;margin-left:4.65pt;border-collapse:collapse'>
 <tr style='height:16.5pt'>
  <td width=353 style='width:231.0pt;padding:0cm 5.4pt 0cm 5.4pt;height:16.5pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span lang=EN-US style='font-size:9.0pt;font-family:"Times New Roman","serif";
  color:black'>based on the requirements of MLC 2006, A 3.2 and guidelines of the USA Departament Health and Services Centres for Disease Control and Prevention and includes the following sections:<br />&nbsp;</span></p>
  </td>
  </tr>
 <tr style='height:16.5pt'>
  <td width=353 style='width:231.0pt;padding:0cm 5.4pt 0cm 5.4pt;height:16.5pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span lang=EN-US style='font-size:9.0pt;font-family:"Times New Roman","serif";color:black'> - fundamentals, importance and general parts of Hazard Analysis and Critical Control Point (HACCP);</span></p>
  </td>
 </tr>
 <tr style='height:10.5pt'>
  <td width=353 style='width:231.0pt;padding:0cm 5.4pt 0cm 5.4pt;height:10.5pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height: normal'><span lang=EN-US style='font-size:9.0pt;font-family:"Times New Roman","serif"; color:black'> - storage and use of food products and  dished;</span></p>
  </td>
 </tr>
 <tr style='height:9.75pt'>
  <td width=353 style='width:231.0pt;padding:0cm 5.4pt 0cm 5.4pt;height:9.75pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal'><span lang=EN-US style='font-size:9.0pt;font-family:"Times New Roman","serif"; color:black'> - use of equipment, detergent and sanitary solution;</span></p>
  </td>
 </tr>
 <tr style='height:14.5pt'>
  <td width=353 style='width:231.0pt;padding:0cm 5.4pt 0cm 5.4pt;height:14.5pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height: normal'><span lang=EN-US style='font-size:9.0pt;font-family:"Times New Roman","serif";color:black'> - cleaning practice and hygiene of galley premises. <br />&nbsp;</span></p>
  </td>
 </tr>
 </table>
                            </td>
                        </tr>
                        <tr style='mso-yfti-irow:11;height:22.7pt'>
                            <td width=114 rowspan=5 style='width:85.8pt;padding:0cm 5.4pt 0cm 5.4pt; height:22.7pt'>&nbsp;</td>
                            <td width=164 colspan=4 valign=bottom style='width:125.25pt;padding:0cm 5.4pt 0cm 5.4pt; height:22.7pt'>
                                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt; line-height:normal'><span lang=EN-US style='font-size:8.0pt;font-family: "Times New Roman","serif";mso-ansi-language:EN-US'>Manager&nbsp;&nbsp;&nbsp;&nbsp;..……………......…......</span></p>
                            </td>
                            <td width=117 colspan=3 rowspan=2 style='width:87.8pt;padding:0cm 0cm 0cm 0cm; height:22.7pt'> </td>
                        </tr>
                        <tr style='mso-yfti-irow:12;height:22.7pt'>
                            <td width=164 colspan=4 valign=bottom style='width:125.25pt;padding:0cm 5.4pt 0cm 5.4pt; height:22.7pt'>
                                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt; line-height:normal'><span lang=EN-US style='font-size:8.0pt;font-family: "Times New Roman", "serif"; mso-ansi-language:EN-US'>Registrar&nbsp;&nbsp;&nbsp;&nbsp; ..……………......…......</span></p>
                            </td>
                        </tr>
                        <tr style='mso-yfti-irow:13;height:22.7pt'>
                            <td width=78 valign=bottom style='width:58.45pt;padding:0cm 5.4pt 0cm 5.4pt; height:22.7pt'>
                                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:1pt; line-height:normal'><span lang=EN-US style='font-size:8.0pt;font-family: "Times New Roman","serif";mso-ansi-language:EN-US'>Date of issue</span></p>
                            </td>
                            <td width=86 valign=bottom colspan=3 style='width:64.8pt;padding:7.4pt 2.4pt 0cm 2.4pt; height:22.7pt'>
                                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom: .0001pt; text-align: center;line-height:normal'><b><span lang=EN-US style='font-size:12.0pt; font-family:"Times New Roman","serif";mso-ansi-language: EN-US'><?= $certificate->getDateStart() ?></span></b></p>
                            </td>
                            <td width=117 colspan=3 rowspan=2 valign=center align=center style='width:87.8pt; padding:0cm 5.4pt 0cm 5.4pt;height:22.7pt'><img width=53 height=53 src="/assets/img/qr-code.gif" style="padding-top: 15px;"></td>
                        </tr>
                        <tr style='mso-yfti-irow:14;height:22.7pt;mso-height-rule:exactly'>
                            <td width=78 valign=bottom style='width:60.45pt;padding:0cm 5.4pt 0cm 5.4pt; height:22.7pt;mso-height-rule:exactly'>
                                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:1pt; line-height:normal'><span lang=EN-US style='font-size:8.0pt;font-family: "Times New Roman","serif";mso-ansi-language:EN-US'>Date of expire</span></p>
                            </td>
                            <td width=86 colspan=3 valign=bottom style='width:64.8pt;padding:7.4pt 5.4pt 0cm 5.4pt; height:22.7pt;mso-height-rule:exactly'>
                                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt; text-align:center;line-height:normal'><b><span lang=EN-US style='font-size:12.0pt; font-family:"Times New Roman","serif";mso-ansi-language: EN-US'><?= $certificate->getDateEnd() ?></span></b></p>
                            </td>
                        </tr>
                        <tr style='mso-yfti-irow:15;height:36.85pt'>
    <td width=281 colspan=7 style='width:211.05pt;padding:0cm 0cm 0cm 0cm; height:36.85pt'>&nbsp;</td>
                        </tr>
                        <tr style='mso-yfti-irow:16;mso-yfti-lastrow:yes'>
                            <td width=114 style='width:85.8pt;padding:0cm 0cm 0cm 0cm'></td>
                            <td width=134 colspan=2 style='width:100.2pt;padding:0cm 0cm 0cm 0cm'></td>
                            <td width=26 style='width:19.4pt;padding:0cm 0cm 0cm 0cm'></td>
                            <td width=5 style='width:3.65pt;padding:0cm 0cm 0cm 0cm'></td>
                            <td width=54 colspan=2 style='width:40.4pt;padding:0cm 0cm 0cm 0cm'></td>
                            <td width=63 style='width:47.4pt;padding:0cm 0cm 0cm 0cm'></td>
                        </tr>
                        <![if !supportMisalignedColumns]>
                        <tr height=0>
                            <td width=114 style='border:none'></td>
                            <td width=78 style='border:none'></td>
                            <td width=67 style='border:none'></td>
                            <td width=31 style='border:none'></td>
                            <td width=6 style='border:none'></td>
                            <td width=14 style='border:none'></td>
                            <td width=44 style='border:none'></td>
                            <td width=59 style='border:none'></td>
                        </tr>
                        <![endif]>
                    </table>
                </td>
            </tr>
        </table>

    </div>
</div>
</body>
</html>