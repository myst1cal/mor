<?php

use app\models\ars\ClientCertificate;
use yii\db\Migration;

class m211024_142151_add_price_to_client_certificate extends Migration
{
    public function safeUp()
    {
        $table = \Yii::$app->db->schema->getTableSchema(ClientCertificate::tableName());
        if (!isset($table->columns['cost_price_2'])) {
            $this->addColumn(ClientCertificate::tableName(), 'cost_price_2', $this->decimal(12, 2));
        }

        $this->addColumn(ClientCertificate::tableName(), 'price_type', $this->tinyInteger()->defaultValue(ClientCertificate::PRICE_TYPE_FULL_COURSE)->after('cost_price_2'));
        $this->update(ClientCertificate::tableName(), ['cost_price_2' => 0.0]);
    }

    public function safeDown()
    {
        $this->dropColumn(ClientCertificate::tableName(), 'cost_price_2');
    }
}
