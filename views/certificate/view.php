<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ars\ClientCertificate */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Сертификаты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="client-certificate-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Список', ['index'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model'      => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'group_id',
                'value'     => isset($model->group) ? $model->group->name : '',
            ],
            [
                'attribute' => 'client_id',
                'value'     => $model->client->getFullNameUkr(),
            ],
            'number',
            'cost_price',
            'additional_sum',
            'total_sum',

            [
                'attribute' => 'paid',
                'value'     => $model->paid ? 'Да' : 'Нет',
            ],
            [
                'attribute' => 'printed',
                'value'     => $model->printed ? 'Да' : 'Нет',
            ],
            [
                'attribute' => 'issued',
                'value'     => $model->issued ? 'Да' : 'Нет',
            ],
            [
                'attribute' => 'date_add',
                'value'     => Yii::$app->formatter->asDate($model->date_add),
            ],
            [
                'attribute' => 'date_of_issue',
                'value'     => Yii::$app->formatter->asDate($model->date_of_issue),
            ],
            [
                'attribute' => 'middlemen_id',
                'value'     => isset($model->middlemen) ? $model->middlemen->name : 'нет',
            ],
        ],
    ]) ?>

</div>
