<?php

namespace app\models\forms;

use yii\base\Model;
use yii\web\UploadedFile;

class UploadForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $photo;

    public function rules()
    {
        return [
            [['photo'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg, jpeg'],
        ];
    }

    public function upload($fileName, $dir)
    {
        if ($this->validate()) {
            return $this->photo->saveAs($dir . '/' . $fileName);
        }

        return false;
    }

    public function getExtension()
    {
        return $this->photo->extension ?? '';
    }
}