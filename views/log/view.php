<?php

use app\helpers\AuthHelper;
use app\models\ars\Course;
use app\models\ars\Group;
use app\models\ars\User;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\searches\GroupSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Логи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="group-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'table',
            ],
            [
                'attribute' => 'old_data',
                'value'     => function ($model) {
                    $string = '';
                    $data = json_decode($model->old_data, true);
                    foreach ($data as $attribute => $value) {
                        $string .= $attribute . ' => ' . $value . '<br>';
                    }
                    return $string;
                },
                'format' => 'html'
            ],
            [
                'attribute' => 'new_data',
                'value'     => function ($model) {
                    $string = '';
                    $data = json_decode($model->new_data, true);
                    foreach ($data as $attribute => $value) {
                        $string .= $attribute . ' => ' . $value . '<br>';
                    }
                    return $string;
                },
                'format' => 'html'
            ],
            [
                'attribute' => 'user_id',
                'value'     => function ($model) {
                    /** @var User $user */
                    $user = User::findOne($model->user_id);
                    return $user ? $user->login : '';
                },
            ],
            'created_at:datetime',
        ],
    ]); ?>


</div>
