<?php
/* @var $client \app\models\ars\Client */
/* @var \app\models\ars\ClientCertificate[] $certificates */
/* @var \app\models\ars\Client $client */

?>
<html>

<head>
    <meta http-equiv=Content-Type content="text/html; charset=utf-8">
    <meta name=Generator content="Microsoft Word 14 (filtered)">
    <style>
        <!--
        /* Font Definitions */
        @font-face {
            font-family: Arial;
            panose-1: 2 15 5 2 2 2 4 3 2 4;
        }

        /* Style Definitions */
        p.MsoNormal, li.MsoNormal, div.MsoNormal {
            margin-top: 0cm;
            margin-right: 0cm;
            margin-bottom: 10.0pt;
            margin-left: 0cm;
            line-height: 115%;
            font-size: 12.0pt;
            font-family: "Arial", "sans-serif";
        }

        .MsoPapDefault {
            margin-bottom: 10.0pt;
            line-height: 115%;
        }

        @page WordSection1 {
            size: 595.3pt 841.9pt;
            margin: 36.0pt 36.0pt 36.0pt 36.0pt;
        }

        div.WordSection1 {
            page: WordSection1;
        }


        .page-break {
            page-break-before: always;
        }
        -->
    </style>

</head>

<body lang=RU>
<div class=WordSection1>
	<div style='height:50%;'>
		<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt; text-align:center;line-height:normal'><span lang=UK style='font-size:14.0pt;font-family:"Arial","serif"'>ТОВАРИСТВО З ОБМЕЖЕНОЮ ВІДПОВІДАЛЬНІСТЮ</span></p>
		<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:center;line-height:normal'><b><span style='font-size:14.0pt;font-family:"Arial","serif"'>«ЄВРОПЕЙСЬКИЙ МОРСЬКИЙ ТРЕНАЖЕРНИЙ ЦЕНТР»</span></b></p>
		<p>&nbsp;</p>
		<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:center;line-height:normal'><span lang=UK style='font-size:14.0pt;font-family:"Arial","serif"'>ЗАЯВА<br/>на проходження навчання</span></p>
		<p class=MsoNormal><span lang=UK style='font-size:14.0pt; font-family:"Arial","sans-serif"'><?= $client->getFullNameUkr() ?></span></p>
		<p class=MsoNormal><span lang=UK style='font-size:14.0pt; font-family:"Arial","sans-serif"'>Паспорт: серія <u><?= $client->passport_series ?></u>&nbsp;№ <u><?= $client->passport_number ?></u>, виданий <u><?= Yii::$app->formatter->asDate($client->passport_date_issued) ?></u> року <u><?= $client->passport_issued_by ?></u></span></p>
		<p class=MsoNormal><span lang=UK style='font-size:14.0pt; font-family:"Arial","sans-serif"'>Адреса реєстрації: <u><?= $client->registration_address ?></u></span></p>
		<p>&nbsp;</p>
		<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align: justify;text-indent:35.4pt;line-height:normal'><span lang=UK style='font-size:14.0pt;font-family:"Arial","serif"'>Прошу Вас прийняти мене на навчання (підвищення кваліфікації) у тренажерний центр ТОВАРИСТВА З ОБМЕЖЕНОЮ ВІДПОВІДАЛЬНІСТЮ «ЄВРОПЕЙСЬКИЙ МОРСЬКИЙ ТРЕНАЖЕРНИЙ ЦЕНТР» за напрямками підготовки відповідно до Міжнародної конвенції про підготовку і дипломування моряків та несення вахти 1978 року (з Манільським поправками)</span></p>
		<br />
		<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height: normal'><b><i><span lang=UK style='font-size:14.0pt; font-family:"Arial","serif"'>Додатки:</span></i></b></p>
		<br />
		<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal'><span lang=UK style='font-size:14.0pt;font-family:"Arial", "serif"'>_______________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________</span></p>
		<br />
		<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal'><span lang=UK style='font-size:14.0pt;font-family:"Arial", "serif"'>_______________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________</span></p>
		<br />
		<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal'><span lang=UK style='font-size:14.0pt;font-family:"Arial", "serif"'>_______________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________</span></p>
		<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal'><span lang=UK style='font-size:4.0pt;font-family:"Arial", "serif"'>&nbsp;</span></p>
		<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal'><span lang=UK style='font-size:10.0pt;font-family:"Arial", "serif"'>З умовами надання навчальник послуг ознайомлений, зобов’язуюсь повністю виконувати умови визначені договором (офертою) про навчання*</span></p>
		<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt; text-align:center;line-height:normal'><span lang=UK style='font-size:14.0pt; font-family:"Arial","serif"'>&nbsp;</span></p>
		<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:center;line-height:normal'><span lang=UK style='font-size:14.0pt;font-family:"Arial","serif"'>&nbsp;</span></p>
		<p class=MsoNormal><span lang=UK style='font-size:12.0pt;line-height:107%; font-family:"Arial","serif"'>«____» ____________ 20____ року _______________/____________________________/ </span></p>
	</div>
	<div style='50%'>
		<div class=WordSection1>
<br />
<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align: center;line-height:normal'><b><span lang=UK style='font-size:14.0pt; font-family:"Arial",serif'>ВВІДНИЙ ІНСТРУКТАЖ З БЕЗПЕКИ ЖИТТЄДІЯЛЬНОСТІ ДЛЯ СЛУХАЧІВ <br />ТОВ «ЄВРОПЕЙСЬКИЙ МОРСЬКИЙ ТРЕНАЖЕРНИЙ ЦЕНТР»</span></b></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;line-height:normal'><b><span lang=UK style='font-size:14.0pt;
font-family:"Times New Roman",serif'>&nbsp;</span></b></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;line-height:normal'><span lang=UK style='font-size:14.0pt;font-family:
"Arial",serif'>Я, <?= $client->getFullNameUkr() ?></span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
115%'><span lang=UK style='font-size:14.0pt;line-height:115%;font-family:"Arial",serif'><?= Yii::$app->formatter->asDate($client->birthday) ?> року народження, пройшов інструктаж перед початком занять з
питань інструктажу з безпеки життєдіяльності, який включає:</span></p>

<p class=MsoListParagraphCxSpFirst style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:115%'><span lang=UK style='font-size:14.0pt;
line-height:115%;font-family:"Arial",serif'>1.<span style='font:7.0pt "Arial"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=UK style='font-size:14.0pt;line-height:115%;
font-family:"Arial",serif'>Загальні відомості про заклад, його
структуру (кабінети, тренажерні кабінети, навчальне судно тощо). Види та
джерела небезпеки в кабінетах ТОВ «Європейський морський тренажерний центр».</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0cm;margin-bottom:
.0001pt;line-height:115%'><span lang=UK style='font-size:
14.0pt;line-height:115%;font-family:"Arial",serif'>2.<span
style='font:7.0pt "Arial"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span
lang=UK style='font-size:14.0pt;line-height:115%;font-family:"Arial",serif'>Загальні
правила поведінки під час підготовки, поновлення та актуалізації знань.
Обставини та причини найбільш характерних нещасних випадків, що сталися в
учбово-тренажерних центрах.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0cm;margin-bottom:
.0001pt;line-height:115%'><span lang=UK style='font-size:
14.0pt;line-height:115%;font-family:"Arial",serif'>3.<span
style='font:7.0pt "Arial"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span
lang=UK style='font-size:14.0pt;line-height:115%;font-family:"Arial",serif'>Вимоги
пожежної безпеки учбово-тренажерному центрі. Знайомство з Правилами пожежної
безпеки для закладів, установ, організацій підприємств під час підготовки,
поновлення та актуалізації знань.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0cm;margin-bottom:
.0001pt;line-height:115%'><span lang=UK style='font-size:
14.0pt;line-height:115%;font-family:"Arial",serif'>4.<span
style='font:7.0pt "Arial"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span
lang=UK style='font-size:14.0pt;line-height:115%;font-family:"Arial",serif'>Радіаційна
безпека, дії у разі надзвичайних ситуацій природного та техногенного характеру.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0cm;margin-bottom:
.0001pt;line-height:115%'><span lang=UK style='font-size:
14.0pt;line-height:115%;font-family:"Arial",serif'>5.<span
style='font:7.0pt "Arial"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span
lang=UK style='font-size:14.0pt;line-height:115%;font-family:"Arial",serif'>Перша
(долікарська) медична допомога у разі нещасних випадків, надзвичайних подій
тощо.</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:
.0001pt;line-height:115%'><span lang=UK style='font-size:14.0pt;line-height:
115%;font-family:"Times New Roman",serif'>&nbsp;</span></p>
<br /><br />
<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;
line-height:115%'><span lang=UK style='font-size:12.0pt;line-height:115%;
font-family:"Times New Roman",serif'>Підпис слухача    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;             _______________ 
_______________________________  _______________________</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
200%'><span lang=UK style='line-height:200%;font-family:"Times New Roman",serif'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;підпис&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;П. І. Б.                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;дата</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
200%'><span lang=UK style='line-height:200%;font-family:"Times New Roman",serif'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;
line-height:115%'><span lang=UK style='font-size:12.0pt;line-height:115%;
font-family:"Times New Roman",serif'>Підпис інструктора &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;        _______________ 
_______________________________  _______________________</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
200%'><span lang=UK style='line-height:200%;font-family:"Times New Roman",serif'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;підпис&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;П. І. Б.                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;дата</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
200%'><span lang=UK style='line-height:200%;font-family:"Times New Roman",serif'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
200%'><span lang=UK style='line-height:200%;font-family:"Times New Roman",serif'>&nbsp;</span></p>

</div>
</div>
</div>

<div class=WordSection1>
<div class=page-break>
<p class=MsoNormal align=center style='text-align:center'><b><span lang=UK
style='font-size:16.0pt;line-height:107%;font-family:"Times New Roman",serif'>Згода
на обробку персональних даних моряків</span></b></p>

<p class=MsoNormal align=center style='text-align:center'><b><span lang=UK
style='font-size:16.0pt;line-height:107%;font-family:"Times New Roman",serif'>&nbsp;</span></b></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span lang=UK style='font-size:14.0pt;font-family:"Times New Roman",serif'>Я, <?= $client->getFullNameUkr() ?></span></p>


<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span lang=UK style='font-size:10.0pt;
font-family:"Times New Roman",serif'>&nbsp;</span></p>

<p class=MsoNormal><span lang=UK style='font-size:14.0pt;line-height:107%;
font-family:"Times New Roman",serif'><?= Yii::$app->formatter->asDate($client->birthday) ?> року народження,
відповідно до Закону України  «Про захист персональних даних»:</span></p>

<p class=MsoNormal style='text-align:justify;line-height:115%'><span lang=UK
style='font-size:14.0pt;line-height:115%;font-family:"Times New Roman",serif'>Надаю
</span><span lang=UK style='font-size:14.0pt;line-height:115%;font-family:"Times New Roman",serif'>ТОВ
«Європейський морський тренажерний центр» в особі директора </span><span
lang=UK style='font-size:14.0pt;line-height:115%;font-family:"Times New Roman",serif'>Костенюк
Р.Б., добровільну згоду на обробку моїх персональних даних з метою забезпечення
реалізації адміністративно-правових відносин у сфері оформлення, видачі,
продовження, обліку і збереження документів; ведення діловодства; підготовки
відповідно до вимог законодавства статистичної, адміністративної та іншої
інформації.</span></p>

<p class=MsoNormal style='text-align:justify;line-height:115%'><span lang=UK
style='font-size:14.0pt;line-height:115%;font-family:"Times New Roman",serif'>        
Відповідно до визначеної мети обробки до персональних даних включається:</span></p>

<p class=MsoNormal style='text-align:justify;line-height:115%'><span lang=UK
style='font-size:14.0pt;line-height:115%;font-family:"Times New Roman",serif'>1. Паспортні
дані (копії першої та другої (а у разі досягнення віку 25 або 45 років –
додатково третьої або п’ятої) сторінок паспорта громадянина України або копії
сторінок документа, який посвідчує особу іноземця або особу без громадянства
(для таких осіб), на яких зазначено відомості про ім’я, прізвища та дату
народження власника та є його фотокартка; попередній кваліфікаційний документ
та підтвердження до нього (за наявністю); копія документа про освіту; копії
документів, що підтверджують проходження схваленої обов’язкової підготовки;
копія документа про підвищення кваліфікації (у разі якщо це передбачено чинним
законодавством); фотокартки; копія робочого диплому та свідоцтва виданого на
підставі міжнародної Конвенції </span><span style='font-size:14.0pt;line-height:
115%;font-family:"Times New Roman",serif'>STCW</span><span lang=UK
style='font-size:14.0pt;line-height:115%;font-family:"Times New Roman",serif'>/ПДНВ
(про підготовку дипломування моряків та несення вахти) 1978 року; копії сторінок
посвідчення моряка (за наявності), на якій зазначено відомості про ім’я,
прізвище та дату народження власника та є його фотокартка, копії сторінок
послужної книжки моряка із записами, що підтверджують необхідний стаж роботи на
судні, а також сторінки, на якій зазначено відомості про ім’я, прізвище та дату
народження власника та є його фотокартка (у разі необхідності підтвердження
стажу роботи);</span></p>

<p class=MsoNormal style='text-align:justify;line-height:115%'><span lang=UK
style='font-size:14.0pt;line-height:115%;font-family:"Times New Roman",serif'>2.
Засвідчую, що мені повідомлено про наступне: </span></p>

<p class=MsoNormal style='text-align:justify;line-height:115%'><span lang=UK
style='font-size:14.0pt;line-height:115%;font-family:"Times New Roman",serif'>2.1
Під обробкою моїх персональних даних розуміється будь-яка дія або сукупність
дій, здійснюваних повністю або частково в автоматизованих системах та/або в
картотеках персональних даних, які пов’язані зі збиранням, реєстрацією,
накопиченням, зберіганням, зміною, відновленням, використанням та поширенням,
знеособленням, знищенням відомостей про мене як моряка </span><span lang=UK
style='font-size:14.0pt;line-height:115%;font-family:"Times New Roman",serif'>ТОВ
«Європейський морський тренажерний центр» </span></p>

<p class=MsoNormal style='text-align:justify;line-height:115%'><span lang=UK
style='font-size:14.0pt;line-height:115%;font-family:"Times New Roman",serif'>2.2
ТОВ «Європейський морський тренажерний центр» приймає на себе зобов’язання щодо
захисту моїх  персональних даних та вживає технічних і організованих заходів
щодо захисту цих відомостей.</span></p>

<p class=MsoNormal style='text-align:justify;line-height:115%'><span lang=UK
style='font-size:14.0pt;line-height:115%;font-family:"Times New Roman",serif'>2.3
Обробка моїх персональних даних провадитиметься виключно працівниками ТОВ
«Європейський морський тренажерний центр», які надали письмові зобов’язання про
нерозголошення персональних даних інших осіб, що стали їм відомі у зв’язку з
використанням посадових обов’язків.</span></p>

<p class=MsoNormal style='text-align:justify;line-height:115%'><span lang=UK
style='font-size:14.0pt;line-height:115%;font-family:"Times New Roman",serif'>2.4
ТОВ «Європейський морський тренажерний центр» має право на передання
персональних даних інших осіб, що стали їм відомі у зв’язку з виконанням
посадових обов’язків.</span></p>

<p class=MsoNormal style='text-align:justify;line-height:115%'><span lang=UK
style='font-size:14.0pt;line-height:115%;font-family:"Times New Roman",serif'>2.5
ТОВ «Європейський морський тренажерний центр» має право на передання
персональних даних моряків без повідомлення їх про це у випадках та на умовах,
визначених законодавством.</span></p>

<p class=MsoNormal style='text-align:justify;line-height:115%'><span lang=UK
style='font-size:14.0pt;line-height:115%;font-family:"Times New Roman",serif'>2.6
Строки зберігання персональних даних, зазначених у пункті 1 цієї згоди,
визначаються згідно зі строками зберігання відповідних документів,
встановленими законодавством України та внутрішніми документами, після чого
персональні дані підлягають знищенню у визначеному законодавством порядку. </span></p>

<p class=MsoNormal style='text-align:justify;line-height:115%'><span lang=UK
style='font-size:14.0pt;line-height:115%;font-family:"Times New Roman",serif'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;line-height:normal'><span lang=UK style='font-size:14.0pt;font-family:
"Times New Roman",serif'>«___»  _____________20____ р.       ____________        
______________________</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;line-height:normal'><span lang=UK style='font-size:14.0pt;font-family:
"Times New Roman",serif'>                                                          
   </span><span lang=UK style='font-size:10.0pt;font-family:"Times New Roman",serif'>підпис                                                       
ПІБ</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;line-height:normal'><span lang=UK style='font-size:14.0pt;font-family:
"Times New Roman",serif'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;line-height:normal'><span lang=UK style='font-size:14.0pt;font-family:
"Times New Roman",serif'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;line-height:normal'><span lang=UK style='font-size:14.0pt;font-family:
"Times New Roman",serif'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;line-height:normal'><span lang=UK style='font-size:14.0pt;font-family:
"Times New Roman",serif'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;line-height:normal'><span lang=UK style='font-size:14.0pt;font-family:
"Times New Roman",serif'>&nbsp;</span></p>

</div>
</div>

<div class=WordSection1>
	<div class=page-break>
		<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:center;line-height:normal'><span lang=UK style='font-size:14.0pt;font-family:"Arial","serif"'>*УМОВИ НАДАННЯ ПОСЛУГ</span></p>
		<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:center;line-height:normal'><span lang=UK style='font-size:12.0pt;font-family:"Arial","serif"'>(Договір про навчання (оферта)</span></p>

    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span lang=UK style='font-size:10.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:center;line-height:normal'><span lang=UK style='font-size:14.0pt;font-family:"Arial","serif"'>1. ПРЕДМЕТ ДОГОВОРУ </span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align: justify;line-height:normal'><span lang=UK style='font-size:12.0pt;font-family: "Arial","serif"'>1.1. Виконавець бере на себе зобов'язання за рахунок коштів Замовника здійснити навчання (підготовку, перепідготовку, підвищення кваліфікації кадрів або надати додаткову освітню послугу), далі - освітня послуга, а саме:</span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:justify;line-height:normal'><span lang=UK style='font-size:12.0pt;font-family:"Arial","serif"'>Підготовка відповідно до Міжнародної конвенції про підготовку і дипломування моряків та несення вахти 1978 року (з Манільським поправками):</span></p>
    <?php $totalSum = 0; ?>
    <?php foreach ($certificates as $certificate): ?>
        <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:justify;line-height:normal'>
            <b>
                <span lang=UK style='font-size:12.0pt;font-family:"Times New Roman","serif";'>- <?= $certificate->course->title_ukr ?></span>
            </b>
        </p>
        <?php $totalSum += $certificate->total_sum; ?>
    <?php endforeach; ?>
    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:center;line-height:normal'><span lang=UK style='font-size:10.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:center;line-height:normal'><span lang=UK style='font-size:14.0pt;font-family:"Arial","serif"'>2. ОБОВ'ЯЗКИ ВИКОНАВЦЯ </span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:justify;line-height:normal'><span lang=UK style='font-size:12.0pt;font-family:"Arial","serif"'>2.1. Надати Замовнику освітню послугу на рівні державних стандартів освіти. </span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:justify;line-height:normal'><span lang=UK style='font-size:12.0pt;font-family:"Arial","serif"'>2.2. Забезпечити дотримання прав учасників навчального процесу відповідно до законодавства. </span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:justify;line-height:normal'><span lang=UK style='font-size:12.0pt;font-family:"Arial","serif"'>2.3. Видати Замовнику, по закінченню курсу навчання, документ державного зразка. (Документ видається, якщо послуга передбачає, згідно з законодавством, видачу такого документа.) </span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:justify;line-height:normal'><span lang=UK style='font-size:12.0pt;font-family:"Arial","serif"'>2.4. Інформувати Замовника про правила та вимоги щодо організації надання освітньої послуги, її якості та змісту, про права і обов'язки сторін під час надання та отримання таких послуг. </span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:justify;line-height:normal'><span lang=UK style='font-size:10.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:center;line-height:normal'><span lang=UK style='font-size:12.0pt;font-family:"Arial","serif"'>3. ОБОВ'ЯЗКИ ЗАМОВНИКА </span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:justify;line-height:normal'><span lang=UK style='font-size:12.0pt;font-family:"Arial","serif"'>3.1. Своєчасно вносити плату за отриману освітню послугу в розмірах та у строки, що встановлені цим договором. </span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:justify;line-height:normal'><span lang=UK style='font-size:12.0pt;font-family:"Arial","serif"'>3.2. Виконувати вимоги законодавства та статуту (положення) Виконавця з організації надання освітніх послуг. </span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:justify;line-height:normal'><span lang=UK style='font-size:10.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:center;line-height:normal'><span lang=UK style='font-size:14.0pt;font-family:"Arial","serif"'>4. ПЛАТА ЗА НАДАННЯ ОСВІТНЬОЇ ПОСЛУГИ ТА ПОРЯДОК РОЗРАХУНКІВ </span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align: justify;line-height:normal'><span lang=UK style='font-size:12.0pt;font-family: "Arial","serif"'>4.1. Розмір плати встановлюється за весь строк надання освітньої послуги.</span></p>
    <?php $nds = $totalSum / 5; ?>
    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:justify;line-height:normal'><span lang=UK style='font-size:12.0pt;font-family:"Arial","serif"'>4.2. Загальна вартість освітньої послуги становить <b><span lang=UK style='font-size:12.0pt; font-family:"Arial","sans-serif"'><?= number_format($nds + $totalSum, 2) ?></span></b> гривень. </span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:justify;line-height:normal'><span lang=UK style='font-size:12.0pt;font-family: "Arial","serif"'>4.3. Замовник вносить плату безготівково на розрахунковий рахунок Виконавця на пізніше 5 (п’яти) календарних днів з моменту укладання даного Договору у повному обсязі.</span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:justify;line-height:normal'><span lang=UK style='font-size:12.0pt;font-family:"Arial","serif"'>4.4. У разі припинення навчання Замовника з його вини, плата за навчання не повертається. </span></p>

    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:center;line-height:normal'><span lang=UK style='font-size:10.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:center;line-height:normal'><span lang=UK style='font-size:14.0pt;font-family:"Arial","serif"'>5. ВІДПОВІДАЛЬНІСТЬ СТОРІН ЗА НЕВИКОНАННЯ АБО НЕНАЛЕЖНЕ </span></p>

    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:center;line-height:normal'><span lang=UK style='font-size:14.0pt;font-family:"Arial","serif"'> ВИКОНАННЯ ЗОБОВ'ЯЗАНЬ </span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:justify;line-height:normal'><span lang=UK style='font-size:12.0pt;font-family:"Arial","serif"'>5.1. За невиконання або неналежне виконання зобов'язань за цим договором сторони несуть відповідальність згідно з чинним законодавством.</span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:justify;line-height:normal'><span lang=UK style='font-size:12.0pt;font-family:"Arial","serif"'>5.2. За несвоєчасне внесення плати за надання освітніх послуг Замовник сплачує Виконавцю неустойку пеню) у розмірі подвійної облікової ставки НБУ за кожний день прострочення платежу від загальної суми вартості послуги.</span></p>

    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:center;line-height:normal'><span lang=UK style='font-size:10.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:center;line-height:normal'><span lang=UK style='font-size:14.0pt;font-family:"Arial","serif"'>6. ПРИПИНЕННЯ ДОГОВОРУ </span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:justify;line-height:normal'><span lang=UK style='font-size:12.0pt;font-family:"Arial","serif"'>6.1. Дія договору припиняється:</span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:justify;line-height:normal'><span lang=UK style='font-size:12.0pt;font-family:"Arial","serif"'>- за згодою сторін;</span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:justify;line-height:normal'><span lang=UK style='font-size:12.0pt;font-family:"Arial","serif"'>- якщо виконання стороною договору своїх зобов'язань є неможливим у зв'язку з прийняттям нормативно-правових актів, що змінили умови, встановлені договором щодо освітньої послуги, і будь-яка із сторін не погоджується про внесення змін до договору;</span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:justify;line-height:normal'><span lang=UK style='font-size:12.0pt;font-family:"Arial","serif"'>- у разі ліквідації Виконавця, якщо не визначена юридична особа, що є правонаступником ліквідованої сторони;</span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal'><span lang=UK style='font-size:12.0pt;font-family:"Arial","serif"'>- у разі відрахування Замовника з закладу Виконавця згідно з законодавством;</span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height: normal'><span lang=UK style='font-size:12.0pt;font-family:"Arial","serif"'>- за рішенням суду в разі систематичного порушення або невиконання умов договору.</span></p>

</div>
</div>
</body>

</html>
