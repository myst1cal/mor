<?php

namespace app\controllers;

use app\helpers\AuthHelper;
use app\models\ars\Course;
use app\models\ars\CourseAdditionalField;
use app\models\ars\CourseAdditionalFieldCourse;
use app\models\ars\Teacher;
use app\models\ars\TeacherCourse;
use app\models\searches\CourseSearch;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * CourseController implements the CRUD actions for Course model.
 */
class CourseController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view'],
                        'allow'   => true,
                        'roles'   => AuthHelper::CAN_SEE_MENU_COURSES,
                    ],
                    [
                        'actions' => ['create', 'update'],
                        'allow'   => true,
                        'roles'   => AuthHelper::CAN_EDIT_COURSE,
                    ],
                    [
                        'actions' => ['get-teachers'],
                        'allow'   => true,
                        'roles'   => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Course models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CourseSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Course model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the Course model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Course the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Course::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Creates a new Course model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Course();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            TeacherCourse::deleteAll(['course_id' => $model->id]);

            $currentTeachers = Yii::$app->request->post('teacher');

            foreach ($currentTeachers as $currentTeacher) {
                $teacherCourse = new TeacherCourse();
                $teacherCourse->course_id = $model->id;
                $teacherCourse->teacher_id = $currentTeacher;

                $teacherCourse->save();
            }

            CourseAdditionalFieldCourse::deleteAll(['course_id' => $model->id]);

            $currentAdditionalFields = Yii::$app->request->post('additionalField');
            $currentAdditionalFieldSort = Yii::$app->request->post('additionalFieldSort');

            if (!empty($currentAdditionalFields)) {
                foreach ($currentAdditionalFields as $currentAdditionalField) {
                    $courseAdditionalFieldCourse = new CourseAdditionalFieldCourse();
                    $courseAdditionalFieldCourse->course_id = $model->id;
                    $courseAdditionalFieldCourse->course_additional_field_id = $currentAdditionalField;
                    $courseAdditionalFieldCourse->sort = $currentAdditionalFieldSort[$currentAdditionalField];

                    $courseAdditionalFieldCourse->save();
                }
            }

            return $this->redirect(['view', 'id' => $model->id]);
        }

        $teachers = Teacher::find()->all();
        $additionalFields = CourseAdditionalField::find()->all();
        return $this->render('create', [
            'model'           => $model,
            'teachers'        => $teachers,
            'additionalFields' => $additionalFields,
            'currentTeachers' => [],
            'currentAdditionalFields' => [],
            'currentAdditionalFieldSort' => [],
        ]);
    }

    /**
     * Updates an existing Course model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        /** @var Course $model */
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            TeacherCourse::deleteAll(['course_id' => $model->id]);

            $currentTeachers = Yii::$app->request->post('teacher');

            foreach ($currentTeachers as $currentTeacher) {
                $teacherCourse = new TeacherCourse();
                $teacherCourse->course_id = $model->id;
                $teacherCourse->teacher_id = $currentTeacher;

                $teacherCourse->save();
            }

            CourseAdditionalFieldCourse::deleteAll(['course_id' => $model->id]);

            $currentAdditionalFields = Yii::$app->request->post('additionalField');
            $currentAdditionalFieldSort = Yii::$app->request->post('additionalFieldSort');

            if (!empty($currentAdditionalFields)) {
                foreach ($currentAdditionalFields as $currentAdditionalField) {
                    $courseAdditionalFieldCourse = new CourseAdditionalFieldCourse();
                    $courseAdditionalFieldCourse->course_id = $model->id;
                    $courseAdditionalFieldCourse->course_additional_field_id = $currentAdditionalField;
                    $courseAdditionalFieldCourse->sort = $currentAdditionalFieldSort[$currentAdditionalField];

                    $courseAdditionalFieldCourse->save();
                }
            }

            return $this->redirect(['view', 'id' => $model->id]);
        }

        $currentTeachers = ArrayHelper::getColumn($model->teachers, 'id');
        $currentAdditionalFields = ArrayHelper::getColumn($model->courseAdditionalFields, 'id');
        $currentAdditionalFieldSort = ArrayHelper::map(CourseAdditionalFieldCourse::findAll(['course_id' => $model->id]), 'course_additional_field_id', 'sort');

        $teachers = Teacher::find()->all();
        $additionalFields = CourseAdditionalField::find()->all();
        return $this->render('update', [
            'model' => $model,
            'teachers' => $teachers,
            'additionalFields' => $additionalFields,
            'currentTeachers' => $currentTeachers,
            'currentAdditionalFields' => $currentAdditionalFields,
            'currentAdditionalFieldSort' => $currentAdditionalFieldSort,
        ]);
    }

    public function actionGetTeachers()
    {
        $id = Yii::$app->request->get('id');
        $course = Course::findOne($id);

        $teachers = $course->teachers;

        foreach ($teachers as $teacher) {
            echo '<option value="' . $teacher->id . '">' . $teacher->full_name . '</option>';
        }
    }
}
